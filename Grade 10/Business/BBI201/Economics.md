## Economics Resources (factors of production)

**Definition** Resources needed to provide goods/services to consumers

Natural Resources
- raw materials that come from the earth, water, and air.
- for example a pencil, the raw materials are the lead, wood, and anything else that makes up the pencil

Human Resources
- the people who work to create the goods and services
- taking on the example of the pencil again, the woodwork and and things done to make the pencil (shape and usage)

Captial Resources
- resources that last for a long period of time and require investement on the part of the buisness
- the machines that can be in an assembly line which are used for a long period of time.

In most cases, it takes a combination of all 3 economic reousrces to create the good and services.

What happens if there isn't enough of an economic resrouce (i.e Oil)
- The price of the good/service increases
- Alternatives must be found for the resource
- Example, a different method to be an alternate instead of oil when frying eggs in a restaurant.

## Demand and Supply

2 Things to understand (2 principles) 
- looking at the market side of consumers
- looking at the market side of the producers

Consumers buy the things that the producers take to the market.

As demand raises, the price also increases (to maximaze on the profit)

As demand lowers, the price also decreases (to encourage people to buy more and increase demand)

When the price is high, the demand is low (people don't want to buy something for more money)

When the price is low, the demand is higher (people want to buy something for less money)


### Example

I'm selling chocolate!
- Who would like to buy some?
- What will you pay for it?
- Conduct a bidding session

### Demand
- the quantity of goods or service that consumers are **willing and able** to buy ata a particular price


### Law of Demand
- Ususally demand goes up as prices goes down, and vice versa

### What creates demand
1. consumer is aware & interested in the goods
2. there is 'supply' of the good/serivice
3. price is reasonable and competitive
4. The constomer can access the good


### What will Change Demand
1. Change in consumer income
2. Change in consumer's taste
3. Changes in what we expect in the future
4. Changes in population


## Supply

**Definition:** The quantity of a good or service that businesses are willing and able to provide within a range of prices
- Law of supply: As supply goes up, prices go up

### What changes the quantity suppplied?
- A change in the number of producers -> Competition
- Price of related goods (if gas prices increase, people may buy more energy efficient cars)
- Changes in technology (VCR sales vs DVD sales)
- Changes in cost of production
