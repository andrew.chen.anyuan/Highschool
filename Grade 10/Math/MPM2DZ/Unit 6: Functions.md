# Unit 6: Functions

## Functions

`Function`: a correspondnce between two sets of elements. It "links" each element between the first set with **one and only one element** in the second set.
- The first set is called the `Domain` (**values of x**)
- The second set is called the `Range` (**values of y**)

A **relation** is something that **relates** a variable to another. It is slightly different than a function. All functions are relations, **but not all relations are functions!**

`Vertical Line Test`: A test to check whether or not a relation is a function. If a **vertical line** parallel to the y-axis can be drawn through any 2 points on a relation, then
that relation is **not a function**.

`Horizontal Line test`: A test to check whetehr or not the inverse of a function is also a function. If  a **horizontal line** parallel to the x-axis can be drawn through any 2 points on a function, then the inverse of that function is **not a function**.

## Types Of Functions

|Function|Base Form|Domain|Range|
|:-------|:--------|:-----|:----|
|Linear Function|$`y=x`$|$`\{ x \mid x \in \mathbb{R} \}`$|$`\{y \mid y \in \mathbb{R} \}`$|
|Quadratic Function|$`y=x^2`$|$`\{x \mid x \in \mathbb{R} \}`$|$`\{y \mid y \ge 0, y \in \mathbb{R} \}`$|
|Cubic Function|$`y=x^3`$|$`\{x \mid x \in \mathbb{R} \}`$|$`\{y \mid y \in \mathbb{R} \}`$|
|Square Root Function|$`y= \sqrt{x}`$|$`\{x \mid x \ge 0, x \in \mathbb{R}\}`$|$`\{y \mid y \ge 0, y \in \mathbb{R}\}`$|
|Absolute Value Function|$`y = \| x \|`$|$`\{ x \mid x \in \mathbb{R}\}`$|$`\{y \mid y \ge 0, y \in \mathbb{R}\}`$|
|Reciprocal Function|$`y = \dfrac{1}{x}`$|$`\{x \mid x =\not 0, x \in \mathbb{R}\}`$|$`\{y \mid y =\not 0, y \in \mathbb{R}\}`$|
|Exponential Function|$`y = 2^x`$|$`\{x \mid x \in \mathbb{R}\}`$|$`\{y \mid y \gt 0, y \in \mathbb{R}\}`$|
|Logarithmic Function (Inverse of the Exponential Function)|$`y = \log (x)`$|$`\{x \mid x \gt 0, x \in \mathbb{R}\}`$|$`\{y \mid y \in \mathbb{R}\}`$|


To get base points, simply choose integers in the range $`-3 \le x \le 3`$ (cause they are small, and $`0`$ is always a great choice), and plug it in as the x-value and you will get the value from the specific function, and that will be one
of your base points.

## Piecewise Functions
These are functions that are made of multiple functions. Thus they have a specified domain for each piece. Examples include:

```math
f(x) = \begin{cases}

x^2 & \text{if } x \lt 2 \\

6 & \text{if } x = 2 \\

10 - x & \text{if } 2 \lt x \le 6 \\

\end{cases}

\quad \text{and} \quad

f(x) = \begin{cases}

-4 & \text{if } -8 \lt x \le -3 \\

x - 2 & \text{if } -3 \lt x \lt 0 \\

\sqrt{x} & \text{if } 0 \lt x \le 8 \\

\end{cases}
```

Reminder, if the function does not include a point, make sure to use a empty dot ($`\circ`$) instead of a filled dot ($`\bullet`$).

## Functions With Other Functions (Compositions of Functions)

Let $`f(x), g(x)`$ represent two functions. We can do other things with functions, such as:
- **Add** two functions: $`f(x) + g(x)`$
- **Subtract** two functions: $`f(x) - g(x)`$
- **Multiply** two functions: $`f(x) \times g(x)`$
- **Divide** two functions: $`\dfrac{f(x)}{g(x)}`$
- Find the **inverse** of a function: $`f^{-1}(x)`$


It is also possbile to put functions within of functions, such as $`f(g(x))`$. This can also be represented as $`(f \circ g)(x)`$. $`f(g(x))`$ is an example of a **Composite Function**.

## Transformation Of Functions.

Any functions can be repsented in a form $`\Huge y = af[k(x-d)] + c`$

The **mapping rule** states for a base point $`(x, y)`$, the new point will be $`(\dfrac{1}{k}x + d, ay + c)`$.

### Vertical Translations
If $`d \lt 0`$, the graph moves to the **left** horizontally $`d`$ units.

If $`d \gt 0`$, the graph moves to the **right** horizontally $`d`$ units.

### Horizontal Translations

If $`c \lt 0`$, the graph moves **down** vertically $`d`$ units.

if $`c \gt 0`$, the graph moves **up** vertically $`d`$ units.

### Vertical Stretch/Compressions

If $`0 \lt a \lt 1`$, then the graph gets **compressed** by a **factor of** $`a`$.

If $`a \gt 1`$, then the graph gets **stretched** by a **factor of** $`a`$.

### Horizontal Stretch/Compressions
If $`0 \lt k \lt 1`$, then the graph gets **stretched** by a **factor of** $`\dfrac{1}{k}`$

If $`k \gt 1`$, then the graph gets **compressed** by a **factor of** $`\dfrac{1}{k}`$.

### Vertical/Horiztonal Relfections

If $`a \lt 0`$, then the graph gets reflected over the **x-axis**.

If $`k \lt 0`$, then the graph gets reflected over the **y-axis**.

## Finding Inverse

To find the inverse, simply replace $`f(x)`$ with $`x`$ and $`x`$ with $`y`$. Then try to model the equation for $`y`$. That will be the inverse function of the original function.

After swtiching them, here a few tips for some of the functions.

For the `quadratic function`, **complete the square** and model the equation for $`y`$.

For the `reciprocal function`, cross multiply, then factor out the $`y`$, and then model the equation for $`y`$. State restrictions whenever necessary.

For the `square root funtion`, make sure to state your restrictions since without, it becomes the inverse of a quadratic equation.

Remember to change to $`f^{-1}`$ **only if** the relation is a function for the inverse.




