## Analytical Geometry Part 2

### Question 3

Shortest distance = straight perpendicular line that connets $`A`$ to a point on line $`\overline{GH}`$

$`M_{GH} = \dfrac{42+30}{38 + 16} = \dfrac{72}{54} = \dfrac{4}{3}`$

$`M_{\perp GH} = \dfrac{-3}{4}`$

$`y_{\perp GH} - 32 = \dfrac{-3}{4}(x+16)`$

$`y_{\perp GH} = \dfrac{-3}{4}x + 20 \quad (1)`$

$`y_{GH} + 30 = \dfrac{4}{3}(x+16)`$

$`y_{GH} = \dfrac{4}{3}x - \dfrac{26}{3} \quad (2)`$

```math
\begin{cases}

y_{\perp GH} = \dfrac{-3}{4}x + 20 & \text{(1)} \\

\\

y_{GH} = \dfrac{4}{3}x - \dfrac{26}{3} & \text{(2}) \\
\end{cases}
```

Sub $`(1)`$ into $`(2)`$

$`\dfrac{-3}{4}x + 20 = \dfrac{4}{3}x - \dfrac{26}{3}`$

$`-9x + (12)20 = 16x - 4(26)`$

$`25x = 344`$

$`x = \dfrac{344}{25} \quad (3)`$

Sub $`(3)`$ into $`(1)`$

$`y = \dfrac{-3}{4}(\dfrac{344}{25}) + 20`$

$`y = \dfrac{-258}{25} + 20`$

$`y = \dfrac{-258}{25} + \dfrac{500}{25}`$

$`y = \dfrac{242}{25}`$

Distance $`= \sqrt{(-16-\dfrac{344}{25})^2 + (32 - \dfrac{242}{25})^2} = 37.2`$

$`\therefore`$ The shortest length pipe is $`37.2`$ units.


### Question 4

Let $`(x, y)`$ be the center of the circle, and $`r`$ be the radius of the circle.


```math
\begin{cases}
(x-4)^2 + (y-8)^2 = r^2 & \text{(1)} \\

(x-5)^2 + (y-1)^2 = r^2 & \text{(2)} \\

(x+2)^2 + y^2 = r^2 & \text{(3)} \\

\end{cases}
```

Sub $`(1)`$ into $`(2)`$

$`x^2 - 8x + 16 + y^2 - 16y + 64 = x^2 - 10x + 25 + y^2 -2y + 1`$

$`-8x -16y + 80 = -10x - 2y + 26`$

$`2x - 14y = -54`$

$`x - 7y = -27 \quad (4)`$

Sub $`(2)`$ into $`(3)`$

$`x^2 - 10x + 25 + y^2 - 2y + 1 = x^2 + 4x + 4 + y^2`$

$`-10x - 2y +26 = 4x + 4`$

$`14x + 2y = 22`$

$`7x + y = 11`$

$`y = 11 - 7x \quad (5)`$

Sub $`(5)`$ into $`(4)`$

$`x - 7(11-7x) = -27`$

$`x - 77+ 49x = 27`$

$`50x = 50`$

$`x = 1 \quad (6)`$ 

Sub $`(6)`$ into $`(5)`$

$`y = 11 - 7(1)`$

$`y = 4 \quad (7)`$

Sub $`(6), (7)`$ into $`(3)`$

$`(1+2)^2 + 4^2 = r^2`$

$`r^2 = 16 + 9`$

$`r^2 = 25`$

$`\therefore (x-1)^2 + (y-4)^2 = 25`$ is the equation of the circle.

