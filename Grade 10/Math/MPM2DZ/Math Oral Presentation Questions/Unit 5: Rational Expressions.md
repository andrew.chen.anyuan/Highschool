## Rational Expressions

### Question 1 a)

Let $`S_r`$ be Ron's speed and $`S_m`$ be Mack's speed. Let $`t`$ be the time.

$`t_a = \dfrac{30}{S_r}`$

$`t_b = \dfrac{20}{S_m}`$

$`t_{\text{total}} = \dfrac{30}{S_r} + \dfrac{20}{S_m} = \dfrac{30S_m + 20S_r}{(S_m)(S_r)}`$

### Question 1 b)

Here, $`S_r = 35, S_m = 25`$ (Because we want the faster guy to travel more distance to save time). We plug it into our formula above:

$`t_{\text{tota}} = \dfrac{30(25) + (20)(35)}{(35)(25)} = 1.657`$

Therefore the minimum amount of time it will take to complete the race is $`1.657`$ hours, or about $`99.42`$ minutes or $`99`$ minutes and $`25.2`$ seconds.

### Question 2 a)

I would first flip the second fraction and then cross-cross out the common factors like so: 

$`\dfrac{(x+3)(x-6)}{(x+4)(x+5)} \times \dfrac{(x+4)(x-7)}{(x-6)(x+8)}`$

We can cross out the $`(x+4)`$ and $`(x-6)`$ since they cancel each other out.

The final fraction is therefore $`\dfrac{(x+3)(x-7)}{(x+5)(x+8)}`$

For restrictions, at each step, I would mark down the restrictions. Such without doing anthing, we know that $`x =\not -4, -5, 7`$, then after we flip the fraction, we know that 
$`x =\not 6, -8`$, and at the final step, we know that $`x=\not -5, -8`$.

Therefore the final restrictions on $`x`$ would be: $`x=\not -4, -5,-8, 6, 7`$

### Question 2 b)

By using the restrictions and the final product, we know that the 2 fraction can only have the following as its denominator: $`(x+4), (x-2), (x-1)`$

And since we know there is a $`(x-2)`$ as the denominator and $`(x+5)`$ as the numerator for the final product, we just need one of the fractions to cancel out
the denominators $`(x+4), (x-1)`$. 

Thus, 2 fractions such as below would work:

$`\dfrac{(x+5)}{(x-4)(x-1)} \times \dfrac{(x-4)(x-1)}{(x-2)}`$

### Question 2 c)

The student forgot to multiply the numerator by the same number he used to multiply the denomiator.

### Question 3 a)

Let $`V, SA`$ be the volume and surface area respectively.

$`V = \pi r^2 h`$

$`SA = 2r\pi h + 2\pi r^2 \implies 2r \pi (h + r)`$

The ratio of $`V : SA`$ is equal to:

$`\dfrac{\pi r^2 h}{2r\pi (h + r)}`$

$` = \dfrac{rh}{2(h+r)}`$

### Question 3 b)

The restrictions are that $`r`$ and $`h`$ are dimensions of a real cylinder, i.e., that $`r > 0`$ and $`h > 0`$, as the denominator cannot be zero.

