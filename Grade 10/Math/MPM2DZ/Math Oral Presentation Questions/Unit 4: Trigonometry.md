## Trigonometry

### Question 1 a) 

It means to solve all missing/unknown angles and sidelengths. It can be achieved by using some of the following:

1. Sine/cosine law
2. Primary Trigonometry Ratios
3. Similar / Congruent Triangle theorems
4. Angle Theorems
5. Pythagorean Theorem

### Question 1 b) 

Draw a line bisector perpendiculr to $`\overline{XZ}`$. Then by using pythagorean theorem: $`7^2 - y^2 = h^2`$, where $`h`$ is the height.

$`\therefore h = \sqrt{13} \approx 3.61cm`$

### Question 1 c)

We can draw a triangle $`ABC`$ where $`\angle A`$ is the angle between the hands, and $`\overline{AB}`$ and $`\overline{BC}`$ are the long and short hands respectively.

Since a clock is a circle, $`\angle A = \dfrac{360}{12} \times 2 = 60^o`$

Let $`x`$ be the distance between the 2 hands. By using the law of cosines:

$`x^2 = 12^2 + 15^2 - 2(15)(12)\cos60`$

$`x = 13.7cm`$.

The distance between the 2 hands is $`13.7cm`$.

### Question 2 a)

Lets split the tree into the 2 triangles shown on the diagram. By using the primary trigonmetry ratios, we know that the bottom triangle's height side lenghts that is part of
the tree's height is $`100\tan 10`$, and $`100 \tan 25`$ for the top triangle.

Therefore the tree's height is the sum of these 2 triangle's side length.

Therefore the total height is $`100(\tan 25 + \tan 10) = 64.3`$

The height of the tree is $`64.3m`$

### Question 2 b)

$`\angle G = 180 - 35 - 68 = 77 (ASTT) `$

By using the law of sines.

$`\overline{RG} = \dfrac{173.2 \sin 35}{\sin 77} = 102m`$

By using the law of sines.

$`\overline{TG} = \dfrac{173.2 \sin 68}{\sin 77} = 164.8m`$

$`P = 173.2 + 164.8 + 102 = 440m`$

The perimeter is $`440m`$

### Question 2 c)

We know the buildings must be on the same side because they both cast a shadow from the same one sun.

Let the triangle formed by the flagpole be $`\triangle FPS`$ and the one by the building $`\triangle TBS`$

$`\because \angle B = \angle P`$ (given)

$`\because \angle S`$ is common.

$`\therefore \triangle TBS \sim \triangle FPS`$ (AA similarity theorem)

$`\dfrac{TB}{BS} = \dfrac{FP}{PS} \implies \dfrac{TB}{26} = \dfrac{25}{10}`$

$`\therefore TB = \dfrac{25(26)}{10} = 65`$

Therefore the building is $`65m`$ tall.

### Question 3

Let the triangle be $`triangle ABC`$, with $`AB = 1500`$ and $`BC = 4000`$. Let $`\angle A = \alpha`$ Then the angle of depression = $`\theta = 90 - \alpha`$ (CAT)

$`\tan(\alpha) = \dfrac{4000}{1500}`$

$`\alpha = \tan^{-1} (\dfrac{4000}{1500}) = 69.4`$

$`\therefore \theta = 90 - \alpha = 20.6`$

Therefore her angle of depression is $`20.6^o`$.

By pythagorean theorem, we know that $`\overline{AC}^2 = \overline{AB}^2 + \overline{BC}^2 \implies \overline{AC}^2 = 1500^2 + 4000^2 \implies \overline{AC} = 4272m`$. 

She flew about $`4272m`$ before touching down.

### Question 4 a)

Since they form a right triangle, we know the distance between them is the adjacent side to the $`55^o`$ angle. We also know that the hypotenuse is $`45m`$ long.

Therefore let $`x`$ be the distance. $`x = 45 \cos 55 = 25.8m`$.

They are $`25.8m`$ apart.

### Question 4 b)

Lets draw a 3D tetrahedron. Let $`KA = 75m, \angle KAB = 31, \angle BAK = 54, \angle BKT = 61`$. And $`K`$ be Ken's position, $`A`$ be Adam's position, $`B`$ be the base of the cliff, and
$`T`$ be the top of the cliff.

We first need to find $`KB`$, where then we can use primary trigonmetry ratios to find out $`BT`$, which is the height.

$`\angle B = 180 - 54 - 31 = 95`$ (ASTT)

By using the law of sines.

$`\dfrac{75}{\sin 95} = \dfrac{KB}{\sin A}`$

$`KB = \dfrac{75\sin 31}{\sin95} = 38.78`$

$`BT = 38.78 \tan 61 = 70m`$

The height of the cliff is 70m.


### Question 4 c)

For congruency:

SSS; When all 3 corresponding sides are the same

SAS; when 2 corresponding sides are the same, and one corresponding angle is the same.

ASA; when 2 corresponding angles are the same, and one corresponding side is the same.

For Similarity:

RRR; When the ratio of the 3 corresponding sides are all the same.

RAR: when the ratio of 2 of the 3 corresponding sides are all the same and one corresponding angle is the same.

AA When 2 corresponding angles are the same.

### Question 5 a)

The sinelaw is a relation between a triangles side length and the sine of its corresponding angle, which this relation has the same ratio as all 
the other sides with their corresponding angles.

You can use sinelaw when you have a oblique tiangle and you have at least 2 sides/angles and 1 angle/side.

### Question 5 b)


$`\because b\sin A \lt a \lt b`$

$`\because 5.3 \lt 7.5 \lt 9.3`$

There are 2 cases.

Let $`B^\prime`$ be the other possible point of $`B`$.

#### Case 1

$`\angle B = \sin^{-1}(\dfrac{5.3}{7.2}) = 47.4`$

$`\angle C = 180 - 35 - 47.4 = 97.6`$ (ASTT)

By using the law of cosines:

$`\overline{AB}^2 = 9.3^2 + 7.2^2 - 2(9.3)(7.2)\cos97.6 \implies \overline{AB} = 12.49mm`$

#### Case 2

$`\angle B^\prime = 180 - \angle B = 132.6`$ (SAT)

$`\angle C = 180 - 132.6 - 35 = 12.4`$ (ASTT)

By using the law of cosines.

$`\overline{AB}^2 = 9.3^2 + 7.2^2 - 2(9.3)(7.2)\cos12.4 \implies \overline{AB} = 2.74 mm`$


