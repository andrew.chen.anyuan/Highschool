## Quadratic Functions

### Question 1 a)

As $`a`$ varies, the graph stretches when $`a \gt 1 `$ and compresses when $`0 \lt a \lt 1`$

As $`p`$ varies, the graph moves to either the right (when $`p`$ is positive) or left (when $`p`$ is negative).

As $`q`$ varies, the graph moves either up (when $`q`$ is positive) or down (when $`q`$ is negative).

### Question 1 b)

I would first find the vertex which is equal to is at (AOS, optimal value), or $`(\dfrac{-b}{2a}, \dfrac{-b^2}{4a} + c)`$.

In this case it would be at $`(\dfrac{-13}{6}, \dfrac{-169}{12} + 4)`$

Then by using the step property, which is $`1a, 3a, 5a \cdots \implies 3, 9, 15 \cdots`$, I can plot the points on the graph. In addition, since $`a`$ is positive, the graph
will be opening upward.

### Question 2 a)

By plugging $`3`$ as the time into the relation $`h = -5t^2 + 100t`$, we get:

$`h = -5(3)^2 + 100(3) \implies h = -5(9) + 300 \implies h = 255`$

The flare will be $`255m`$ tall.

### Question 2 b)

The maximum height reached by the flare is when $`t = \dfrac{-b}{2a}`$ (optimal value).

So, $`\dfrac{-b}{2a} = \dfrac{-100}{-10} = 10`$

$`\therefore h = -5(10)^2 + 100(10) \implies h = 500`$

The maximum height reached by the flare is $`500m`$.

### Question 2 c)

By setting $`h=80`$, we can get the 2 times where the flare reaches $`80m`$, and by taking the difference in $`x`$ values, we get the time the flare stayed above $`80m`$.

$`80 = -5t^2 + 100t`$

$`5t^2 - 100t + 80 = 0`$

$`t^2 - 20t + 16 = 0`$

$`t = \dfrac{20 \pm \sqrt{366}}{2}`$

$`\therefore`$ The duration is $`2(\dfrac{\sqrt{336}}{2}) = \sqrt{336}`$

### Question 3 a)

We can represent the area as $`hw`$, where $`h+w = 20`$, so we can model a quadratic equation as such: $`w(20-w)`$. Therefore the AOS is when $`w=10`$

### Question 3 b)

Since the maximum area is when $`w = 10`$, and $`h = 20 - w \implies h = 10`$. So the dimension is a pen $`10m`$ by $`10m`$.

### Question 4

The cross-sectional area can be modeled by the equation $`(50-2x)x`$. 

Therefore the AOS is when $`\dfrac{25}{2}`$ since $`x=0, 25`$ are the solutions to this quadratic equation when it equals $`0`$, and the AOS is the average of them both.

Therefore the value of $`x=12.5cm`$ gives the maximum area for the sectional area.


