# Question 1

$`\because \angle B^\prime = \angle B \quad (\text{PLT-F})`$

$`\because \angle C^\prime = \angle C \quad (\text{PLT-F})`$ 

$`\therefore \triangle AB^\prime C^\prime \sim \triangle ABC  \quad (\text{AA } \sim) `$

$`\therefore \dfrac{AB^\prime}{B^\prime C^\prime} = \dfrac{AB}{BC} `$

$`\therefore \dfrac{30}{14} = \dfrac{30+x}{22}`$

$`14(30+x) = 22(30) `$

$`x = \dfrac{22(30)}{14} - 30 `$

$`x = 17.1428571 \approx 17.14 `$

$`\therefore \dfrac{AC^\prime}{B^\prime C^\prime} = \dfrac{AC}{BC} `$

$`\therefore \dfrac{y}{14} = \dfrac{y+15}{22} `$

$`22y = 14y + 14(15) `$

$`8y = 14(15) `$

$`y = 26.25`$

# Question 2

$`h = b \sin A`$

$`h = 11.3 \sin 32`$

$`h = 5.99`$

$`\because h \lt 6.8 \lt 11.3`$

$`\therefore 2 \triangle 's \text{ exist}`$

$`\text{ Lets call point } T  \text{ is the height that is perpendicular on side } AB \text{ and connects to point } C. \text { and } B^\prime \text{ be the other possible point of } B.`$

<img src="https://files.catbox.moe/hvluwl.png" width="1000">

-------------------------
$`\text{ Case } 1:`$

$`\angle CB^\prime T = \sin^{-1} \Bigl(\dfrac{5.99}{6.8} \Bigr)`$

$`\angle CB^\prime T = 61.75^o`$

$`\angle AB^\prime C = 180 - 61.75 = 118.25^o (\text{SAT})`$

$`\angle ACB^\prime = 180 - 118.25 - 32 = 29.75^o (\text{ASTT})`$

$`\dfrac{AB}{\sin \angle ACB^\prime} = \dfrac{CB^\prime}{\sin A}`$

$`\dfrac{AB}{\sin29.75} = \dfrac{6.8}{\sin 32}`$

$`AB = \dfrac{\sin 29.75 \times 6.8}{\sin32}`$

$`AB = 6.37`$

-------------------------
$`\text{ Case } 2: `$

$`\angle ABC = \angle CB^\prime T = 61.75^o (\text{ITT})`$

$`\angle ACB = 180 - 32 - 61.75 = 86.25^o (\text{ASTT})`$

$`\dfrac{AB}{\sin C} = \dfrac{CB}{\sin A}`$

$`\dfrac{AB}{\sin 86.25} = \dfrac{6.8}{\sin 32}`$

$`AB = \dfrac{\sin 86.25 \times 6.8}{\sin 32}`$

$`AB = 12.8`$

--------------------------

$`\therefore AB \text{ could either be } 6.37cm \text{ or } 12.8cm`$

# Question 3

$`\text{let the square be } \square ABCD \text{ and the inner triangle be } \triangle AEF `$

<img src="https://files.catbox.moe/ex7mea.png" width="1000">

$`\sin (\beta) = \dfrac{EF}{AE} = \dfrac{EF}{1} = EF`$

$`\sin (\alpha) \sin(\beta) = \dfrac{EC}{EF} \times \dfrac{EF}{AE} = \dfrac{EC}{AE} = \dfrac{EC}{1} = EC`$

$`\cos(\alpha) \sin(\beta) = \dfrac{CF}{EF} \times \dfrac{EF}{AE} = \dfrac{CF}{AE} = \dfrac{CF}{1} = CF`$

$`\text{Draw a parallel line to } CD \text{ that connects point } E \text{ to } AD. \quad \sin(\alpha + \beta) = \dfrac{CD}{AE} = \dfrac{CD}{1} = CD`$

$`\cos(\alpha) \cos(\beta) = \dfrac{AD}{AF} \times \dfrac{AF}{AE} = \dfrac{AD}{1} = AD`$

$`\sin(\alpha) \cos(\beta) = \dfrac{FD}{AF} \times \dfrac{AF}{AE} = \dfrac{FD}{1} = FD`$

$`\cos(\beta) = \dfrac{AF}{AE} = \dfrac{AF}{1} = AF`$

$`\text{Draw a parallel line to } CD \text{ that connects point } E \text{ to } AD. \quad \cos(\alpha + \beta) = \dfrac{BE}{AE} = \dfrac{BE}{1} = BE`$

$`\therefore \sin(\alpha + \beta) = CD = CF + FD = \cos(\alpha) \sin(\beta) + \sin(\alpha) \cos(\beta)`$

$`\therefore \cos(\alpha + \beta) = BE = \cos(\alpha - \beta)`$
## Footer Notes

$`\text{ASTT} = \text{ Angle Sum Of Triangle Theorem}`$

$`\text{ITT} = \text{ Isoceles Triangle Theorem}`$

$`\text{SAT} = \text{ Corresponding Angle Theorem}`$

$`\text{PLT-F} = \text{ Parallel Line Theorem; F-pattern}`$

$`\text{AA} \sim \space = \text{ Angle Angle Similarity Theorem}`$