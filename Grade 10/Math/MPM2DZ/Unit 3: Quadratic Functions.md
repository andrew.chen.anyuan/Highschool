# Unit 3: Quadratic Functions

## Definitions
 - `Linear relation`: A relation with a constant rate of change, or with a constant first difference
 - `Quadratic relation`: A relation where the 2nd differences are constant
 - `Non-linear relation`: A relation with a variable rate of change
 - `Degree` of a polynomial: Highest exponent that appears in any term of a polynomial
 - `a-value`: Step property
 - `Axis of symmetry`: Vertical symmetrical line through a parabola, also x-value for vertex
 - `Zeroes`: x-intercepts of parabola
 - `Vertex`: Maximum or minimum value on a parabola
 - `Optimal value`: y-value for vertex
 - `Domain`: List of all valid **x-values** for relation, expressed as $`D = {x, x_2, x_3}`$ or as a relation such as $`D = \{x \in \mathbb{R}, x =\not 0, x < 50\}`$
 - `Range`: List of all valid **y-values** for relation, expressed as $`R = {y, y_2, y_3}`$ or as a relation such as $`R = \{y \in \mathbb{R}, y =\not 0, y < 50\}`$
 - `Relation`: Set of ordered pairs of numbers
 - `Function`: A relation in which no y-coordinates share an x-coordinate (e.g., circles are not functions)

## Parabolas
A standard graph of a parabola $`y=x^2`$ would look something like this:

<img src="https://study.com/cimages/multimages/16/2f02bcfd-854d-486a-ab39-30961b3337c4_yx2.jpg" width="300">

There are several things you should know about a parabola:
- X-intercepts
- Y-intercepts
- Vertex
- Optimal value
- Axis of Symmetry
- Direction Of Opening
- Step Property


### X-intercepts
- These are the zeroes of the quadratic function, or the solutions you found when solving for a quadratic function in factored form.
- These are the values of $`x`$ where $`y=0`$. 
- By using factored form, we can easily see that an equation with factored form of $`y=a(x-r)(x-s)`$ has its x-intercepts at $`(r, 0), (s, 0)`$.

### Y-intercepts
- These are the values of $`y`$ where $`x=0`$.

### Vertex
- This highest/lowest value of $`y`$ that the parabola takes.
- This point tells us alot of things, including the `axis of symmetry` and the `maximum/minimum/optimal` value.
- With vertex form in $`y=a(x-h)^2 + k`$, we know that the vertex is at $`(h, k)`$.

### Optimal value
- This is basically the $`y`$ value of the vertex, and is useful for maximum/minimum word problems
- If the parabola is opening upwards, this is the minimum value. If the parabola is opening downards, this is the maximum value

### Axis of Symmetry
- A vertical line of symmetry for the parabola.
- This can be determined in many ways:
    - Using factored form $`y=a(x-r)(x-s)`$, the axis of symmetry is at $`\dfrac{r+s}{2}`$ 
    - Using standard form $`y=ax^2+bx+c`$, the axis of symmetry is at $`\dfrac{-b}{2a}`$
    - Using vertex form, $`y=a(x-h)^2+k`$, the axis of symmetry is simply the $`x`$ coordinate of the vertex, so $`h`$.

### Direction Of Opening
- To put it bluntly, the parabola opens upward if the $`a`$ value is positive, and downwards if the $`a`$ value is negative.
- Think of a postive $`a`$ value as a **happy face**, and a negative $`a`$ value as a **sad face**.

### Step Property
- This is a property that can be used to quickly graph a quadratic function.
- The step property for an $`a`$ value of $`1`$ is $`1, 3, 5, 7, \cdots 2n+1`$ for any $`n \ge 1`$. The step property for any other a value is $`1a, 3a, 5a, 7a, \cdots a(2n+1)`$ for any $`n \ge 1`$.
- The step property tells us the difference of values between each point starting from the vertex, meaing if the vertex is at $`(x, y)`$, <br> the next two points would be $`(x-1, y+n), (x+1, y+n)`$, where $`n`$ is the $`n^{th}`$ step property number.

## Functions
`Function`: A relation in which no y-coordinates share an x-coordinate (e.g., circles are not functions)

### Vertical Line Test (VLT)
We can easily tell if a relation is a function by using the vertical line test. If a single straight line of $`x=n`$ for any $`n`$ has more than $`1`$ point on the function, then that relation is not a function. Bluntly, a function cannot have a vertical straightline touching any $`2`$ of its points.

### Ways of Representing Functions

#### 1. Table Of Values
|$`x`$|$`y`$|
|:----|:----|
|$`2`$|$`5`$|
|$`5`$|$`7`$|
|$`6`$|$`5`$|

#### 2.  Coordinates in a Set
$`f = \{(-2, 1), (0, 1), (3, 1), (4, 1), (7, 1)\}`$

#### 3. Graph
<img src="https://d2jmvrsizmvf4x.cloudfront.net/5ptsdt6SVmzgiF5QPfeS_2016-01-11_203936.jpg" width="300">

#### 4. Mapping (Bubble Diagrams)
<img src="https://www.varsitytutors.com/assets/vt-hotmath-legacy/hotmath_help/topics/mapping-diagrams/diagram.gif" width="300">

#### 5. Equation
$`y=x^2-5`$

#### 6. Function Notation
$`f(x) = x^2 - 5`$

### Domain And Range
- A `domain` is the set of $`x`$-values, and the `range` is the set of $`y`$-values.
- To represent domain and range, we use **set notation** to represnt it or simply by listing the $`x`$ and $`y`$ values.
- We use listing method for representing points, such as:
    - <img src="https://s3-eu-west-1.amazonaws.com/functionsandgraphs/graph+with+all+points+except+F.png" width="400"> 
    - $`D = \{-3, -1, 0, 1, 2, 2.5 \}`$
    - $`R = \{-2, -1, 0.5, 3, 3, 5 \}`$
    - Make sure the values or sorted in order.
- We use **set notation** or **interval notation** to represent a continous graph, such as:
    - <img src="http://www.analyzemath.com/high_school_math/grade_11/quadratic/find_quadratic_f(x).gif" width="300">
    - $`D = \{x \mid x \in \mathbb{R} \}`$
    - $`R = \{y \mid y \ge 2, y \in \mathbb{R} \}`$

## Transformations
There are 3 main types of transformations for a quadratic function.

### Vertical Translation
- When we graph the quadratic relation $`y=x^2+k`$, the vertex of the parabola has coordinates $`(0, k)`$
- When $`k \gt 0`$, the graph of $`y=x^2`$ is **vertically** translated **up** $`\mid k \mid`$ units.
- When $`k \le 0`$, the graph of $`y=x^2`$ is **vertically** translated **down** $`\mid k \mid`$ units.

### Horizontal Translation
- When we graph the quadratic relation, $`y=(x-h)`$ the vertex of the parabola has coordinates $`(h, 0)`$
- When $`h \gt 0`$, the graph of $`y=x^2`$ is **horizontally** translated **left** $`\mid k \mid`$ units.
- When $`h \le 0`$, the graph of $`y=x^2`$ is **horizontally** translated **right** $`\mid k \mid`$ units. 

### Vertical Stretch/Compression
- If $`a \lt 0 \rightarrow`$, the graph is **reflected** over the x-axis.
- If $`\mid a \mid gt 1 \rightarrow`$ vertical stretch.expansion by a **factor of** $`\mid a \mid`$.
- If $`0 \lt \mid a \mid \lt 1 \rightarrow`$ vertical compression by a factor of $`\mid a \mid`$
- The step property also gets affected.

## Forms of Quadratic Functions
- A quadratic relation in the form $`y=a(x-r)(x-s)`$ is said to be in factored form. The zeroes are $`x=r`$ and $`x=s`$.
-The axis of symmetry can be determined by using the formula $`\dfrac{r + s}{2}`$
-The axis of symmetry is also **x-coordinate** of the vertex.

|Quadratic Form|Function|Zeroes|Vertex|Axis of Symmetry|
|:-------------|:-------|:-----|:-----|:---------------|
|**standard**|$`y=ax^2+bx+c`$|$`x= \dfrac{-b \pm \sqrt{b^2-4ac}}{2a}`$|Plug in Axis of **symmetry** and solve for $`y`$|$`x = \dfrac{-b}{2a}`$|
|**vertex**|$`y=a(x-h)^2+k`$|Set $`y=0`$, solve for $`x`$|$`(h, k)`$|$`x = h`$|
|**factored**|$`y=a(x-r)(x-s)`$|$`(r, 0), (s, 0)`$|Plug in Axis of **symmetry** and solve for $`y`$|$`x = \dfrac{r+s}{2}`$|

- **Notice** that the $`a`$ value **stays the same** in all the forms.
- By using **completing the square**, we can find that the vertex is at $`(\dfrac{-b}{2a}, c - \dfrac{b^2}{4a})`$, where $`a =\not 0`$

## Partial Factoring
- If $`y=ax^2+bx+c`$ cannot be factored, then we can use partial facotring to determine the vertex. 
- We set $`y =c`$, then we basically now stated that $`0 = ax^2 + bx`$. Since there is no constant value, we can factor the equation to becoming $`x(ax + b)`$, from where we can solve for the values of $`x`$.
- In respect of the axis of symmetry, using the $`2 \space x`$ values we can find the axis of symmetry, by using the formula $`\dfrac{r+s}{2}`$, since the axis of symmetry works for any 2 opposite points on the parabola.
- With this, we can easily find the vertex of a quadratic equation.

## Quadratic Inequalities
- Quadratic inequalites can be solved **graphically** and **algebraically**. Since we know how to graph quadratic relations, we can solve quadtratic inequalites **graphically**
- An example of an **algebraic** solution of an quadratic inequality would be the **number line method** we learned in the previous unit.
- To solve it **graphically**, we will need to put the equation into factored form, then finding out the x-intercepts. With these, we can find out the side of the graph the solution is on. 
- If the solution is greater than $`0`$, then it would be denoting the values that are above the x-axis, and below the y-axis for the opposite. 

