# Unit 4: Trigonometry

## Angle Theorems

1. ```Transversal Parallel Line Theorems``` (TPT)     
    a. Alternate Angles are Equal ```(Z-Pattern)```   
    b. Corresponding Angles Equal ```(F-Pattern)```    
    c. Interior Angles add up to 180 ```(C-Pattern)```      

  - <img src="https://dj1hlxw0wr920.cloudfront.net/userfiles/wyzfiles/58a52a99-05da-4595-88b8-2cbca91e8bbf.gif" width="300">   

2. ```Supplementary Angle Theorem``` (SAT)     
  - When two angles add up to 180 degrees     

  - <img src="https://embedwistia-a.akamaihd.net/deliveries/cdd1e2ebe803fc21144cfd933984eafe2c0fb935.jpg?image_crop_resized=960x600" width="500">   

3. ```Opposite Angle Theorem (OAT)``` (OAT)    
  - Two lines intersect, two angles form opposite. They have equal measures    

  - <img src="https://images.slideplayer.com/18/6174952/slides/slide_2.jpg" width="400">   

4. ```Complementary Angle Theorem``` (CAT)    
  - The sum of two angles that add up to 90 degrees     

  - <img src="http://images.tutorvista.com/cms/images/67/complementary-angle.png" width="300">   

5. ```Angle Sum of a Triangle Theorem``` (ASTT)   
  - The sum of the three interior angles of any triangle is 180 degrees    

  - <img src="https://dj1hlxw0wr920.cloudfront.net/userfiles/wyzfiles/f0516fa1-669b-441d-9f11-a33907a2a0b0.gif" width="300">   

6. ```Exterior Angle Theorem``` (EAT)
  - The measure of an exterior angle is equal to the sum of the measures of the opposite interior angles      

  -<img src="https://www.katesmathlessons.com/uploads/1/6/1/0/1610286/exterior-angle-theorem-diagram-picture_orig.png" width="300">

7. ``` Isosceles Triangle Theorem``` (ITT)   
  - The base angles in any isosceles triangle are equal   

  - <img src="http://www.assignmentpoint.com/wp-content/uploads/2016/06/isosceles-triangle-theorem.jpg" width="400">

8. ```Sum of The Interior Angle of a Polygon```
  - The sum of the interioir angles of any polygon is ```180(n-2)``` or ```180n - 360```, where ```n``` is the number of sides of the polygon

  - <img src="https://i.ytimg.com/vi/tmRpwCM1K1o/maxresdefault.jpg" width="500">


9. ```Exterior Angles of a Convex Polygon```
  - The sum of the exterior angle of any convex polygon is always ```360 degrees```   

  - <img src="https://image.slidesharecdn.com/findanglemeasuresinapolygon-110307143453-phpapp02/95/find-angle-measures-in-a-polygon-11-728.jpg?cb=1299508555" width="400">

## Congruency
`Congruent`: Same size and shape

### Side-Side-Side (SSS)

If three sides of a triangle are respectively equal to the three sides of another triangle, then the triangles are congruent

<img src="https://media.cheggcdn.com/study/51f/51f6fea6-a0df-4da9-ad5c-f9663291a22f/DC-2411V1.png" width="500">

### Side-Angle-Side (SAS)
If two sides and the **contained** angle of a triangle are respectively equal to two sides and the **contained** angle of another triangle, then the triangles are congruent.

<img src="http://questgarden.com/92/84/2/091202221850/images/congruence_sas.gif" width="500">

### Angle-Side-Angle (ASA)
If two angles and the **contained** side of a triangle are respectively equal to two angles and the **contained** side of another triangle, then the triangles are congruent.

<img src="https://www.onlinemath4all.com/images/trianglecongruenceandsimilarity4.png" width="500">

## Similar Triangles
`Similar`: Same shape but different sizes (one is an enlargement of the other)

### Properties

Lets say we have $`\triangle ABC \sim \triangle DEF`$
1. Corresponding angles are **equal**
- $`\angle A = \angle D`$
- $`\angle B = \angle E`$
- $`\angle C = \angle F`$

2. Corresponding side are **proportional**.
- $`\dfrac{AB}{DE} = \dfrac{AC}{DF} = \dfrac{BC}{EF}`$

3. Proportional Area
- Let $`k`$ be the **scale factor**, when concerning for triangle area, if the triangle area can be defined as $`\dfrac{bh}{2}`$, then by using the smaller triangles side lengths
our big triangle's area is equal to $`\dfrac{k^2bh}{2}`$. Similar equations and agruments can be dervied from this

### Side-Side-Side similarity (RRR $`\sim`$)

Three pairs of corresponding sides are in the **same ratio**

<img src="https://docs.google.com/drawings/d/snd5DSjJuOz9Lql5RgzUxCw/image?parent=1ltNI2q_ajTaJyAGt7C7GLY0uwh9LbBOfjW1B4Og_KwM&rev=59&h=188&w=398&ac=1" width="500">

### Side-Angle-Side similarity (RAR $`\sim`$)
Two pairs of corresponding sides are proportional and the **contained** angle are equal.

<img src="http://804369586450478528.weebly.com/uploads/4/5/2/6/45266747/775263614.png?367" width="400">

### Angle-Angle similarity (AA $`\sim`$)
Two pairs of corresponding angles are equal. In the diagram below, we can solve for the missing angle using Angle Sum Of A Triangle Theorem (ASTT) and see that those 2 triangle's angles are equal.

<img src="https://www.onlinemath4all.com/images/angleanglesimilarity2.png" width="500">



## Primary Trigonometric Ratios

|Part Of Triangle|Property|
|:---------------|:-------|
|Hypotenuse|The longest side of the right triangle. it is across the $`90^o`$ (right angle)|
|Opposite|The side opposite to the reference angle|
|Adjacent|The side next to the reference agnle|

**Remember**: Primary trigonometric ratios are only used to find the **acute** angles or sides of a **right-angled** triangle

### SOH CAH TOA

**SINE** $`\sin \theta = \dfrac{\text{Opposite}}{\text{Hypotenuse}}`$

**COSINE** $`\cos \theta = \dfrac{\text{Adajacent}}{\text{Hypotenuse}}`$

**TANGENT** $`\tan \theta = \dfrac{\text{Opposite}}{\text{Adajacent}}`$

## Angle Of Elevation And Depression

|          |Angle of Elevation|Angle of Depression|
|:---------|:-----------------|:------------------|
|Definition|**Angle of Elevation** is the angle from the horizontal looking **up** to some object|**Angle of Depression** is the angle frorm the horizontal looking **down** to some object|
|Diagram|<img src="https://www.varsitytutors.com/assets/vt-hotmath-legacy/hotmath_help/topics/angle-of-elevation/angle-of-elevation-image001.gif" width="300">|<img src="https://www.varsitytutors.com/assets/vt-hotmath-legacy/hotmath_help/topics/angle-of-elevation/angle-of-elevation-image002.gif" width="300">|


We can see that **Angle of Elevation = Angle of Depression** in the diagram below (Proven using Z-pattern)

<img src="https://www.varsitytutors.com/assets/vt-hotmath-legacy/hotmath_help/topics/angle-of-elevation/angle-of-elevation-image003.gif" width="300">

## Sine Law

In any $`\triangle ABC`$: $`\dfrac{\sin A}{a} = \dfrac{\sin B}{b} = \dfrac{\sin C}{c}`$ or $`\dfrac{a}{\sin A} = \dfrac{b}{\sin B} = \dfrac{c}{\sin C}`$

We can derive the formula further to get: 
- $`\dfrac{\sin A}{\sin B} = \dfrac{a}{b}`$
- $`\dfrac{\sin A}{\sin C} = \dfrac{a}{c}`$
- $`\dfrac{\sin B}{\sin C} = \dfrac{b}{c}`$

Also, for some trigonometry identities:
- $`\tan x = \dfrac{\sin x}{\cos x}`$
- $`\sin^2 A + \cos^2 A = 1`$

**If you are finding the sides or agnles of an `oblique triangle` given 1 side, its opposite angle and one other side or angle, use the sine law.**

### Ambiguous Case
The ambiguous case arises in the SSA or (ASS) case of an triangle, when you are given angle-side-side. The sine law calculation may need to give 0, 1, or 2 solutions.

In the ambiguous case, if $`\angle A, a, b`$ are given, the height of the triangle is $`h= b\sin A`$


|Case|If $`\angle A`$ is **acute**|Condition|# & Type of triangles possible|
|:---|:---------------------------|:--------|:-----------------------------|
|1   |<img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSQfCqnJUFFQ9UQHoYT-CXd614RTXNGL_R5QNomKDCaDV3Nja4g&s" width="200">|$`a \lt h`$|no triangle exists|
|2   |<img src="https://www.analyzemath.com/Triangle/sine_law_2.gif" width="300">|$`a = h`$|one triangle exists|
|3   |<img src="http://www.technologyuk.net/mathematics/trigonometry/images/trigonometry_0078.gif" width="300">|$`h \lt a \lt b`$|two triangle exist (one acute triangle, one obtuse triangle)|
|4   |<img src="http://jwilson.coe.uga.edu/EMT668/EMAT6680.2001/Mealor/EMAT%206700/law%20of%20sines/Law%20of%20Sines%20ambiguous%20case/image3.gif" width="300">|$`a \ge b`$|one triangle exists|

|Case|If $`\angle A`$ is **obtuse**|Condition|# & Type of triangles possible|
|:---|:----------------------------|:--------|:-----------------------------|
|5   |<img src="http://jwilson.coe.uga.edu/EMT668/EMAT6680.2001/Mealor/EMAT%206700/law%20of%20sines/Law%20of%20Sines%20ambiguous%20case/image5.gif" width="200">|$`a \le b`$|no triangles exist|
|6   |<img src="https://www.mathopenref.com/images/constructions/constaltitudeobtuse/step0.gif" width="300">|$`a \gt b`$|one triangle exists|


## Cosine Law

In any $`\triangle ABC`$, $`c^2 = a^2 + b^2 - 2ab\cose C`$

**If you are given 3 sides or 2 sides and the contained angle of an `oblique triangle`, then use the consine law**

## Directions

`Bearings`: **Always** start from **North**, and goes **clockwise**
`Direction`: Start from the first letter (N, E, S, W), and go that many degrees directly to the second letter (N, E, S, W)

**Note:** Northeast, southeast, northwest etc. all have 45 degrees to the left or the right from their starting degree (0, 90, 180, 270)

## 2D Problems
**Note:** Watch out for the case where you don't know which side the 2 things (buildings, boats, etc.) are, they can result in 2 answers

## 3D problems
**Note:** Use angle theorems to find bearing/direction angle, and to help with the problem in general. Apply sine law, cosine law, and primary trigonometric ratios whenever necessary.


