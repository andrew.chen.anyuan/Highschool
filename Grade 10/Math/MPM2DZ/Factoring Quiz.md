# Level 1

## $`2x^4 - 32`$
- $`= 2(x^4-16)`$
- $`= 2((x^2-4)(x^2+4)`$
- $`= 2((x+2)(x-2)(x^2+4)) \\`$

## $`2x^2+11x-21`$
- $`=2x^2+11x-21`$
- $`\quad \quad 2 \quad -3`$
- $`\quad \quad 1 \quad \quad 7`$
- $` = (2x+3)(x+7)`$

## $`x^3y^2 + 2x^2y^2-3xy^2`$
- $`=xy^2(x^2+2x-3)`$
- $`\quad \quad \quad \quad 1 \quad -1`$
- $`\quad \quad \quad \quad 1 \quad \quad 3`$
- $`= xy^2(x-1)(x+3)`$

# Level 2

## $`4x^4-9x^2+30x-25`$
- $`=4x^4-(9x^2-30x+25)`$
- $`\quad \quad \quad \quad \quad \quad 3 \quad -5`$
- $`\quad \quad \quad \quad \quad \quad 3 \quad -5`$
- $`= 4x^4 - (3x-5)^2`$
- $`= (2x^2-3x+5)(2x^2+3x-5)`$
- $`\quad \quad \quad  \quad \quad \quad  \quad \quad \quad 2 \quad \quad 5`$
- $`\quad \quad \quad \quad \quad  \quad \quad \quad  \quad 1 \quad -1`$
- $` = (2x^2-3x+5)(2x+5)(x-1)`$

## $`3(x-y)^2 - 5x + 5y - 2`$
- $`= 3(x-y)^2 -5(x-y) - 2`$
- $`\text{Let } a = (x-y)`$
- $`= 3a^2 - 5a - 2`$
- $` \quad \quad 3 \quad \quad 1`$ 
- $` \quad \quad 1 \quad -2 `$
- $`= (3a+1)(a-2)`$
- $`=(3(x-y)+1)(x-y-2)`$
- $`=(3x-3y+1)(x-y-2)`$

# Level 3

## $`x^8 - 17x^4 - 48`$
- $`\text{Let } y = x^4`$
- $`= y^2 - 13y - 48`$
- $`\quad \quad 1 \quad \quad 3`$
- $`\quad \quad 1 \quad -16`$
- $`=(y+3)(y-16)`$
- $`=(x^4+3)(x^4-16)`$
- $`=(x^4+3)(x^2-4)(x^2+4)`$
- $`=(x^4+3)(x-2)(x+2)(x^2+4)`$

## $`10(3x-5)^4(2x+3)^5 + 4x^2(3x-5)^3(2x+6)^6`$
- $`=(2x+3)^5(3x-5)^3(10(3x-5) + 4x^2(2x+3))`$
- $`=(2x+3)^5(3x-5)^3(30x+50+8x^3+12x^2)`$
- $`=(2x+3)^5(3x-5)^3(2(4x^3+6x^2+ 15x -25))`$