# Unit 5: Rational Expressions

## Rational Exponents

`Power Form:` $`\large a^{\frac{m}{n}}`$

`Radical Form:` $`n \sqrt{a^m} = (n \sqrt{a})^m`$

`Powers with negative rational exponents:` $`\large{a}^{\frac{-m}{n}} = \dfrac{1}{a^{\frac{m}{n}}} = \dfrac{1}{a^{n\sqrt{a^m}}}`$

`Powers with negative exponents:` $`a^-n = \dfrac{1}{a^n}`$ 

**Notes:** When dealing with power form, always reduce the exponent if you can.
- $`(-2)^{\frac{2}{4}} \rightarrow (-2)^{\frac{1}{2}}`$

## Solving Exponential Equations

Eg. $`5^{x+1} = 125`$

1. Change expressions on both sides to the **SAME BASE** and simplify the exponent(s). $`5^{x+1} = 5^3`$
2. Equate the exponents. $`{x+1} = 3`$
3. Solve for the variable and checks solutions, if required. $`x+1 = 3 \implies x = 2`$

## Restrictions Of A Rational Expression

Basically the denominator of a fraction can never be $`0`$. Therefore, we have to put restrictions on the variables of the denominator such that the final result of
the denominator is not equal to $`0`$.

### Steps To Simplify Rational Expressions

1. Factor fully
2. Divide common factor
3. State restrictions of **original** expression.


## Multiplying And Dividing Rational Expressions

Multiplying:
1. Factor the numerators and denominators
2. State ALL restrictions on the variables
3. Using division, remove any common factors in the numerator and denominator
4. Multiply the numerators, then multiply the denominators
5. Write the result as a single expression

Dividing:
1. Factor the numerators and denominators
2. State all restrictions on the variables
3. Take the reciprocal of the second rational expression and change the $`\divide`$ to $`\times`$
4. State any NEW restrictions (When you try to flip the fraction, the denominators of the original and new fraction must be considered)
5. Using division, remove any common factors in the numerator and denominator
6. Multiply the numerators, then multiply the denominators
7. Write the result as a single expression

## Adding And Subtracting Rational Expressions
1. Factor the denominator.
2. State the restrictions on the variables.
3. Determine the lowest common denominator.
4. Write each expression with the common denominator.
5. Add or subtract the numerators. (Combine them into one large expression)
6. Expand the numerator.
7. Simplify the numerator by combining like terms.
8. Factor the numerator, if factorable.
9. Divide out common factors.

**Notes:** The LCD is not always the product of the denominators. To determine:
- Factor the denominators, find common coefficients, find a term that contains all the **UNIQUE** factors.


