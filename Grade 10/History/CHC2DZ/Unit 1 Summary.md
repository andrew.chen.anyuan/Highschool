﻿# History Unit 1
### By Andrew Chen
Unit 1 focuses in on the different causes and battles in World War 1. This note will have many different sections all of which will be tested.
## Causes of World War 1
### Militarism
 * an arms race was occurring between the British and the Germans
 * dreadnoughts were being built up by both the British and Germans
 * spending on the military increased 300% in the main European countries between 1870 and 1914
### Alliances
 * the **Triple Entente** formed between _Britain_, _Russia_, and _France_
 * the **Triple Alliance** formed between _Germany_, _Austria-Hungary_, and _Italy_
### Imperialism
 * industrialized countries needed **markets** for their goods in the form of colonies
 * Germany believed that overseas colonies would be a **sign of great power** to the rest of Europe
 * Austria-Hungary also wished to expand its empire through conquering Serbia and the rest of the Balkan states. It recently annexed Bosnia angering Russia and Serbia.
### Nationalism
 * Russia was defeated by Japan in the Russo-Japanese War and wanted to **prove she was still powerful**
 * Italy was  newly unified and wanted to **declare its power** in Europe
 * the Austrian empire was made up of **many different nationalities** who all wanted to separate from Austria.
### The Assassination of Franz Ferdinand
 * Archduke Franz Ferdinand, heir apparent to the Austro-Hungarian empire was assassinated **in Sarajevo by Gavrilo Princip** on June 28th, 1914
 * This was orchestrated by the **Black Hand**, an organization from Serbia
 * An ultimatum was sent to Serbia from the Austro-Hungarian empire with the following demands:
	1. Stop all hatred against Austro-Hungary
	2. Punish all of those involved in the assassination
	3. **Allow Austro-Hungarian police to destroy the Black Hand** (this was not acceptable)

 * After this ultimatium was rejected, **Russia mobilized** to protect Serbia. **Germany** responded by **declaring war on Russia** to protect Austria-Hungary. They then **declared war on France**, Russia's ally. 
 * In the invasion against France, Germans **go through neutral Belgium**, triggering Britain (Canada included) to **join the war on the side of France and Russia**. World War 1 was now in full swing.
## Canada's Entry into World War 1
### Initial Reaction
 * Borden's government passed the **War Measures Act** giving the government the powers of 
	* censorship
	* price and wage controls
	* pass laws without parliament
	* arrests, detentions, and deportations without trial
### Sam Hughes
* appointed by Borden to recruit and train new troops
* he set up **Valcartier Camp** in a few weeks near Quebec City which was built to house soldiers before they headed to Europe
* he helped Ontario to industrialize
* he had four main contreversies through his tenure
	1. **Shovel shield** - a tool that was ineffective at being a shovel or shield and was eventually sold for scrap
	2. **Ross rifle** - a rifle that was ineffective and jammed in mud
	3. **Munitions production** was of low quality leading to accusations of corruption
	4. **Religious bigotry** against the French, leading to lower enlistment
### Minorities in World War 1
#### Japanese and Chinese Canadians
* strong desire for a vote and acceptance in the country
#### No.2 Construction Battalion
* informal segregation of Black Canadians made it difficult to join the army
* this non-combatant battalion was formed as the first black battalion in Canadian history
#### First Nations, Metis, and Inuit 
* joined for 
	* a regular wage
	* adventure
	* friends and family had enlisted
	* travel the world
	* honor the relationship set in the War of 1812 between the Indigenous and the Crown
	* chance to be a warrior
* the Indian act made it so that those who fought in World War 1 and lived off the reserves could vote and were expected to pay taxes
* many FNMI became snipers or scouts
### Misc. Terms 
* **Social Darwinism** provided scientific explanation and social justification for inequality and exploitation
* **Imperialism** caused belief in white superiority and that the British principles of government were the superior ones

## Battles and Strategies of World War 1
### Schlieffen Plan
* put into effect to try to **avoid a two-front war**
* to avoid a war with France and Russia simultaneously, Germany would send **90% of its troops towards French territory** thru the Low countries, quickly defeating France
* they believed that the **Russians could not mobilize** and attack Germany's Eastern border fast enough, and that the entire army would be back from defeating France fast enough to defend against Russia

	| Expectations        | Reality| 
	| :-------------: |:-------------:| 
	| Russia would take 6 weeks to mobilize      | Russia mobilized in just 10 days |
	| France would be defeated in 6 weeks     | The war lasted 4 years|
	| Belgium would not resist | The Germans were held up by the Belgian Army    |  
	| Britain would not be involved| They joined right away upon hearing of the Belgian invasion    |  
### Trench Warfare

#### How did Trench Warfare come to be?
* after the Schlieffen Plan failed, the Germans implemented trench warfare to stop the British and French armies
	* **a war of attrition;** both sides tried to wear down the other 
* military leaders tried to end trench warfare by attempting a breakthrough, but to no avail 

#### Features of Trench Warfare
* many commanders of the British forces employed the usage of **over-the-top**, a strategy that involved rushing at the enemy trenches
* **duck boards** were wood boards nailed together to form a bridge over mud
* **no-man's land** was the area between the opposing trenches
* **mud, lice, and rats** were everywhere in the trenches
	* rats contaminated food
	* lice caused trench fever
	* mud caused trench foot, an ailment that rendered the foot numb, swelling, and smelly
* barbed wire was a defensive construct used to stop infantry
* **shell shock** was a nervous disorder resulting from exposure to artillery bombardment
#### Other Military Technologies 
* artillery
	* fired different types of shells including ones that contained shrapnel and explosives
* chemical warfare 
	* **chlorine gas** caused the lungs to fill with pus, leaving the victim to drown
* airplanes
	* allowed reconnaissance flights
	* guns eventually became a presence on these flights, with the Germans developing a device that allowed a gun to fire without hitting a propeller
* tanks 
	* counter to machine guns, but often got stuck in the mud
* u-boats
	* submarines used to prevent countries to reach supplies 

### Battles of World War 1

#### Second Battle of Ypres
* **first usage of chlorine gas** in World War 1
* no ground was gained or lost
* _In Flanders Fields_ was written during this battle
* this was the **first battle in which Canadians participated**

#### Battle of the Somme
* designed to relieve the French offensive at Verdun
* joint British and French offensive
* **massive amounts of losses** on both sides
* little land (about 8km) was gained by the Allies
* introduction of the **tank** as a counter to machine guns
#### Battle of Vimy Ridge
* carefully planned attack that was rehearsed beforehand with infantry given maps and specific tasks
* engineers dug tunnels to minimize areas where soldiers could be killed
* **first successful usage of the creeping barrage** where soldiers would advance behind a wave of artillery
* this battle was lead by **Julian Byng** of Britain and **Arthur Currie** of Canada
	* Byng was originally the leader of the Canadian corps but after Vimy Ridge, **Currie was given full reins of the Canadian corps**
#### Passchendaele
* only 6km of land was gained, with many casualties
	* military victory for the British, but **massive morale loss for Britain**
* Haig was convinced that the Battle of Passchendaele would push a German army close to collapse to fall
* the bombardment by the British destroyed the drainage system which was combined with **heavy rain and resulted in mud everywhere**
#### Canada's Hundred Days
* Allied capture of Mons, spearheaded by the Canadians
* Breach of the Hindenberg line 
* involved the breach of the Canal du Nord by Currie, a plan that was extremely daring but given the seal of approval by **General Douglas Haig** of the British Army
* Canadian troops were truly marked as elite here
	* Canadian corps launched a series of attacks that broke German lines
#### Hindenberg Line
* German defensive position built between 1916 and 1917
* parts of it were held until the end of the war
* **many parts captured** during the Battle of Arras including Vimy Ridge

### Turning Points
#### Russian Departure from the War
* in February of 1917, Russia leaves the war as Nicholas II is overthrown
* the **treaty of Brest-Litvosk** in March 1918 gives a large amount of land to Germany and Russia is able to leave the war
* **Lenin's communist revolution** begins in full swing
#### Americans Joining the War
* Germans employed a strategy called **unrestricted submarine warfare** after it had been blockaded by Britain
* through this strategy, they would destroy the **Lusitania** which was a British ship carrying American passengers
	* this would prompt **American entry into the war** on the side of Britain and its allies
### The Home Front
#### Conscription
* where all men of age 20-45 and good health are forced to go fight in war
* Borden uses the **War Measures Act** to pass two other acts
	* the **Wartime Elections Act** gives votes to women and takes away votes from Ukrainians and Germans (deemed aliens)
	* the **Military Voters Act** gave votes soldiers overseas in any riding while taking the vote away from conscientious objectors and pacifists
* Borden wants conscription to pass so that Canada can win the war faster and gain a voice in post-war politics
* farmers wanted to stay in Canada and produce food instead of fighting
	* Borden temporarily granted this request, but later reversed the decision
* status First Nations peoples were exempt from serving as they were not citizens of Canada
	* they could still be called to non-combatant roles in Canada
* French-Canadians were reluctant to serve as they were viewed as below the English-speaking troops
	
#### Propaganda and Paying for the War
* the government paid for the war through a variety of methods
	* taxes on business
	* personal income tax
	* victory bonds which would only be paid out if Canada won the war
* they used propaganda heavily to promote **Victory bonds**

#### Women and their Role in Society
* women were a major part of workforce for the first time in World War 1
	* they had lower wages
	* the unions didn't want them
	* fired for returning soldiers after the war
	* bad hygiene standards
* women that were related to soldiers or who were nurses were given the right to vote in 1917 through the Wartime Election Act
	* all women were given the right in 1918 with the passing of the Federal Women's Franchise Act
### After the War
#### Peace
* the **Paris Peace Conference** in 1919 was held to discuss terms of peace
	* gathering of 32 nations, including Canada
	* Canada could not vote but had 2 seats
* the big three people calling shots were
	* **Georges Clemenceau** of France
		* wanted to ensure protection of the French
	* **David Lloyd George** of Britain
		* increase British holdings in terms of colonies
	* **Woodrow Wilson** of the USA
		* wanted his fourteen points to be passed
* Germany had to make many concessions after World War 1
	* restricted army size
	* war reparations through the **War Guilt Clause**
	* lost territory in many areas of the world
	* forbade union with Austria
	* forbade troops in the Rhineland
	* the port of Danzig became a "free city" as Poland was given access to the sea
	* Sudetenland was given to Czechslovakia even though it was a German majority region
* The **Fourteen Points** by Wilson were the basis of the Treaty of Versailles, however **much of it was left out** as others viewed it as **idealistic**
* unsatisfactory peace, as the **Germans had agreed to the Fourteen Points, a much less harsh peace**

#### The Economy
* the economy after World War 1 was not very healthy
	* **unemployment** caused by the closing of munitions factories
	* **huge debts** that would take years to pay off
* Germany had the brunt of the economic costs, as the **War Guilt Clause** forced them to **continue paying reparations** until 1984
	* the German economy became extremely unstable, leading to **hyperinflation** by 1922
* **quality of life** in Canada after the war was **poor**
	* **unemployment rose** as the war ended
	* food and fuel were more expensive and in short supply
	* **wages were low, and prices were high**
* the veterans were not treated well when they came back
	* expected to go back to their civilian responsibilities with **very limited support** from the government
	* many of them could not go back due to **shell shock or PTSD**
* many workers wanted to have a **partnership of unions in the form of the One Big Union**
	* this union would have **enormous bargaining power** with employers, making it **easier to achieve their demands**
* Canadian workers were unsatisfied and began to **demand three things**
	* recognition of their unions
	* 8 hour workdays
	* improved wages
	* these were not given by employers, and thus lead to the Winnipeg General Strike
#### Winnipeg General Strike
* members of Winnipeg's building-trade union went on strike
	* they were soon joined by many different workers from different industries
	* **this became a general strike**, one that involved every non-essential worker 
* the strike disrupted daily life in Winnipeg and was strongly opposed by the **Citizen's Commission of 1000**
	* made up of business owners, politicians, and bankers
	* they spread propaganda that this was a Communist revolution and hired **scabs** to replace workers
* On June 21st, 1919, **Bloody Saturday** occurred where RNWMP officers on horseback charged into a protest
	* the **army occupied the city**, and workers **went back to their jobs** 
* the Winnipeg general strike did lead to **positive changes later on** with laws recognizing the rights of workers and strike organizers becoming politicians
	* **J.S Woodsworth**, an organizer, became a founding member of the NDP
#### Spanish Flu
* **influenza** epidemic caused by a strain called H1N1
* no historical data showing where it started
* killed **young healthy adults**
* started in March 1918 and ended in June 1920
* called Spanish Flu as the **Spanish press were the first to report on it**
	* all the other countries affected involved in World War 1 had censored the event
* 20 to 100 million killed
	* the flu caused an overreaction in the immune system leading to death
