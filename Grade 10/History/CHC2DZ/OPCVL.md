# OPCVL


## Sources
Reasons they are kepy:
- kept for a **purpose**, someone wanted to keep them
- preserved by **luck, accident or chance**

Reasons they are lost
- because people want to **destroy them**
- lost by **accident**
- **hidden** because they do not suit **popular beliefs** (such as Canadians bombing German Civilians during WW2)

## Type Of Sources

- **Primary Sources**: Are from the time period and have not been intepreted or altered by anyone. Examples include WW1 pictures, a solider's diary, etc.
- **Secondary Sources**: Are based off `primary sources`, which **may** or **may not** be from that time period. (Eg. reported interview witnesses of WW1, and writes up a report, or a book based on a solider's letter etc.)
- **Tertiary Sources**: Are based off `secondary sources`, which is basically sources that generalize things. The **LEAST** useful source.

## Content and Context
- Understanding conent means you understand exactly what information is included in the source. Content can be explicit, or implicit where you have to make inferences/implied/assumed.
- It is just as important to understand **content** as to **context**

## Bias
- a charateristic of a source or the one who created it that prevents the source from being reliable or trustworthy
- Although it is less important, if we can recongize the bias, it can still be useful.
- finding sources with alternate viewpoints can help us minimaze or elimniate bias.

### Intentional Bias
- When the source use emotional images or loaded words to persuade you of something.
- When the intent of the author is to persuade you to do something.
- The author could be only presenting **part of the** information and use other tatics to persuade you.

### Limted Access To Information
- The person creating the source may not have all the details/viewpoints.
- Eg general and solider viewpoints of a battle may be different

### Type of Source Bias
- The type of source can act as a bias
- eg. How you judge someone based on their report cards and diary may be completely different
    - neither will give a complete picture of who that person is
- Poem can be used to create more emotional responses
    - this also falls under lingustic bias
    
### Linguistic Bias
- The use of diction and languge may be intentionally or by accident used to create emotional responses in the reader to favor one side more.
- eg He treated the `innocent` young girls differently than the `aggressive` boys
- The way the words are written (eg. put in parentheses, brackets, separated, punctuation)
- Use "all or nothing" statements (Canadians are tolerant, women are gentle)

## Presentism
- is when modern historians judge people of the past using today's standards. This amy be making **moral** or **ethical** judgements, judgements about the knowledge of the poeple of the past, or judgements about how unimportant or irrelevant they were.
- Eg. the nazi symbol represented peace before the time of the nazi's, and if we saw people then wearing them, we would be offended because its associated with the nazi's, while in their time it didn't mean such a thing.

## Historiography
- the study of the way history has been written
- eg. The northern historian may see the American civil war as an inevitable outcome of slavery; while southern historian may view the civil war as an attempt by the arrogan north to impose its will on the south.
- study of sources in general

## Source Analysis

- **Content**: What is the source about? What is the creator or author trying to tell you? What is the main point?
- **Context**: What do you need to know about the time period, author or subject to better understand it?
- **Origins**: Where, when was this created? Who made it? What is it? How was it made (5 Ws)
- **Purpose**: Why was this source produced?
- **Values**: In the perspective of an historian, how is this piece of source valuable in answer historical questions?
- **Limitations**: As an historian, what are the problems that I need to consider when using this source in answering historical questions?

## Source Anaylsis Guide

### Origin 
- `Origin` refers to the author -> state **where** he/she is from, the position they held, **when** the source was produced.
- eg. Diary of solider, you should specifiy the nationality as the perspectives would be different. 
- The time of the source also impacts the `values and limitations` of the source as well

Questions to ask:
- who is the author?
- when was it created?
- when was it published?
- where was it published?
- who is publishing it?
- is there anything we know about the author that is pertinent to our evaluation?
    - very important question, the more you know about the author, the better understand of the context you will have and you will have a better understand of the content as well.
    

### Purpose
- Basically why the source was produced
- eg. a speech may have been produced to rally the troops so they'll continue to fight in the war
- Once you discovered the **origins** and **purpose** of a source, the easier to determine what the **values** and limitations** are.

### Content 
- This component requires a summary of the source inorder to demonstrate your ability to understand its meaning.
- purpose $`=\not`$ content

Questions to ask:
- What does the documentnt "say"?
- What is the main idea of the source?
- What arguments, analysis, or conclusion are present within the source's content?

### Values
- How the source is useful in the study of history
- The **content** may help you answer controverial or important questions about history
- The **author** or **context** may give you insights to why this osuurce was created (which is valuable)
- The **format** (a poem or picture) may prove valuable insight to the mood and emotions of an event or period that you cannot get from other formats.
- The **background** (origins of the author) may provide you insight on the context of the source and help you better understanding the meaning.

### Limitations
- You are essentially giving other historians warnings or advice about some othe the potential problems with using this source in the study of history.
- **The content**: If the content of the source is too general or too focused on unimportant details, this might prove to be a problem. The source can also be missing import details as well
- **The time and place**: the time and place of the source that was created provides context and `presentisim` can play a role here.
- **Bias**: Bias of the source, (the 4 types of biases)
- **The Style**: If the style is hard to read or the diction and language used is biased or useless
- **The Background**: If the source does not cite, fact check or is a "just for fun" website.
- **Technical values** are the limitation that consider the format, readbility etc.
- **Qualitative values** consider the values base on the content, author, and context of the source.

