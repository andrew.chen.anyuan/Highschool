# Overview

**Big O Notation:** In computer science, big O notation is used to classify algorithms according to how their running time or space requirements grow as the input size grows. 

**Time Complexity:** The analysis of the time it takes for a program to finish execution in relation to the number of operations it does.

**Space Complexity:** The analysis of the space/memory a program takes during execution.

**Overhead:** The amount of memory and other resources required to set up an algorithm.

**Stable:**  A sorting algorithm is said to be stable if two objects with equal keys appear in the same order in sorted output as they appear in the original array. Such as the same keys keep their relative order as before being sorted.

**Adaptive:** An adaptive algorithm has a different best and worst case. Useful for a specific case.

**Recursion:** When a method calls on itself to break down the problem into a simpler version of that problem

### How We meaure efficiency
- Time complexity (how long it takes according to data - size)
- Number of swaps/comparisons (How many times it compares)
- Space Complexity (How much space it takes according to data-size)
- Overhead (the overall resources that the program uses)
- Stable
- Adability
- Difficulty of implementation: how hard is it for a programmer to implement this algorithm.