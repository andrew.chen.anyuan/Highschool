# Selection Sort

Selection sort is a sorting algorithm that sorts an array by repeatedely finding the minimum element considering scending order from sorted order 

Number of passes: $`n-1`$

Number of comaparisons: $`n(n-1)/2`$

## Pros
- performs well on a small list
- easy to code
- it doesnt use much memory

## Cons
- poor efificiency when dealing with large lists
- other sorting methods are more efficient


Algortihm
1. Declare array minimum & temporary
2. ask for values to enter into array
3. set min variable equal to first index of array (value)
4. find lowest value in array using min variable (complete 1 pass)
5. Swap current index (value) with index with min value using temp variable
6. set min varaible equal to the next index of array (value)
7. repeat steps 4-6 until array is sorted

For each element, the entire list is checked to find the smallest element os in the worst case n elements are checed for each element hence the time compelxity is O n ^2 meaning hte time complexity performance is directly proportional

Since the array is sorted in palce and no extra space is used, the psace complexity is o1 meanis that hte space required by the algorith m ot proessd ata s cosntant; it does not grow

The order of elements does not affect the sorting time. In other words, enve if the array is partialyl sorted, still each element is copared and there is no breaking otu early. hence selection sort is non-adaptable

