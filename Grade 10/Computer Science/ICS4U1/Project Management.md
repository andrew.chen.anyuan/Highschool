# Project Management

**Learning Goals:** <br/>
- Learn the stages in Project Management 
- Apply it to a real project

**What is a Project:** <br/>
- According to the Project Management Body of Knowledge (PMBOK), a project is a temporary endeavour undertaken to create a unique product, service or result. 
- Temporary means that every project has a definite beginning and end

## What is Project Management? <br/>
- According to PMBOK, project management is the application of knowledge, skills, tools and techniques to project activities to meet project requirements 
- Project management is accomplished through the application of several project management processes 
    - Initiating 
    - Planning 
    - Executing 
    - Monitoring 
    - Controlling 
    - Closing 
- Traditionally, project management includes a number of elements: four to five proces groups, and a control system.

**Process Groups:** 
1. Initiation 
2. Planning or development 
3. Product or execution 
4. Monitoring and controlling 
5. Closing 

**Initiation:** 
The initiation process determine that nature and scope of the project. This stage shouldinclude a plan that encompasses the following areas: 
- `Analyze the needs of the buisness` in measurable goals 
- `Financial analysis` of the costs and benefits include a budget 
- Project charter including `costs, tasks, deliverables and schedule` 

**Scope:** The `products, services and results to be provided as a project`

**Requirement:** A `condition or capability that must be met` or processed by a system, product, service, result or component to satisfy a contract or specification

**Budget:** The `approved estimate for the project` or any work or any scheduled activity

**Project Charter:** A `document that formally authorizes the project`, the initial scope and the resources that the organization is willing to invest in.

**Resource:** `Material that is used inorder to complete the project`. Eg, skilled human resource, equipment, services, supplies, materials, etc.

## Planning and Design:
Plan the time, cost and resources adequately to estimat teh work needd. Effectively manage risk during project execution. Failure to adequately plan greatly reduces the project's chance of successfully accomplishing its goals. This section also consists of: 
- Determining how to plan
- Developing `scope statement` 
- Selecting `planning team` 

`Identifying the deliverables` and `creating the work breakdown structure`, identifying the activies needed to complete those deliverables and netwokring the activies in their  logical sequence. `Estimating the resource requirements, time and cost, budget, developing the schedule, risk planning and gaining formal approval`.

## Executing:
Executing portion consists of the processes used to `complete the work defined in the project management plan` to accomplish the project's requirements. It also involves `coordinating people and resources`. `Deliverables are produced` as outputs from the processes performed as defined in the project management plan.

## Monitoring and Controlling:
Processes performed to obvserve projet execution, potential `problems can be identified in a timely manner` and `corrective action will be taken`. <br/>
- `Measuring the ongoing project activites` <br/>
- `Monitoring the project variables` against the project management plan and the project performance baseline <br/>
- Identify `corrective actions to address isuess` and risk properly 

## Project Maintenance:
Work scop emay change, and it is a normal and expected phenomenon (`Change Management`). The changes needs to be documented to show what was atually constructed. The record is made on contract documents but not necessarily limited to.

## Closing
Closing includes the `formal acceptance` of the projet and the ending thereof. Administrativ eactivies include the `archiving of the files and comunenting lessons learned`. <br/>
`Project close:` Finalize all activites across all of the process groups to fomrally close the project or a phase of a project. <br/>
`Contract Closure:` Complete and settle each contract (including the resolution of any open items) and close each contract applicable to the project or project phase.