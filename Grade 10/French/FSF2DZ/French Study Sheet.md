# Francais
- Help me plz. :(

## Mots de liaison:
- `au contraire`: on the contrary, quite the opposite
- `d'ailleurs`: by the way, incidently
- `c'est-a-dire`: thats to say
- `alors`: so
- `par consequent`: therefore, consequently

## Les pronoms objets
- Follow the rule

|**Ne**|**Pr**onoun|**D**irect|**I**ndirect|**Y**|**En**|**Aux**illary|**Pas**|**P**articiple **P**asse|
|:-----|:----------|:---------|:-----------|:----|:-----|:--------|:--|:---------------|
|Remeber Ne is always first|me, te, nous, vous se|le, la, l' les|lui, leur|y|en| | | |



|If you don't have connector|If you have connector (à, au, aux)|If you have a place|If you have something (de, des, du, you might also need to open up the nouns such as besoion (de))|
|:--------------------------|:---------------------------------|:------------------|:--------------------|
|Example: John mange la pomme, $`\rightarrow`$ John **la** mange.|Example: John parle à Marie, $`\rightarrow`$ John **lui** parle|Example: Je visite paris $`\rightarrow`$ j'**y** visite|Example: Je mange du pain $`\rightarrow`$ J'**en** mange.|

**Note:** `reflechir` and `penser` go with **y** instead of the usual pronoun.

## Tenses
- Present de l'indicatif
- Futur simple
- Future proche
- imparfait
- passe compose
- conditionnel du passe
- conditionnel du present
- Future anterieur
- plus-que-parfait
- subjontif
- gerondif
- imperatif

### Present
- Seriously just memorize tables and follow rules

### Futur simple
- Remember table, remember the **stem** always ends in **r**, then add `ai, as, a, ons, ez, ont` to ending of stem.

### Future proche
- conjugate `aller` in present and add infintive

### Imparfait
- conjugate verb in present nous, then cut `ons` from the ending, and add `ais, ais, ait, ions, iez, aient`. 
- `etre` is an exception, and the stem is `et`.

### Passe Compose
- Is two parts, an auxillary and participle passe
- For auxillary:
    - either avoir or etre in present conjugation
    - We do not usually accord gender and number with avoir
- For participle passe:
    - For regular ER verbs, cut the `er` and replace with e (with accent up).
    - For regular IR verbs, cut the `ir` and replace with i.
    - For some RE verbs, replace RE with `u`
    - Rest you have to memorize tables, such as `appris`, `voulu`, etc.

#### ETRE
- Etre only go with the verbs of the mountain and its relatives, there are 14 main ones and there are many more based off of the main ones, such as revenir.

|Verb|Particple Passe|
|:---|:--------------|
|venir|venu|
|arriver|arrive|
|monter|monte|
|entrer|entre|
|rester|reste|
|sortir|sorti|
|tomber|tombe|
|descendre|descendu|
|partir|parti|
|retourner|retourne|
|passer|passe|
|naitre|ne|
|mourir|mort|

- Etre participle passe accords with gender and number (eg. if the pronoun is elle, we have add an extra `e`, and plural we have to add an extra `s`).
- For self-pronoun verbs, we also accord with gender and number.

### Exceptions
There are exceptions to passe compose however:

If there is a COD (object direct, so le, la, les) before the passe compose, we have to accord to gender and number. 
- Eg. John a mange les pomme, becomres John les a mangees.

If there is `que` before the the passe compose, we have to accord to gender and number. 
- Eg C'est le pomme que John a mange, becomes C'est que John la mangee.

If there is COD after the passe compose for etre, we use avoir instead of etre and we do not accord to gender and number, because we are talking about the COD, not the verb.
- Eg. John a monte le livre. Le livre is a COD because if we open the sentence, there is no connector.

However, if we do pronouns on the above sentence, we have to following our avoir rule with COD, so John a monte les livres becomes John les a montes.

With self-pronoun verbs, there is also exceptions.

If there the pronoun/verb is COI (usually communcative verbs), then we do not accord to gender and number.
- Eg. Ils se sont parle, if we open up the sentence, parle has a connecter (a), so its COI (objects indirect)

However, if there is a COD after the passe compose, it cancels out the COD verb and we also do not accord to gender and number.
- Eg Ils se sont lave les mains, although lave is COD, les mains is also COD, so we do not accord to gender and number.


### Conditionnel du Passe
- This is a compound tense, you conjugate the auxillary with present du conditionnel and you use the normal participle passe. Everything, including exceptions
of passe compose still apply.

### Present du Conditionnel
- It uses the futur simple stem and adds the imparfait ending.
- Not a compound tense
- Usually used to say something politely (I would, I could, etc)

### Future Anterieur
- This is a compound tense, you conjugate the axuillary with future simple, and you use the nomral participle passe.

### Plus-que parfait
- This is a compound tense
- You conjugate the auxillary with imparfait and you use the normal participle passe 
- Exceptions of passe compose still apply

### Subjonctif
- We will be only worrying out how to conjugate in this section.
- Conjugate the verb in the `ils` plural form, then add `e, es, e, ions, iez, ent`.
- However, there are exceptions with 7 of the verbs:

|Pronoun|Avoir|Etre|Pouvoir|Faire|Savoir|Vouloir|Aller|
|:------|:----|:---|:------|:----|:-----|:------|:----|
|Je|aie|sois|puisse|fasse|sache|veuille|aille|
|Tu|aies|sois|puisses|fasses|saches|veuilles|ailles|
|Il|ait|soit|puisse|fasse|sache|veuille|aille|
|Nous|ayons|soyons|puissions|fassions|sachions|veuillions|allions|
|Vous|ayez|soyez|puissiez|fassiez|sachiez|veuilliez|alliez|
|Ils|aient|soient|puissent|fassent|sachent|veuillent|aillent|

### Gerondif
- Basically the process of doing something, the middle of something, such as:
- Im working, travillant. 
- Basically getting the stem of the verb (removing er, ir, re), and adding `ant`.
- However this is variable and I'm not quite sure so :p, good luck.

### Imperatif
- Basically ordering people to do stuff.
- Only three forms, tu, nous, and vous.
- Simply conjugate the verb in present
- For tu however, you remove the `s`, but you call it back when there is a vowel right after (eg Va**s**-y instead of Va-y)
- For negative, the pronoun is infront of the verb (like in english, "don't do work!"), and for positive the pronoun is after (eg lave-toi!)


## Les Hypotheses
- This is when you have Si...., and then.....
- So its if whatever, then whatever.
- You just got to remember the 3 relations
    - Si **present**, it goes to **future simple**
    - Si **imparfait**, it goes to **present du condtionnel**
    - Si **Plus-que parfait**, it goes to **conditionnel du passe**.
    - Remember to always check after the **SI** (If), they could be mean and switch up the order, meaning Si doesn't have to be at the start of the sentence necessarily.

## Pronoms Relatifs
- `qui` usually represents the subject/pronoun (who), so it is usually followed by a verb
- `que` usually represents the object (what), so it is usually followed by a pronoun
- `ce qui` means what/which/etc, is the subject of the verb that follows it. (eg Ce qui est certain, c'est qu'on va bien s'amuser! The subject is est certain)
- `ce que` means what/which etc, is used as direct object, generally followed by a subject and a verb
- `ce dont` (dont means whose), used as the object the preposition de. (Open the verb to se, such as (besoin))
- `ou`, literally means where, so you can use it as you see fit.

## Preposition Avec Les Pays
- Literally memorization, but usually En for feminin, and Au for masculin

## Subjonctif L'indicatif
- Subjontive is usually something you cannot control, such as I wish, He must, etc. It is a mode, not a tense.
- The indcative is the indicative, something real, such as I can understand, I talk, etc, It is a mode, not a tense
- You really have to understand the sentence to check if it is subjonctif or indicatif, usually nous and vous or the execeptions give away its mode, but
that is not always the case.
