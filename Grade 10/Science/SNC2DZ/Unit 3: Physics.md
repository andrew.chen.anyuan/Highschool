# Unit 3: Physics

## Light
 - `Light`: Electromagnetic radiation/waves, as light interacts with both electricity and magnets
 - Light travels at $`3.0 \times 10^8`$
 - `Energy`: Ability to do work
 - `Work`: Ability to move matter in space
 - Energy can be transferred and transformed, but not destroyed
 - Light behaves as a particle and/or a wave
    - Behaves as particle when travelling through a vacuum, which waves cannot do
    - Behaves as wave by forming "interference patterns", properties of light waves are also measurable
 - `Photon`: Light particle

### Properties of electromagnetic waves
<img src="https://sites.google.com/site/mrgsscienceclass/_/rsrc/1536069562258/about-science-class-1/physical-science-physics/energy-waves-1/Wave%20properties.PNG" width="500">

 - `Amplitude`: Height from centre to crest/trough
 - `Crest`: Peak of wave
 - `Trough`: Base of wave
 - `Wavelength`: Distance between two points on wave on the same plane
 - `Frequency`: Waves passing per <unit> (e.g., hertz (waves per second))
 - Visible light wavelengths are between 400-700 nm long

<img src="https://www.researchgate.net/profile/Jolanda_Patruno2/publication/280792916/figure/fig10/AS:669441209692171@1536618630777/Electromagnetic-spectrum-These-general-characteristics-above-indicated-are-valid-both.png" width="500">

 - Light always travels in a straight line
 - **Longer** wavelength = **smaller** frequency = **less** energy
 - **Shorter** wavelength = **higher** frequency = **more** energy
 - **Higher** energy, **lower** penetration (e.g., 2.4 GHz vs 5 GHz Wi-Fi)
 - `Luminous`: Emits light
 - Non-luminous objects do not emit light
 - `Colour`: Reflected parts of white light from non-luminous objects
 - Blacks absorb all visible light while whites do the opposite

|Type of electromagnetic wave|Picture|Use/phenomena|
|:---------------------------|:------|:------------|
|Radio Waves|<img src="https://images-na.ssl-images-amazon.com/images/I/91PqLproBUL._SX679_.jpg" width="200">|• AM/FM radio<br>• TV signals<br>• cellphone communication<br>• radar<br>• astronomy (for example, discovery of pulsars)|
|Microwaves|<img src="https://cdn.shopify.com/s/files/1/2660/5202/products/KMCC501S_HC_1400x.jpg?v=1571711208" width="200">|• telecommunications<br>• microwave ovens<br>• astronomy (for example, background radiation associated with the big bang)|
|Infrared light|<img src="https://i.ytimg.com/vi/wIZozabAMO8/maxresdefault.jpg" width="200">|• remote controls (eg DVD players and gamecontrollers)<br>• lasers <br>• heat detection<br>• Astronomy|
|Visible light|<img src="https://images.ctfassets.net/cnu0m8re1exe/5bMohBQkq3U2zU2Z5PtyBA/8a3a0f4530c95e41e868527c0983dd21/rainbow.jpg?w=650&h=433&fit=fill" width="200"> |• human vision<br> • rainbows <br>• astronomy (eg optical teloscopes, discovering the chemical composition of celestial bodies)|
|Ultraviolet light|<img src="https://i1.wp.com/treatcancer.com/wp-content/uploads/2015/08/670px-Understand-the-Effects-of-Different-UV-Rays-Step-1.jpg?ssl=1" width="200">|• causes skin to tan and sunburn <br>• increases risk of skin cancer<br>• kills bacteria in food and water<br>• lasers<br>• stimulates production of Vitamen D<br>• Astronomy|
|X-Rays|<img src="https://medlineplus.gov/images/Xray.jpg" width="200"> |• medical imaging<br>• security equipment<br>•  cancer treatment<br>• astronomy (eg. study of black holes, binary star systems)|
|Gamma Rays|<img src="https://cdn.mos.cms.futurecdn.net/qZjPVEZUNdngyBykB4J6fj-320-80.jpg" width="300">|• Cancer treatment<br>• product of nuclear decay <br>• astronomy (eg. supernovas)|

### Luminescence

|Type Of Luminescence|Description|Picture|
|:-------------------|:----------|:------|
|Incandescence|- Produces light by using high temperature to create heat and light. <br>- Occurs in light bulbs, where electricity passes through a **filament** using made of tungsten it becomes so hot that it gives off visible light<br>- It also emits `infrared` light that you feel as heat radiating from the bulb depending on the bulb only a tiny fraction is converted to visible light the rest is converted to `infrared` light. <br>- This is makes this process very inefficient <br>- Examples include <br>- incandescence light bulbs<br>- burning candle<br>- lit sparks flying off a grinder|<img src="https://i.stack.imgur.com/rSUeI.jpg" width="300">|
|Electric Discharge|- The process of producing light by passing electric current through a gas. Different gases produce different colours when electricity is passed through<br>- Examples include: <br>- Neon light signs <br>- Lightning (in this case, the gas is air)|<img src="https://i.ytimg.com/vi/r-E3vw5F8sI/maxresdefault.jpg" width="300">|
|Phosphorescence|- The process of producing light by the absorption of `ultraviolet` light resulting in the emission of visible light over an **extended** period of time<br>- This is different than `Fluorescene`, as the light is released over a period of time<br>- Often described as `glow-in-the-dark` materials<br>- Examples include: <br>- glow in the dark watches, stickers, clocks etc|<img src="https://sc01.alicdn.com/kf/HTB1etspiDqWBKNjSZFxq6ApLpXaD/Glow-In-The-Dark-Dinosaur-Toy.jpg_350x350.jpg" width="300">|
|Fluoresence|- Process of producing light immediately as a result of the absorbtion of `ultraviolet` light<br>- Detergent manufacturerse often add flourescent dyes to make washed shirts more brighter<br>- This is process is even apparent in visible light because normal daylight includes a small amount of `ultraviolet` light<br>- Flourescent lights makes use of both `electric discharge` and `fluorescence`. The electric gas (usually mercury) produces ultra-violet light during electric discharge, which is then used to produce visible light.<br>- Fluorescent lights 4-5 more efficient than incandescent bulbs<br>- Examples include: <br>- Fluorescent lights|<img src="http://sdhydroponics.com/wp-content/uploads/2012/05/PastedGraphic-21-1.png" width="400">|
|Chemiluminescence|- The direct production of light as the result of a chemical reaction with **little** or **no heat** produced<br>- Light sticks is glow because when snapped, the 2 chemicals react with each other to produce light. <br>- Chemiluminescence does not rely on `electric discharge`, little heat produced, no moving parts and can be sealed with durable material, making it very useful in hazardous environments. <br>- Examples include: <br>- Light sticks|<img src="https://www.thoughtco.com/thmb/1GoRbw0r-1kWPkmFey9BmR3H7Dw=/768x0/filters:no_upscale():max_bytes(150000):strip_icc()/flasks-with-glowing-liquids-520120820-594044535f9b58d58a548082.jpg" width="500">|
|Bioluminescence|- The production of light in living organisms as the result of `chemiluminescence`<br> Examples include: <br>- Fireflies<br>- fungi<br>- marine invertebrates<br>- fish<br>- glow-worms<br>- certain bacteria|<img src="https://www.hakaimagazine.com/wp-content/uploads/header-bioluminescence_0.jpg" width="300">|
|Triboluminescence|- The production of light from **friction** as a result of scratching, crushing, or rubbing certain cystals<br>- Examples include: <br>- Rubbing twoquartz crystals together will produce light due to triboluminescence|<img src="https://i.ytimg.com/vi/MzBXXmcaf2M/maxresdefault.jpg" width="300">|
|Light-Emitting Diode (LED)|- light produced as a result of an electric current flowing in **semiconductors**. <br>- **semiconductors** are materials that allow an electric current to flow in only one direction<br>- When electricity flows in the allowed direction, the LEd emits light<br>- **Does not** produce much **heat** as a by-product, nor require a **filament**, and is more energy efficient<br>- Examples include<br>- LED lights<br>- christmas tree lights<br>- illuminated signs<br>- traffic lights|<img src="https://d114hh0cykhyb0.cloudfront.net/images/uploads/rgb-fast-color-changing-led01.jpg" width="300">|

### Rays
 - Light path can be tracked via arrrows
 - `Normal`: Perpendicular line to an interface (e.g., mirror, medium boundary), intersecting where light reflects off
 - `Angle of incidence`: Angle of light hitting reflective surface, relative to the normal
 - `Angle of reflection`: Angle of light leaving reflective surface, relative to the normal
 - Laws of reflection
     - Angle of incidence = angle of reflection
     - Light rays are on the same plane
 - Types of reflection
     - `Specular reflection`: All normals are parallel (e.g., reflection off mirror)
     - `Diffuse reflection`: Not all normals are parallel (e.g., paper, not-mirrors)

## Mirrors
 - A mininum of **two** incident rays are required to find an image
 - Where rays converge describe image
 - **Dotted** lines are used for light going beyond a mirror (as light does not actually travel there)
 - `SALT`: Describes image
    - `Size`: Relative to object
    - `Attitude`: Orientation relative to object
    - `Location`: Relative to mirror and/or object
    - `Type`: Virtual (behind mirror) or real (in front of mirror)

### Plane mirrors

<img src="https://www.researchgate.net/profile/Merlin_John2/publication/293415482/figure/fig1/AS:326758030168064@1454916593825/Ray-diagram-for-image-formation-of-a-point-object.png" width="500">

 - `Object-image line`: Line perpendicular to plane mirror
    - Distance is equal on both sides of mirror
    - Describes location of object without requiring 2+ incident rays
    - Banned

### Concave and convex mirrors
 - `Concave mirror`: Curved mirror curving inwards in the direction of incident rays, like a cave
 - `Convex mirror`: Curved mirror curving away from incident rays, like back of a spoon

<img src="https://qph.fs.quoracdn.net/main-qimg-c84654e146945deec5ee6491cb547873.webp" width="500">

 - `Principal axis`: $`PA`$, line perpendicular to mirror when it hits it
 - `Centre of curvature`: $`C`$, point where the centre of the circle would be if mirror was extended to a full circle
 - `Focus`: $`F`$, point where all light rays focus on if incident rays are parallel to principal axis
 - `Vertex`: $`V`$, point where principal axis meets mirror
 - Imaging rules for curved mirrors:
    - 1. Any incident ray **parallel** to the principal axis will reflect directly to or away from the **focus**
    - 2. Any incident ray that would pass through the **focus** will reflect **parallel** to the principal axis
    - 3. Any incident ray that would pass through the **centre** of curvature will reflect **back on the same path**
    - 4. Any incident ray that reflects off the **vertex** reflect as if it were a plane mirror

**Characteristics of concave mirror images**

| **Object location** | **Size** | **Attitude** | **Location** | **Type** |
| :--- | :--- | :--- | :--- | :--- |
| Farther than $`C`$ | Smaller than object | Inverted | Between $`C`$ and $`F`$ | Real |
| At $`C`$ | Same as object | Inverted | On $`C`$ | Real |
| Between $`C`$ and $`F`$ | Larger than object | Inverted | Farther than $`C`$ | Real |
| At $`F`$ | N/A, lines do not converge | | | |
| Between $`F`$ and $`V`$ | Larger than object | Upright | Behind mirror | Virtual |

**Characteristics of convex mirror images**

| **Object location** | **Size** | **Attitude** | **Location** | **Type** |
| :--- | :--- | :--- | :--- | :--- |
| Anywhere | Smaller than object | Upright | Between $`F`$ and $`V`$/behind mirror | Virtual | 

## Refraction
 - Speed of light depends on its medium
 - Light bending while transitioning from a slower to faster medium or vice versa
 - Greater the change in speed, greater than change in direction
 - Turns in direction of **leading edge**
    - Analogy: Sleds slowing from one runner first when transitioning from snow to pavement
 - **Slow -> fast** medium: Refracts **away** from normal
 - **Fast -> slow** medium: Refracts **towards** normal
 - `Angle of refraction`: Angle of light after interface, relative to normal
 - Index of refraction: speed of light in vacuum / speed of light in medium
    - $`n = \dfrac{c}{v}`$
 - $`n_1 \sin\theta_{\text{incidence}} = n_2 \sin\theta_{\text{refraction}}`$
    - Where $`n_{1}`$ and $`n_{2}`$ are the refractive indexes of two different media
 - Snell's law: $`\dfrac{\sin\theta_2}{\sin\theta_1} = \dfrac{v_2}{v_1} = \dfrac{n_1}{n_2}`$

## Thin Lens 
- The optical center $`O`$ is the center of the lens.
- The point where the light rays converge (extended or actual), is called the **Primary Focus** ($`F`$), and the focus that is on the opposite side of the primary focus
is called the **Secondary Focus** ($`F^\prime`$).


### Converging (double convex) lens
- a lens that is thickest in the middle and that causes incident parallel light rays to converge through a single point after refraction
- also called a bi-convex lens
- the primary focus is on the right side of the lens, while the secondary focus is on the left side of the lens

Some rules for converging lens for locating the image: (note that this only true for thin lenses, which are the only lenses in this unit?)

1. A ray parrel to the principal axis is refracted through the principal/primary focus ($`F`$).
2. A ray through the secondary principal focus ($`F^\prime`$) is refracted paarallel to the principal axis.
3. A ray through the optical center $`O`$ continues straight without being refracted.

|Location (Object) |Size|Attitude|Location (Image)|Type|
|:-----------------|:---|:-------|:---------------|:---|
|beyond $`2F^\prime`$|smaller|inverted|beteween $`2F`$ and $`F`$|real|
|at $`2F^\prime`$|same size|inverted|at $`2F`$|real|
|between $`2F^\prime`$ and $`F^\prime`$|larger|inverted|beyond $`2F`$|real|
|at $`F^\prime`$|No clear image||||
|inside $`F^\prime`$|larger|upright|same side as object (behind lens)| virtual|

### Diverging (double concave) lens
- a lens that is thinnest in the middle and that causes incident parallel light rays to spread aparet after refraction
- also called a bi-concave lens
- the primary focus is on the left side of the lens, while the secondary focus is on the right side of the lens

Some rules for diverging lens for location the image: (note that this only true for thin lenses, which are the only lenses in this unit?)
1. A ray parallel to the principal axis is refracted as if it had come through the principal focus ($`F`$)
2. A ray that appears to pass through the secondary principal focus $`(F^\prime)`$ is refracter parallel to the principal axis
3. A ray through the optical center ($`O`$) continues straight through on its path.

A diverging lens **always** produces a `smaller`, `upright`, `virtual image` that is on the same side of the lens as the object.

## Total internal reflection
 - `Critical angle`: Angle of incidence that causes refracted ray to be perpendicular to normal
 - TIR occurs when angle of incidence exceeds critical angle, causing near-100% reflection
 - Happens only when refracting from **slow to fast**
 - **Refraction is not perfect; some light is reflected during refraction**
    - Reflected ray grows brighter as we reach critical angle, and refracted ray grows dimmer
 - **Higher** index of refraction = **lower** critical angle

<img src="https://www.physicsclassroom.com/Class/refrn/u14l3b2.gif" width="500">

## Lens

### Thin lens equations
 - Can be used to find **location** and **magnification** of images

$`\frac{1}{d_{o}} + \frac{1}{d_{i}} = \frac{1}{f}`$

 - $`d_{o}`$: Distance of **object** from optical centre, always positive
 - $`d_{i}`$: Distance of **image** from optical centre
    - If positive, image is **real** and on the **opposite** side of the lens as the object
    - If negative, image is **virtual** and on the **same** side of the lens as the object
 - $`f`$: Distance of **focus** from optical centre
    - Is positive in a converging lens
    - Is negative in a diverging lens

$`M = \frac{h_{i}}{h_{o}} = -\frac{d_{i}}{d_{o}}`$

 - $`h_{o}`$: Height of object
 - $`h_{i}`$: Height of image
    - If positive, image is **upright and virtual**
    - If negative, image is **inverted and real**
 - $`M`$: Magnification of image
    - If positive, image is **upright and virtual**
    - If negative, image is **inverted and real**
    - If greater than 1, image is larger and farther from the optical centre than the object
    - If less than 1, image is smaller and closer to the optical centre than the object

## Human eye
 - `Iris`: Controls amount of light entering eye
 - `Lens`: Focuses light to form an **inverted real** image at the back of the eye
 - `Retina`: Captures **light** and converts it to **electrical signals**
 - `Optic nerve`: Transports information from the **retina** to the **brain** for processing
 - `Cornea`: A protective layer on top of the lenses, refracts more light than the lenses, and focuses the light.
 - `Accommodation`: The eye muscles streches/squishes the lens to change its shape inorder to focuse the image onto the retina. (changes the focal length)

### Focusing problems
 - `Myopia`: Near-sightedness
    - The light rays converge before the retina, occurs when the distance between lens and the retina is too large or if the corena+lens combination coverges light too strongly
    - light from distance objects is brought to a focus **infront** of the retina
    - this can be fixed by using a diverging lens or a negative meniscus, the lens/meniscus diverge + refract the rays so that the light rays converge later on the retina
 - `Hyperopia`: Far-sightedness
    - The light rays converge after the retina, occurs when the  distance between the lens and the retina is too little or if the corean+lens combaintion is too weak
    - Light from nearby objects focuses **behind** the retina
    - This can be fixed by using a converging lens or a positive meniscus, the lens/meniscus converge + refract the rays so they converge later on retina
 - `Presbyopia`: Far-sightedness due to aging
    - The eye looses its elasticity, and the **accommodation** mechanism does not work well. Can be fixed with a converging lens. 
 - `Astigmatism`: Blurry vision for everything
 - Contact lenses are basically glasses but on your eye (according to textbook), however they are ones that reshape your eye or reshape the lenses in your eyes.
 - Laser eye surgery uses a laser to resphae the cornea of the eye in order to improve vision.

## Applications Of Lenses
- Not sure if its going to be tested, but put here in case:

### Camera
Basically it takes in objects that are far away (beyond 2F') and uses a converging mirror to focus it onto a film or digital sensor.

### Movie Projector
Basically it takes a small object (the film), places it between 2F' and F', then uses a converging mirror to make the image larger than object which is being projected onto the movie screen.

### Magnifying Glass
When an object is inside F' of the converging lens, the refracted light rays don't actually converge, but your brain extends these rays bacwards and produces an enlarged vritual image located on the same side of the lens as the object.

### The Compound Microscope
Uses 2 converging lens, the objective lens produces a real image inside F, and like the magnifying glass, the eyepiece lens refracts the real image, and then your brain extends the refracted rays from the eyepiece lens and see the larger virtual image.

### The Refracting Teloscope
Since light coming from far away are parallel, the light rays are beyond 2F', it uses 2 converging lens to do something similar to the compound microscope. What you actually see is the virtual image.
