# Chemistry

## Acids
 - All acids contain hydrogen ions
 - Behave like molecular compounds while out of water
 - Behave like ionic compounds while aqueous
    - Hydrogen ions are released
    - Conduct electricity
 - Reacts with active metals depending on the activity series as a single displacement reaction between the metal and the hydrogen ion
 - Corrosive (reacts with skin)
 - Sour-tasting
 
### Naming Binary Acids
- $`HCl_{(g)} \rightarrow`$ hydrogen chloride
- $`HCl_{(aq)} \rightarrow`$ **hydro**fluoric **acid**
- Need to put the **hydro** prefix as it tells us its disolved in water and is an acid

### Naming Oxyacids
- `ite` ions make `ous` acides (suffix). $`HCLO_2 = `$ Chlor**ous** acid
- `ate` ions make `ic` acides (suffix). $`HCLO_3 = `$ Chlor**ic** acid
- Prefixes stay the same

## Bases
 - All bases contain hydroxide or carbonate ions
 - Behave like molecular compounds while out of water
 - Behave like ionic compounds while aqueous
    - Hydroxide ions are released
    - Conduct electricity
 - Reacts with fats and oils to form soap
 - Bitter-tasting

## pH
 - Used as a scale to measure how acidic or basic a substance is
 - Presented as a scale from 0-14
 - The lower the pH the more acidic, the higher the pH the more basic
 - Measures `concentration` of hydrogen ions in solution
 - pH scale is logarithmic
 - Concentration is increased tenfold when pH is increased by 1
    - pH 1 = 0.1 mol/L
    - pH 14 = 0.00000000000001 mol/L
 - Water at 25 degrees Celsius is pH 7.0 (neither acidic nor basic)

## Chemical Change
- Similar to chemical reaction

## Evidence of Chemical Change: (observations that tell us its happening)
1. A new gas is formed (new odour, formation of bubbles)
2. A large change in energy (eg. Light, heat, sound, electricity)
3. A new colour is formed
4. A new solid is formed

## Representing Chemical Reactions
- A Chemical equation!
    - Used to model what is happening during a chemical reaction.
    - We use an arrow instead of an equal sign to show `becomes`, `reacts to form`, `produces`
    - On the **left** side, we always have the **`REACTANTS`**, (INPUT), these get used up in the chemical reaction
        - There can be plus signs to show multiple `reactants`. (Recipes, we are adding them together)
    - On the **right** side, we have the **`PRODUCTS`** (OUTPUT).
        - Newly produced/made from `reactants`  
        - Plus signs show multple products, more like an **AND** more than anything else
- In a chemical reaction, reactant, molecules/atoms/formula units/ions reaarange to produce products moleccules/atoms/ions/formula units
- eg. Wax $`+`$ oxygen gase $`\rightarrow`$ Soot $`+`$ Water $`+`$ Carbon dioxide. 
- A `word` equation 
- $`\triangle`$ Greek letter to represent heat.
- $`C_{25}H_{52(g)} + O_{2(g)} \rightarrow^\triangle C_{(g)} + H_2O_{(g)} + CO_{2(g)}`$

## Types Of Reactions

- 6 types of reactions
    - synthesis
    - decomposition
    - single displacement
    - double displacement
    - combustion
    - neutralization

## Sythensis
- When 2 or more substances combine into one substance
- $`A + B \rightarrow AB`$

## Decomposition
- When a substance breaks down into 2 or more simpler substances
- $`AB \rightarrow A + B`$

## Single Diplacement
- When an element displaces another of the same group in a substance
- $`A + BC \rightarrow B + AC`$

## Double Displacement
 - When two cations switch places in two substances
 - $`AB + CD \rightarrow CB + AD`$
 - Note: If both products would be aqueous, there is no reaction

## Combustion
 - Two types: Combustion with metals and combustion with hydrocarbons
 - Combustion requires and releases heat but for the purposes of grade 10 chemistry this is ignored
 - Combustion with metals
    - When a pure metal and oxygen gas react to produce the most common oxide
    - e.g., $`2Mg + O_{2(g)} \rightarrow 2MgO_{2(s)}`$
 - Combustion with hydrocarbons
    - Two types: Complete and incomplete combustion
    - Complete combustion
        - When a hydrocarbon reacts with oxygen gas to produce water and carbon dioxide
        - Only works when enough oxygen is provided
        - Flame is blue
        - e.g., $`CH_{4} + 2O_{2} \rightarrow 2H_{2}O + CO_{2}`$
    - Incomplete combustion
        - When a hydrocarbon reacts with oxygen gas to produce water, carbon dioxide, carbon, and/or carbon monoxide
        - Lack of oxygen prevents complete combustion
        - No hydrogen can be left over
        - Flame is orange/yellow
        - e.g., $`2CH_{4} + 3O_{2} \rightarrow 4H_{2}O + 2CO`$

## Neutralization
 - When an acid and base react with each other
 - Changes properties of both the acid and base so that they are no longer acidic nor basic
 - Acids give their hydrogen ion to the base
 - Looks similar to double displacement
 - Always makes a salt and water
    - Note: Scientific definition of salt is any soluble ionic compound
 - If carbonic acid ($`H_{2}CO_{3}`$) would be formed it instantly splits into water and carbon dioxide
    - This reaction occurs even if both products would be aqueous
    - If this reaction occurs the product is written as water and carbon dioxide
 - $`H(A) + B(OH) \rightarrow AB + H_{2}O`$ or $`H(A) + B(CO_{3}) \rightarrow AB + H_{2}O + CO_{2}`$
 - e.g., $`HCl_{(aq)} + NaOH_{(aq)} \rightarrow NaCl_{(aq)} + H_{2}O`$

## Properties Of Acids and Bases

|Property|Acids|Bases|
|:-------|:----|:----|
|Ion present in solution|Hydrogen, $`H^+`$|Hydroxide, $`OH^-`$|
|Reactivity with metals|Reactive|Reactive|
|Electric conductivity|Conductive|Conductive|
|Taste|Sour|Bitter|
|pH range|$`[0, 7)`$|$`(7, 14]`$|
|Chemical indicators:|Will turn `red` with **blue litmus paper**|Turns `pink` with `phenolphthalein`. Turns `blue` with **red litmus paper**|

## Total Ionic Equations
- First balance the equation and mark the ones that can be dissolved in water. $`(aq)`$. If the ones cannot be dissolved in water, mark them as solids and leave them.
- For all the ones that can be dissolved in water, break them down into their individual ions and write their charge, if there are multiple of an ion, put it as an coefficient. (meanning $`6Cl_2`$ becomes $`12Cl^-`$). This is called the `Ionic equation/Total ionic equation`
- Remember, if you change the subscript or a charge of an element, that element becomes a new element.
- Cross out the same elements that are on both sides of the equation, remember, they must have the **same coefficient, charge, and be the same element** in order to cross out. This is called the `Net ionic equation`
- The elements you crossed out are the `spectator ions`, they do not participate in the reaction.

## Acid Rain
- `Acid rain`: Is rain or any other form of precipitation that is usually acidic.
- `Clean rain`: 5.6 pH - $`CO_2 + H_2O \rightarrow H_2CO_3`$, a weak acid.
- Biological processes/srouces such as dimenthyl sulfide $`(CH_3)_2S`$ react with water to form acides. The most common/princial case of acid rain is `sulfur` and `nitrogen` compounds from human sources, which react with water to form Nitric acid and Sulfuric acid.
- Nonmental oxides (sulphur and nitorgen oxides mainly) cause acid rain because they react with water in the atmopshere to form acid precipitation.
- Factories and coal power plants and motor vehicle from human sources releases this pollutatns. While emissions from volcanoes and waste products from photosynthetic microorganisms build up naturally to form acid precipitation.

## Adverse Effects of Acid Rain
- Lake/water acidity can change the enviornment for many aquatic life, such as fishes. Fish eggs will not hatch at **pH** of 5 or lower and any lower **pHs** can kill adult fish. Biodiversity in lakes and rivers are reduced due to acid precipitation.
- If one species is affected, if can ripple through the food chain. The acid also kills life near the bodies of water, such as insects and frogs. Not all species can tolerate the same **pH** levels.
- Soil biology can be seriously damaged by acid rains. Some microbes cannot consume the acid and tolerate the low **pH**s and are killed. The enzyemes of theses microorganisms are denatured, so they can no longer be used. The `hydronium` ions of acid rains also **mobilize** `toxins` and **leach away** `essential nutrients` and `minerals`.
- Due to the soil biology being damaged, the waste lands of nutrient depleted forests and other vegetations areas are unhabitable for many species, as there isn't food or things that can grow in the level of **pH** the soil has.
- Human health is damaged as well although no scientific proof has concluded that acid precipitation has caused these illnesses, the fine particles made from `sulphur dioxide` and `nitrogen dioxide` have shown to cause illness such as cancer and deadl diseases.
- Acid rains also affect monuments and statues, which have calcium compounds (limestone, sandstone, marble and granite) and creates `gypsum`, which just flakes off. Examples include old gravestones which have illegible inscriptions.
