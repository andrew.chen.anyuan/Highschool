# Notes Day 3

## Vocabulary / Diction
- `Paraphernalia`: Miscellaneous articles, especially the equipment needed for a particular activity. trappings associated with a particular institution or activity that are regarded as superfluous.
- `Acolyte`: A person assisting the celebrant in a religious service or procession. An assistant or follower.
- `Ire`: Anger
- `Pantomime`: Express or represent (something) by extravagant and exaggerated mime.

## Notes
- Rihiannon shows slight characterization, from the shy girl that is like a slave to Justin to a more open girl, as she starts to have more conversations with the protagnist.
- She also drifting away from Justin, as we do not see much of his dialogue for a long time.
- Character philosphy?
- > And the only way to show her how it makes sense, the only way to make the enormity real, is for me to lean over and kiss her. Like last time, but
not at all like last time. Not our first kiss, but also our first kiss. My lips feel different against hers, our bodies fit differently. And there is also
something else that surrounds us, the black cloud as well as the enormity. I am not kissing her because Iwant to, and I am not kissing her because I
need to—I am kissing her for a reason that transcends want and need, that feels elemental to our existence, a molecular component on which our
universe will be built. It is not our first kiss, but it’s the first kiss where she knows me, and that makes it more of a first kiss than the first kiss ever
was.
- Its pretty obvious, but with characters that are more important, more pages and information are being given to, this depressed girl got around 5-10 pages of that chapter, as some got like 1 page or a half.
- Nathan's story got bigger.
- > THE DEVIL AMONGUS!
- Metaphors
- > From the moment we hit Annapolis, Austin is in his element.
- Idea of homosexuality and evil? I mean, its natural for it to be in an a anti-LGBTQ campaign, but the author decided to use this one, as it has `devil` in it, which leads back to the repeated idea of devil throughout the book when the protaganist met Nathan.
- > HOMOSEXUALITY IS THE DEVIL’S WORK,
- Nice sedusive, lovely atmosphere, especially when described by using dialogue.
- > “Maybe we should do something,” I suggest. <br> “Yeah. Sure.” <br> “Maybe just the two of us.” <br> Click. He finally gets it. <br> I move in. Touch his hand. Say, “I think that would be fun.”
- Justin actually shows signs that he cares about Rhinannon, possible characterization from the first mean and jerky Justin we got to know in the beginining. Also similes.
- > “Why not?” I ask. <br> He looks at me like I’m a complete idiot. <br> “Why not?” he says. “How about Rhiannon? Jeez.”
- More devil stuff. At this point, I think the author could be saying a theme of how humans will start to blame something else when they see something that they don't know how to explain.
- > WILLIAM CARLOS WILLIAMS TO REVEREND POOLE: ‘THE DEVIL MADE ME EAT THE PLUM.’
- Chapter 6008 has a bunch of devil words, it seems the `devil` is a strong plot/idea in this book. The protagnist also has a self-self conflict in this scene, where he starts to believe that he is the devil.
- Nice metaphor?
- > school day is already in full swing,
- Repeated words show that the protagnist is obessed with Rihannion, completly in love with her:
- > Nothing from her. <br> Nothing. <br> Nothing.