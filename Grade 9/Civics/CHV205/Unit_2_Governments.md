# Unit: 2 Government And Elections

## Terms
- Not sure if needed, just good background knowledge:

|Term|Definition|
|:---|:---------|
|Election|a formal decision making process by which a population chooses an individual to hold public office|
|Responsible government|A government that is responsible to the epople, executive that requires support of an elected assembly|
|Riding (consitiuency)|The body of works on a specified area who elect a representative to a legistative body|
|Political party|An organization of people with similar views on public issues who work to elect their canidates|
|Party Platform|The proposals put forward by a political part during an election campaign|
|Bill|A written proposal for a law|
|Act|A written law passed by parliament|

## Levels Of Government

|Level Of Government|Description|Responsibilities|
|:------------------|:----------|:---------------|
|Municipal|- is a type of local council authority that provides <br>local services, facilities, safety and infrastructure for communities|- Libraries<br>- Snow Removal<br>- Transit<br>- Building Permits<br>- Property Taxes<br>- Water<br>- Waste Management|
|Provincial|- is responsible for areas listed in the Constitution Act, 1867, <br> such as education, health care, some natural resources,<br> and road regulations. <br>- Sometimes they share responsibility with the federal <br> government|- Schools<br>- Education<br>- Health Care Delivery<br>- Social Assistance<br>- Natural Resources<br>- Licences|
|Federal|- This level of government deals with areas of <br>law listed in the Constitution Act, 1867 and that <br>generally affect the whole country.|- National Security<br>- Defence<br>- Military<br>- International Relations<br>- Citizenship & Immigration<br>- Money<br>- Banking<br>- Postal Service|

<img src="https://files.catbox.moe/ek8ape.png" width="800">

## Branches Of Government
- `Prime Minster`: The leader of the nation, head of the federal government, and the leader of the party with the greatest number of seats in parliament
- `Premier`: Head and leader of provinical government and provinical party in power

<img src="https://files.catbox.moe/3fs684.jpeg" width="800">

## Executive Branch
- Carries out the plans & policies of the government
- Branch of governement that has the power to carry out the palns & policies of the government

#### Outline Of The Branch At The Federal Level
- Queen (represented by the Governor General) $`\rightarrow`$ Prime Minister $`\rightarrow`$ Cabinet $`\rightarrow`$ Public Service

#### Outline Of The Branch At The Provincial Level
- Queen (represented by the Lieutenant General) $`\rightarrow`$ Premier $`\rightarrow`$ Cabinet $`\rightarrow`$ Public Service

## Legislative Branch
- Branch of governement that has the power to make, change and repeal (remove) laws.


#### Outline Of The Branch At The Federal Level
- Governor General $`\rightarrow`$ Senate (upper house) $`\rightarrow`$ House of Commons (lower house)

#### Outline Of The Branch At The Provincial Level
- Lieutenant Governor $`\rightarrow`$ Legislative Assembly

## Judicial Branch
- Branch of governement with power to interpret the law, decide who has broken the law, and what penalties should apply.

<img src="https://files.catbox.moe/z9sia3.jpg" width="500">

## House Of Commons
<img src="https://files.catbox.moe/tingao.png" width="800">

## Roles In Government
|Role|Description|
|:---|:----------|
|Governor General|Federal representative of the Queen|
|Lieutenant Governor|Provincial representative of the Queen|
|Prime Minister|The leader of the nation, head of federal government, and leader of party with the greatest number of seats in parliament|
|Premier|Head and leader of provincial government and party in power|
|Caucus|A group of representatives from the same political party in parliament|
|Cabinet|The group of ministers that decides what the government policy should be. They usually have responsiblity for particular departments of government, such as foreign affairs, defence and justice|
|Ministers|Member of the cabinet|
|Public service|The people hired to work for the government|
|Members Of Parliament|Person chosen in elections to represent the citizens, and to debate and vote on public issues in parliament|
|Speaker Of The house|Elected member of parliament, applies rules of parliament fairly & firmly to all members, including the Prime Minister|

## Right V.s Left Wing

|Left Wing|Centre|Right Wing|
|:--------|:-----|:---------|
|- more liberal||- more conservative|
|- support change in order to <br>improve welfare of all <br>citizens|- tradition is important but <br>change is supported if most <br>people want it|- tradition is important and <br>change should be treated <br>with caution|
|- government should play <br>a larger role in people's lives <br>(social services, benefits|- government should play <br>a role only in that it improves <br>the lives of citizens|- government should play <br>a small role<br>- private business should <br>ensure needs of citizens are <br>met|
|- law and order are <br>important to protect right of <br>all citizens fairly and <br>equally|- law and order are <br>important to encourage and <br>protect right of invididuals|- emphasis of law and order <br>to protect soceity and its <br>traditions|
|- more freedom to <br>individuals and less power <br>to police||- less freedom to individuals <br> and more power to police|

## Majority And Minority
- Vote/motion of non-confidence (can only occur in a minority government when less than half 50% of the MPs support the govt. on a bill. Govt. loses the confidence of the house and the governor general will dissolve parliament and call an election.
- This why minority governments do not always make it for a full term.

## How a Bill becomes law
1. Different MPP's and experts on what should be on the bill. After they finish, it gets introduced to `first reading`
2. A MPP stands up in the `legislature` and talks about the overall bill and appeal to the different members. No debates or anything, so questions will have to wait until later. Then they vote if it gets to go onto `second reading`
3. In the `second reading`, MPPs gets to debate and discuss the entails of the bill, and whether or not it should pass or fail, however, **nothing can be changed** Then they vote if it makes into the `Parliament Committee   `.
4. In the `Parliament Committee`, they now vote, and **can** make changes, amends, take away, approve, put in, parts of the bill. They ask different MPP, experts, and even the public has a say in this. Then the reworked bill gets passed onto `third  reading`.
5. In the `third reading`, the MPP's vote on to see if the bill **passes or not**. If it **does**, it gets sent to `lieutenant governor`.
6. The `lieutenant governor` signs the bill on the behalf of the queen. This is called `royal assent`. Now the bill becomes an `act` **(official law)**. 

## Different Electoral Systems

- `First Past The Post / FPTP`: the candiate who has more votes than any other single candidate win the the election.
    - Benefit: Produces more majority than minority governments
    - Problem: Produces results that our increasingly at odds with voters desires.
- `Preferential Voting`: a system of voting whereby the voter indicates his order of preference for each of the candidates listed on the ballot for a specified office so that if no candidate receives a majority of first preferences the first and second preferences and if necessary third and other preferences may be counted together until one candidate obtains a majority. 
    - Alternate short definition: Preferential voting is a system whereby a candidate must poll an absolute majority of the total formal votes (ie in excess of 50%) in order to be elected to the vacancy.
- `Proportional Representation`: an electoral system in which parties gain seats in proportion to the number of votes cast for them.

### Differences
- One is where you must have the majority, which is harder to get with many candidates, and one is where you get the majority of the seat votes, and if you reach a certain point, you are elected.

### Which Is More Democratic?
- I personally believe the preferential voting, as you should be having the majority in order to win, however, with alot of people, that can be very hard to achieve.

