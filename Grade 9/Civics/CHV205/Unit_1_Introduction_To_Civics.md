# Unit 1: Introduction To Civics
- Why we study civics: We study civics so we can make informed decisions and topics regarding politics, and we also learn more about our own rights, freedoms, responsibilites, and the government.

## Terms
|Term|Definition|
|:---|:---------|
|Civil Society|voluntary organizations of citizens that work to make a difference on important public issues|
|Democracy|a government in which the supreme power is rested in the people and exercised by them directly or indirectly through a system of representation, usually involving periodically held free election|
|Majority Rule|The principle that the opinion of the greater number should prevail|
|Minority Rule|The principle that the rights of the minority must be safeguarded| 

## Types Of Government Systems
- Type of government system for Canada is Consitutional Monarchy, we are under the rule of the queen of England, but we have consitutions, such as democractic votes and prime ministers.
- Type of government system for Totalitaria is most likely Oligarchy, where a select few (the three wise men) rule the people 

|Type Of Government|Description|Examples|
|:-----------------|:----------|:-------|
|Oligarchy|Rule by a few or by a select elite group of people|Russia, China, Iran, Ancient Rome|
|Monarchy|Rule by a hereditary king or queen|Saudi Arabia, Spain|
|Anarchy|Absence of government or total chaos|Iceland, Iraq 2003/2004|
|Consitutional Monarchy|Rule by a king or queen whose powers are limited by a consitution|Sweden, England, Canada|
|Dictatorship|Rule by one person - often a military figure|Sudan, North Korea, Germany|
|Theocracy|Rule by God, gods, or a religious leader under God's direction|Yemen, Iran|
|Repubic|A nation not ruled by a monarchy|Czech Republic, Argentina, U.S|

## Leadership Styles

- Leadership style for people who do not know each other is most likely Autocratic, they need a leader to tell each of them what to do to achieve good teamwork.
- Leadership style for people who are motivated is most likely lassiez-faire, where they are all motivated, and will independently work on the project.

|Style|Characteristics|When Effective|When Ineffective|
|:----|:--------------|:-------------|:---------------|
|Autocratic|- Tells others what to do<br>- Limits discussion on ideas and new ways of doing things<br>- Group does not experience feeling of teamwork|- Time is limited<br>- Individuals/group lack skill and knowledge<br>- Group does not know each other|- Developing a strong sense of team is the goal<br>- Members have some degree of skill/knowledge<br>- Group wants an element of spontaneity in their work|
|Laissez-Faire|- Gives little or no direction to group/individuals<br>- Opinion is offered only when requested<br>- A person does not seem to be in charge|- High degree of skill and motivation<br>- A sense of team exists<br>- Routine if familiar to participants|- Low sense of team/interdependence<br>- Members have low degree of skill/knowledge<br>- Group expects to be told what to do|
|Democratic|- Involves group members in planning and carrying out activities<br>- Asks, rather than tells<br>- Promotes a sense of teamwork|- Time is available<br>- Group is motivated and/or a sense of team exists<br>- Members of the group have some degree of skill/knowledge|- Group is unmotivated<br>- Members have little skill or knowledge<br>- High degree of conflict present|

## Democracy
- `Democracy`: a government in which the supreme power is rested in the people and exercised by them directly or indirectly through a system of representation, usually involving periodically held free election. 
- `Origins of Democracy`: Ancient Greece around the Middle of the 5th century BCE
- `Minority rights`: The principle that the rights of the minority must be safeguarded. 
- `Majority Rule`: The principle that the opinion of the greater number should prevail.
- During democratic decision making, the majority rule states the idea that the majority of the people voted for will be the chosen onr, however, there are measures that ensure that the powerful groups are not bullying the minority (minority rights). 


### 5 Central Beliefs of Democracy
1. Citizens should have a voice in decision making
2. All citizens should be treated as equal
3. All citizens should have fundamental rights and freedom
4. Citizens should have a sense of responsibility to other people in the community
5. Citizens should have a sense of what is socially just


## Conflict Resolution

|Type|Description|Pros|Cons|
|:---|:----------|:---|:---|
|Physical Force|Power, violence or pressure direct against a person consisting in physical act|Fast, gets the job done|Violence and huge losses from both sides|
|Negotiation|Discussion aimed at reaching an agreement|Peaceful|Very slow, since no one wants the worse side of the arguement|
|Mediation|Intervention in a dispute in order to resolve it|Somewhat peaceful, the conflict is usually solved quickly|May be bad, as someone may get the worse side|
|Arbitration|The process of solving an arguement between people by helping them agree to an acceptable solution|Usually peaceful, and both sides are usually happy|Takes longer than mediation, and physical force can still be involved|
|Consensus|A generally accepted opinion discussion among a group of people|Usually peaceful, both sides are usually happy|Takes a long time, lots of arguing, its hard to come to a common agreement|

## Importance Of Rights 
- The 2 documents that guarantee people's rights and freedoms in Canada are:
    1. **The Canadian charter of Rights and Freedoms**
    2. **Universal Declaration of Human Rights**
    - These things help protect our rights, in the case that someone wishes to breach or impale your rights.

- Rights are something that you are entilted to, for example, the right to vote. However, *with great power, comes great **responsibility** (spiderman)*, so with your right to vote, you have the responsibility to express your opinions in electrions with your vote.
- Responsibilities are the things that are expected from you that comes with your rights.

|Right|Responsibility|
|:----|:-------------|
|Right To Drive|Drive safely, follow traffic laws|
|Right To Vote|Express Opinions in elections|
|Right to free education and health care|Pay taxes|

## Antz Movie Diplomacy

|Character|Beliefs And Values|Goal/Plan For The Colony|How They Dealt With Those That Opposed Them|Type Of Leadership|Type Of Government|
|:--------|:-----------------|:-----------------------|:------------------------------------------|:-----------------|:-----------------|
|Queen|Peaceful, public figure|To keep thriving, and continue the way it was|Not much, she wasn't aware of the general's plans|Autocratic, she was the leader of the colony, and everyone had to listen to her|Monarchy, the group is ruled by a queen, however, it can also be consitutional, as the general had almost equal power|
|General|Wants to separate the society into the strong and weak, and the strong should lead the colony|To weed out the weak ones, and continue forward with the strong ones|He used physical force, and did whatever means necessary to silence those who opposed him|Autocratic, where he was the leader and basically made all the decision|Dictatorship, ruled by one military person|
|Z / Bala|Wants to have freedom, individualism|To change how the society worked, so that every ant can think for themselves, so they can thrive happily and peacefully|He mostly used negotiation to talk and discuss with them|Democratic, with the recurring motif of everyone thinking for themselves and everyone expressing their own opinions|Mostly resembles republic, as Z wants to have a system that isn't ruled by a king or a queen, but by the people|


