## Stephanie Kindly Refuses The Drugs

## Scenario
- Stephanie states that she will pass on taking the drugs and recommends that Kevin does not as well. 

<img src="https://cdn2.vectorstock.com/i/1000x1000/34/01/no-drugs-symbol-vector-1623401.jpg" width="500">

- Stephanie states that she has done a research project on the drug at school and lists the many negative consequences. 

<img src="https://www.mathematica-mpr.com/-/media/internet/labor-graphics-and-photos/labor-photos/ss_251529133-clear-630-315.jpg" width="500">

<img src="https://www.vertical-leap.uk/wp-content/uploads/2018/05/thumbs-down-1400x800.jpg" width="500">

- Kevin states that Stephanie is really uncool. 

<img src="http://www.bumblefoot.com/store/stamp-uncool.jpg" width="500">

## Choices
- How should Stephanie respond?
- [She Should Talk About The Negatives Of Drugs](https://gitlab.com/magicalsoup/Highschool/blob/master/Grade%209/Physical%20Education/PPL1O9/Choose%20Your%20Own%20Adventure%20ISP/Choice_2-1.md)

- [She Should Break Up With Kevin](https://gitlab.com/magicalsoup/Highschool/blob/master/Grade%209/Physical%20Education/PPL1O9/Choose%20Your%20Own%20Adventure%20ISP/Choice_2-2.md)