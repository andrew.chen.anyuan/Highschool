## Stephanie Refuses The Drugs

## Scenario
- Stephanie bluntly states that she will not take the drugs and then goes on to insult Kevin as being stupid.

<img src="https://images-na.ssl-images-amazon.com/images/I/61arNBCq-LL._SX425_.jpg" width="500">

<img src="https://i.ytimg.com/vi/gWOMTe_f0mo/hqdefault.jpg" width="500">

- Kevin fights back and calls Stephanie a number of names and also calls her uncool. 

<img src="http://clipart-library.com/images/5TRrgojqc.jpg" width="500">

<img src="http://www.bumblefoot.com/store/stamp-uncool.jpg" width="500">

- Stephanie feels uncomfortable and is thinking of leaving the concert.

## Choices

- [Stephanie Leaves](https://gitlab.com/magicalsoup/Highschool/blob/master/Grade%209/Physical%20Education/PPL1O9/Choose%20Your%20Own%20Adventure%20ISP/Choice_1-1.md)

- [Stephanie Stays](https://gitlab.com/magicalsoup/Highschool/blob/master/Grade%209/Physical%20Education/PPL1O9/Choose%20Your%20Own%20Adventure%20ISP/Choice_1-2.md)