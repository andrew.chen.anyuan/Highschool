## Stephanie Doesn't Tell Anyone About Her Situation

## Scenario

- Stephanie is embarrassed about the situation and does not tell anyone. 

<img src="https://1.bp.blogspot.com/-LeNAeaHQ1Yo/UUdmUZFiPrI/AAAAAAAAF2M/aNOFKV6bxE0/w1200-h630-p-k-no-nu/embarrassed+word%2528c%2529+melonheadz+13+bw.png" width="500">

- The bullying grows both in-person and online. 

<img src="http://clipart-library.com/images/dT9KbnoGc.jpg" width="500">

<img src="https://www.talkspace.com/blog/wp-content/uploads/2017/07/cyberbullying-woman-feature_1320W_JR-1.png" width="500">

- Eventually, Stephanie loses most of her friends and spirals into depression. 

<img src="https://previews.123rf.com/images/sergeyvasutin/sergeyvasutin1510/sergeyvasutin151000026/47066633-depression-concept-made-with-many-words-in-different-positions.jpg" width="500">

- Stephanie cracks and must tell her parents. Stephanie must switch schools.

<img src="https://newroadstreatment.org/wp-content/uploads/2017/10/communication2.jpg" width="500">

