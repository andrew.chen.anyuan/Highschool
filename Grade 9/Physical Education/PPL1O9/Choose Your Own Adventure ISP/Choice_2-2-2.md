## Stephanie Calmly Breaks Up

## Scenario
- Stephanie waits until the concert takes a break. 

<img src="https://www.enisa.europa.eu/topics/trainings-for-cybersecurity-specialists/online-training-material/images/time.png/download" width="500">

- Stephanie draws Kevin aside and calmly states that the relationship is not working. Kevin is mad at first, but Stephanie is able to make a clean break up. There is still a rift between Kevin and Stephanie.

<img src="https://static.highsnobiety.com/thumbor/x9_cTumaV-mc1kSRBUPnENJP09A=/fit-in/480x320/smart/static.highsnobiety.com/wp-content/uploads/2018/05/24110720/how-to-break-up-with-someone-digital-age-main.jpg" width="500">