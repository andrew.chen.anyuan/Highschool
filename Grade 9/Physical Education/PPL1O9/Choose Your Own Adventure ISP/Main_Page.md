## Welcome To Choose Your Own Adventure!

## Scenario
- Kevin and Stephanie are newly dating and are at a late night concert. 

<img src="http://i.ebayimg.com/images/g/5osAAOSwtfhYo38M/s-l1600.jpg" width="500">

- Kevin meets a new friend who offers drugs to the couple. Kevin takes the drugs and tells Stephanie to take them with him. Stephanie wants to refuse but doesn’t know how.

<img src="http://www.simcoemuskokahealth.org/images/default-source/homeslider/istock-174825736-webbanner.jpg?sfvrsn=2" width="500">

<img src="https://previews.123rf.com/images/nasirkhan/nasirkhan1504/nasirkhan150400025/38725763-3d-rendering-of-thinking-man-sitting-on-floor-and-looking-at-question-mark-sign-3d-white-person-peop.jpg" width="500">

## Choices

- [Stephanie Straight Up Refuses The Drugs And Insults Kevin](https://gitlab.com/magicalsoup/Highschool/blob/master/Grade%209/Physical%20Education/PPL1O9/Choose%20Your%20Own%20Adventure%20ISP/Choice_1.md)

- [Stephanie Calmly Refuses The Drugs](https://gitlab.com/magicalsoup/Highschool/blob/master/Grade%209/Physical%20Education/PPL1O9/Choose%20Your%20Own%20Adventure%20ISP/Choice_2.md)
