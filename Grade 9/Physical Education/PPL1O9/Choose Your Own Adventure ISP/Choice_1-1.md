## Stephanie Decided To Leave

- Stephanie decides to leave. Kevin and his friend are yelling names as Stephanie walks out. 

<img src="https://researchdigest.files.wordpress.com/2016/05/b61eb-thinkstockphotos-92134543.jpg" width="500">

<img src="https://www.freelancewritinggigs.com/wp-content/uploads/2012/04/insults.jpg" width="500">

- Stephanie and Kevin break up afterwards. Kevin begins to trash Stephanie at school, including sharing pictures of her and calling names. 

<img src="https://cdn2.iconfinder.com/data/icons/hearts-16/100/004-512.png" width="500">

<img src="http://clipart-library.com/data_images/129159.jpg" width="500">

## Choices

- How does Stephanie Deal with this situation?

- [Stephanie Doesn't Tell Anyone About This](https://gitlab.com/magicalsoup/Highschool/blob/master/Grade%209/Physical%20Education/PPL1O9/Choose%20Your%20Own%20Adventure%20ISP/Choice_1-1-1.md) 

- [Stephanie Talks With People You Trust](https://gitlab.com/magicalsoup/Highschool/blob/master/Grade%209/Physical%20Education/PPL1O9/Choose%20Your%20Own%20Adventure%20ISP/Choice_1-1-2.md)