# Badminton
## Start
- Split into 2 groups, `exprienced` and `inexprienced`.
- Each group split into 3/4 teams. With having the strongest being the first team.
- Each group uses 2 courts, meaning 2 games are being played in both groups.
- Need to set up nets, and poles at the beginning (5 minutes).
- Make sure everyone wears goggles.
- Make sure we have enough rackets and birdies.

## Matches
- Each match is either played to 21 or whoever has the highest points at the end of 5 minutes. Each match is 5 minutes long. If there is a tie, tie break by playing to one point.
- Each win contributes to `1` point to the final score, while a lost contributes no points.
- ### Exprienced
-
    |Match Number|Home Team|Vistor Team|
    |:-----------|:--------|:----------|
    |1|Team 1| Team 1|
    |1|Team 2| Team 2|
    |2|Team 3|Team 3|
    |2|Team 4|Team 4|
    |3|Team 1|Team 2|  
    |3|Team 2|Team 1|
    |4|Team 3|Team 4|
    |4|Team 4|Team 3|
    |5|Team 1|Team 4|  
    |5|Team 2|Team 3|  
    |6|Team 2|Team 1|  
    |6|Team 4|Team 2|  
    |7|Team 1|Team 4|
    |7|Team 2|Team 3|
    |8|Team 3|Team 2|  
    |8|Team 4|Team 1|

- ### Inexprienced
-
    |Match Number|Home Team|Vistor Team|
    |:-----------|:--------|:----------|
    |1|Team 1| Team 1|
    |1|Team 2| Team 2|
    |2|Team 3|Team 3|
    |2|Team 4|Team 4|
    |3|Team 1|Team 2|  
    |3|Team 2|Team 1|
    |4|Team 3|Team 4|
    |4|Team 4|Team 3|
    |5|Team 1|Team 4|  
    |5|Team 2|Team 3|  
    |6|Team 2|Team 1|  
    |6|Team 4|Team 2|  
    |7|Team 1|Team 4|
    |7|Team 2|Team 3|
    |8|Team 3|Team 2|  
    |8|Team 4|Team 1|

- Total time playing: 40 minutes.
- Left over time: Tally points.

# Soccer
- Teams of 11 on soccer field
- Left over subs, change subs every 5 minutes, same people cannot keep subbing.
- No tackles
- We need 11 pinnies of any colour.
- Play for whole 60 minutes, the winning team at the end wins.

