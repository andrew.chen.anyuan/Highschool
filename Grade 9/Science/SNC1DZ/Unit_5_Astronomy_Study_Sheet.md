# Unit 5: Astronomy

## Terms
- `AU` = Astronmical Unit, which is the distance between the sun and the Earth = $`1.5 \times 10^8km`$
- `1 Light year` = $`9.46 \times 10^{12}km`$
- `Satellite`: Celestial objects that travel around a planet (or dwarf planet)
- `Orbit`: Closed path of a celestial object/satellite as it travels around another celestial object.
- `Celestial Object`: Any object that exists in space.
- `Universe`: Everthing that exists, including all energy, matter and space 
- `Electromagnetic Spectrum`: is the range of frequenices  (the spectrum) of electromagnetic radiation and their respective wavelengths and photon energies.
- `Electromagnetic Radiation`: refers to the waves (or their quanta, photons) of the electromagnetic field, propagating (radiating) through space, carrying electromagnetic radiant energy. It includes `radio waves`, `microwaves`, `infrared`, (visible) `light`, `ultraviolet`, `X-rays`, and `gamma rays`.
- Our milky way is a spiral yeet (forgot full lesson, think this is all you need).

## Layers of the Sun
|Layer|Temperature|Description|
|:----|:----------|:----------|
|Corona|5800<sup>o</sup>C|- Gleaming white, halo-like - extends millions of km into space|
|Chromosphere|65 500<sup>o</sup>C||
|Photosphere|5 500<sup>o</sup>C|- The layer just below the Chromosphere where the light we see originates|

## Inside Of The Sun
|Zone|Descrption|
|:---|:---------|
|Convection Zone|- The `outermost` ring of the sun, comprosing of the `30` percent of its radius|
|Radiative Zone| - The section immediately `surrounding` the core, comprising `45` percent of its radius|

### Core
- `Hottest` part of the sun, reaching $`15,000,000^o`$C
- Energy released by **nuclear fusion** continues to move outward until it reaches the photosphere
- #### Compostion
    - **75%** `hydrogen`
    - **25%** `helium` (with small amounts of other gases)

### Nuclear Fusion
- The sun is made out of **hydrogen** atoms.
- The Sun’s energy comes from the **nuclear fusion** reactions that occur in the **core** of the Sun.
- **High temperatures** and **pressure** cause particles to collide at extremely high speeds. The **hydrogen** atoms of the sun fuse together forming **helium** atoms.
- Gives off **enormous amounts of energy**.

## Suns Affect on Earth

### The Aurora Borealis (Northern Lights)
- The `Northern Lights` are the result of collisions between gaseous particles in the Earth's atmosphere with charged particles released from the sun's atmosphere.
- `Solar winds` travelling toward Earth follow the lines of `magnetic force` created by Earth’s magnetic field (which is strongest near the **NORTH** and **SOUTH** `poles`).
- Near the poles, they come in contact with particles in Earth’s atmosphere, producing a display of `light` in the night sky.
- `Northern Lights` = `Aurora Borealis`.
- `Southern Lights` = `Aurora Australis`. 

## Earth's Rotation And Seasons
- The reason of why we have seasons is due to Earth's rotation around the sun (orbit), its like an egg shaped orbit, we have have summer when the earth is the closest to the sun, and winter when its the farthest away. 
- Earth spins on an angle, and makes one revolution per 24 hours, and the side facing the sun is morning and the other is night.

### Lunar Eclipses
- Lunar eclipses occur when Earth's shadow blocks the sun's light, which otherwise reflects off the moon. There are three types — `total`, `partial` and `penumbral` — with the most dramatic being a total lunar eclipse, in which Earth's shadow completely covers the moon
- A total lunar eclipse occurs when the moon and the sun are on exact opposite sides of Earth. Although the moon is in Earth's shadow, some sunlight reaches the moon. The sunlight passes through Earth's atmosphere, which causes Earth’s atmosphere to filter out most of the blue light. This makes the moon appear red to people on Earth.
- A partial lunar eclipse happens when only a part of the moon enters Earth's shadow. In a partial eclipse, Earth's shadow appears very dark on the side of the moon facing Earth What people see from Earth during a partial lunar eclipse depends on how the sun, Earth and moon are lined up.

A lunar eclipse usually lasts for a few hours. At least two partial lunar eclipses happen every year, but total lunar eclipses are rare. It is safe to look at a lunar eclipse.
<img src="https://cms.qz.com/wp-content/uploads/2018/07/lunar-eclipse-longest-21st-century.jpg?quality=75&strip=all&w=410&h=230.53714285714284" width="500">
<br>
<img src="https://www.nasa.gov/sites/default/files/styles/side_image/public/lunar-eclipse-diagram_0.jpg?itok=h-yNxUym" width="500">

### Solar Eclipses
- Sometimes when the moon orbits Earth, it moves between the sun and Earth. When this happens, the moon blocks the light of the sun from reaching Earth. This causes an eclipse of the sun, or solar eclipse. During a solar eclipse, the moon casts a shadow onto Earth
- The first is a total solar eclipse. A total solar eclipse is only visible from a small area on Earth. The people who see the total eclipse are in the center of the moon’s shadow when it hits Earth. The sky becomes very dark, as if it were night. For a total eclipse to take place, the sun, moon and Earth must be in a direct line.
- The second type of solar eclipse is a partial solar eclipse. This happens when the sun, moon and Earth are not exactly lined up. The sun appears to have a dark shadow on only a small part of its surface.
- The third type is an annular solar eclipse. An annular eclipse happens when the moon is farthest from Earth. Because the moon is farther away from Earth, it seems smaller. It does not block the entire view of the sun. The moon in front of the sun looks like a dark disk on top of a larger sun-colored disk. This creates what looks like a ring around the moon.

<img src="https://www.nasa.gov/sites/default/files/styles/side_image/public/solar-eclipse-diagram.jpg?itok=5fiowmol" width="500">

## The Solar System

<img src="https://thumbor.forbes.com/thumbor/960x0/https%3A%2F%2Fblogs-images.forbes.com%2Fstartswithabang%2Ffiles%2F2018%2F05%2Fshot.jpg" width="800">

### Planets
1. A planet must orbit a star
2. A planet must be big enough for its gravity to pull into a round shape
3. It must be big enough to clear most asteroids out of its path for its orbit.
- If they can't do these things, it's not a planet, it's a dwarf planet.

### Drawf Planets
- A celestial object that orbits the Sun and has a spherical shape but **does not** dominate its orbit.
- Ceres, Pluto, Haumea, Makemake, and Eris
- `Pluto’s` tilted orbit crosses Neptune’s orbit
- <img src="https://static1.squarespace.com/static/556bbefce4b028faf4c094c5/t/58d89c53c534a536b8a194f2/1490590824211/" width="500">

### The Inner Planets
- Mercury, Venus, Earth & Mars.
- Small rocky planets.
- Located between the `Sun` and `Asteroid Belt`.

|Planet|Orbital Period|Rotation|Atmosphere|Temperature|Number of Moons|Rings?|Unique Characteristics|
|:-----|:-------------|:-------|:---------|:----------|:---------|:----|:---------------------|
|Mercury|88 days|59 days|None|180 to 400<sup>o</sup>C|0|No|- No atmosphere to trap heat<br>- Contains craters<br>- Rarely visible in our night sky because its is so close to the sun|
|Venus|224.7 days|243 days, `(Opposite rotation)`|Carbon dioxide, nitrogen|462<sup>o</sup>C|0|No|- Brightest object in the sky after the Sun & Moon|
|Earth|365.26 days|24 hours|Nitrogen, Oxygen|-88 to 58<sup>o</sup>C|1|No|- Ozone filters some of the damaging radiation from the Sun<br>- Temperatures are constant<br>- 70% of planet's surface is water|
|Mars|687 days|24.65 hours|Carbon dioxide, Nitrogen|-90 to -5<sup>o</sup>C|2|No|- Called the `red planet` due to its rusty soil<br>- Very dry<br>- Once had volcanoes, glaciers, & water|

### The Outer Planets
- Jupiter, Saturn, Uranus, Neptune
- `Large`, composed of `gas`.
- Atopsheres consist mainlyof the gases `hydrogen` and `helium`.

|Planet|Orbital Period|Rotation|Atmosphere|Temperature|Number of Moons|Rings?|Unique Characteristics|
|:-----|:-------------|:-------|:---------|:----------|:---------|:----|:---------------------|
|Jupiter|11.9 years|9.85 hours|Hydrogen, Helium, methane|-148<sup>o</sup>C|63|Yes|- Largest planet (11x the diameter of the Earth)<br>- Features are its coloured bands, the Great Red Spot & hurricanes<br>- Orbiting rings of rocks|
|Saturn|29.5 years|10.65 hours|Hydrogen, Helium, Methane|-178<sup>o</sup>C|60|Yes|- Second largest, no solid core<br>- Cloudy & windy, over 1000 separate rings|
|Uranus|84.1 years|17.3 hours `(on its side)`|Hydrogen, Helium, Methane|-216<sup>o</sup>C|27|Yes|- Winds blow up to 500km/h|
|Neptune|164.8 years|15.7 hours|Hydrogen, Helium, Methane|-214<sup>o</sup>C|13|Yes|- Uneven orbit, Bright blue & white clouds<br>- Has a dark region called the Great Dark Spot, which appears to be the center of a storm| 

### Asteroids
- They are composed of rock & metal.
- Although they orbit the Sun, they are too small to be considered planets.
- Most asteroids lie in the asteroid belt, located between Mars & Jupiter.
- A **`meteroid`** is a piece of metal or rock that is `smaller` than an asteroid.
- Sometimes a meteroid get pulled in by `Earth's gravity`. They `burn` up in the Earth's `atmosphere`, creating a bright streak of `light` across the sky, know as **`meteor`** (shooting star).
- Larger meteors do not burn up completely in the atmosphere and their `remains`, which we call **`meteorites`**, crash to the ground.
- <img src="https://images.slideplayer.com/25/8036208/slides/slide_2.jpg" width="500">

### Asteroid Belt
- 700,000 to 1.7 million asteroids with a diameter of 1 km or more.
- Over 200 asteroids are known to be larger than 100 km.
- <img src="https://cdn-images-1.medium.com/max/1600/0*ucYelZ_W8549iLGn.gif" width="300">

### Comets
- **`Comets`** are large chunks of `ice, dust`, and `rock` that orbit the Sun.
- As a comet approaches the Sun, radiation and solar wind from the Sun, causes a `gaseous tail` to form, `pointing` directly `away` from the Sun.
- A `dust` tail forms in the direction from which the comet originated.
- Most comets have 2 tails;
    - `gaseous tail`
    - `dust tail`
- <img src="https://malagabay.files.wordpress.com/2017/09/comet-tails.gif?w=600" width="500">


## Big Bang Theory
- It happened around 13.7 billion years ago when the Universe was a infintely dense point.
- Formed from an extremely dense singularity (centre of a black hole)
- Prior to that there was nothing

### Evidence to support theory
- #### Redshift and Hubble’s Law
    - Hubble observed the line spectra from many different galaxies in sky, and most of spectra for galaxies were shifted towards the red end of the spectrum, a red shift
    - Hubble concluded that if most of galaxies were redshifted, they must be moving in all directions and the Universe is expanding from a single point
- Space between galaxies expand, not the galaxies themselves
- **Dark Matter:** the rest of the Universe appears to be made of a mysterious, invisible substance called dark matter (25%) and a force that repels gravity known as dark energy (70%)
    - 90% of matter in and between galaxies is of an unknown form that does not emit or absorb light
    - Can be detected through its gravity by the way it affects objects we can see
    - Without dark matter, normal matter would have been unable to clump and form stars and galaxies

## Apparent and Absolute Magnitude
- `Luminosity`: Total amount of energy produced by a star per second
- `Apparent Magnitude`
    - Brightness of a star in the night sky as they appear on Earth
    - The lower the number, the brighter the star is
- `Absolute Magnitude`
    - Brightness of a star as if they were located 33 ly from Earth
    - The lower the number, the brighter the star is

## Size of stars changes their lifestyle

### Hertzsprung Russel Diagram
- The Hertzsprung-Russell Diagram is a graphical tool that astronomers use to classify stars according to their luminosity, spectral type, color, temperature and evolutionary stage.
- Basically plotting the class of the stars based on their lumionsity (how bright they are) and their temperature (how hot they are).
- <img src="https://lh5.googleusercontent.com/ABuKmOEUapLg2_IBkHygdUrJqmUYt1fgCdkqICNdHma4SoTeLbTh6XpLAXYCnbabzhlXxzatOKRGu7U_6F9cQOlHd2H3h2Sf2uC7HPUkpVZxGJr5ZMDN8zzn6hMvUavWBdBYr1zS" width="800">

### Low Mass Stars
- With less gravity, burns hydrogen fuel slowly and lasts for 100 billion years, matures into red dwarf, and when fuel for nuclear fusion runs out, becomes a white dwarf

### Medium Mass Stars
- Lasts for 10 billion years
- When a medium mass star runs out of fuel, it collapses under its own gravity, collapse heating up and pressure increases causing nuclear fusion of helium
- Star expands and becomes a red giant, eventually burning out to form a white dwarf
- When white dwarfs become cool enough to no longer emit heat or light, they become black dwarfs, however since the time required for a white dwarf to reach this state is older than the Universe, no black dwarfs currently exist

### High Mass Stars
- Lasts up to 7 billion years, 10 times size of our Sun
- When high mass star runs out of fuel it collapses and expands to form a supergiant
- Supergiants end in a violent massive explosion called a supernova
- End results - Cosmic debris (nebula), a neutron star (or pulsar) or a black hole 

### Supernova
- Supergiants that run out of fuel end in a massive explosion
- Nuclear fusion reactions occur and new elements form and explode into space
- Debris from explosion is source for a new nebula, and what happens to the stars remaining core depends on original size of the star

### Neutron Stars
- Remaining core of a supergiant that is less than 40 times the size of our Sun
- Also called a pulsar, very dense matter made of neutrons

## Black Holes
- Remaining core of a supergiant that needs to be more than 40 times the size of our Sun
- Core of the supergiant after a supernova is so dense that its gravitational pull sucks in space, time, light, and matter
- Thought to be at the centre of all galaxies

## Formation of Stars
|Stage|Description|Picture|
|:----|:----------|:------|
|1. Birth and Early Life|- Life for a star begins in a **nebula**, which are HUGE, unevenly distributed clouds of dust and gases (**mainly H** & **He**).<br>- Denser areas gather surrounding material due to greated **gravitational pull**<br>- As material is added, gravity increases , drawing in even more material… then density and pressure increase as well.<br>- This core and surrounding material start spinning more as they continue to condense. (like a figure skater)<br>- Any surrounding dust and gases that aren’t drawn into the core will **flatten out** to look like a disc around the core. (the natural tendency for all spinning objects)<br>- **Temperature begins to rise** due to atomic collisions and start emitting **low level energies like microwave & infrared**.<br>- This is now called a protostar.|<img src="https://www.myastroshop.com.au/guides/vc200l/helix-judge-s.jpg" width="500">|
|2. Main sequence phase (adult star)|- As core temperature reaches a critical point (15 million °C), **NUCLEAR FUSION begins** and it becomes a *star**. <br>- H atoms join to form He atoms, releasing enormous amounts of **high energy radiation**, which also **emits light energy.**|<img src="https://lifecycleofstarsscience.weebly.com/uploads/1/8/8/2/18823332/2663663_orig.jpg?363" width="500">|
|3. Old Age|- Once a star’s core has **used up its H**, it fuses **He**, which **releases even more energy**. <br>- This causes the star to swell into a **red giant** or **red supergiant** depending on their original mass.|<img src="https://images.newscientist.com/wp-content/uploads/2018/01/18185636/gettyimages-648917144-800x533.jpg" width="500">|
|4. Death|- An average star “dies” when it doesn’t have enough energy to continue **nuclear fusion** (usually once it forms **carbon**). <br>- For a star like our sun, the core shrinks/collapses, releasing the outer layers of gases. <br>- The `small, hot, and dense core` becomes a **white dwarf**, while the outer gases form a new **nebula** around it. This combo is called a **planetary nebula**. <br>- A more massive star will do fusion up until **iron** then collapse, but the outer layers will explode off this iron core to form a **supernova**.|<img src="https://i.ytimg.com/vi/aysiMbgml5g/maxresdefault.jpg" width="500">|
|5. Remains|- **Small red giants** collapse & shrink into a **white dwarf**, which will slowly cool down and eventually **fade out** (no energy emitted) to be a **black dwarf**.<br>- **Large red giants** explode as a **supernova**, & will form either a **neutron star** or even a **black hole** if the core has enough mass.|<img src="https://fortunedotcom.files.wordpress.com/2019/04/first-image-of-black-hole-national-science-foundation.jpg" width="500">

<img src="https://files.catbox.moe/la2ho5.png" width="500">

## Space Composition
- <img src="https://files.catbox.moe/q0ofhp.png" width="500">

### Dark Matter
-  The rest of the universe appears to be made of a mysterious, invisible substance called dark matter (25 percent) and a force that repels gravity known as `dark energy` (70 percent). Scientists have not yet observed `dark matter` directly.
- `90%` of matter in and `between` galaxies is of an `unknown` form that `does not emit or absorb light (so we can’t see it)`.
- It can be detected through its `gravity` by the way it `affects` objects we can see. 
- Without dark matter, `normal matter` would have been unable to `clump` and `form` stars and `galaxies` - and US!
