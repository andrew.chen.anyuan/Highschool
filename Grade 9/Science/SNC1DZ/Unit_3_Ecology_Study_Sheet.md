# Unit 3: Ecology

## Terms
- `Habitat`: Place where organisms live
- `Biotic`: Living components (their remains AND features)
    - Bears, insects, micro-organisms, nests
- `Abiotic`: Non-living components
    - Physical/chemical components
    - Temperature, wind, humidity, precipitation, minerals, air pressure
- `Sustainability`: **The ability to maintain natural ecological conditions without interruption, weakening, or loss of value.**
- `Community`: Individual from all of the DIFFERENT populations (communities of different species)
- `Ecosystem`: Term given to the community and its interactions with the abiotic environment 
- `Sustainable Ecosystem`: An ecosystem that is maintained through natural processes
- `Ecological niche`: Every species interacts with other species and with its environment in a unique way. This is its role in an ecosystem (e.g. what it eats, what eats it, how it behaves, etc.)
- `Biodiversity`: The variety of life in a particular ecosystem, also known as biological diversity.
    - Canada is home to about 140 000 to 200 000 species of plants and animals. Only 71 000 have been identified.
- `Species Richness`: the number of species in an area.
    - Diverse ecosystem = high species richness.
    - Higher close to the equator.
    - Ex. Amazon rainforest home to more than 200 species of hummingbirds, Ontario only has a single species.
- `Population`: A group of organisms of one species that interbreed and live in the same place and time.
    - Population often change due to both **natural and artifical** factors (human activity). 
- `Carry Capacity`: The maximum population size of a **particular species** that a given ecosystem can sustain.
- `Pollution`: harmful comtaminants released into the enviornment.
- `Bioremediation`: the use of micro-organisms to consume and break down environmental pollutants.
- `Photosynthesis`: The process in which the Sun’s energy (LIGHT) is converted (put together with) into chemical energy AS GLUCOSE (sugar).
- `Succession`: The gradual and usually predictable changes in the composition of a community and the abiotic condtions following a disturbance.
- `Producer`: Organism that makes its own energy-rich food using the Sun’s energy.
- `Consumer`: Organism that obtains its energy from consuming other organisms.
- `Eutrophication`: Overfertilzation of staganat bodies of water with nutrients
- `Heterotrophs` Organisms that feed on others
- `Bioaccumulation`: The process by which **toxins accumulate in the bodies** of animals. (Eg, DDT). **They cannot be easily excreted from the body.**
- `Bioamplification`: The **increase in concentration of a substance** such as a pesticide as we move up trophic level within a food web. **It happens because of bioaccumulation**. (Sometimes called `biomagnification`).
- `Oligotrophic` Bodies of water that are **low** in nutrients. (clear water, opposite to `eutrophic`).
- `Watershed` (drainage basin): Area of land where **ALL WATER** drains to a single river or lake.
- `Invasive Species`: A non-native species whose intentional or accidental introduction negatively impacts the natural environment.
- `Population` = (Birth Rate + `Immigration` (People coming in)) - (Death rate + `Emigration`(People leaving)).
- `Argoecosytems`: Is essentially an agricultural ecosystem, engineered ecosystems: urban centres, roads, etc.
- `Monocultures`: cultivation of a single crop in an area.
    - Bad, bees can't pollinate, as there is only one crop, destruction of one crop will result in total food shortage from that crop, as we are only growing one crop. Also nutrient cycling is disrupted and less crops can be grown at the same rate.
- `Pest`: Any plant, animal, or other organism that causes illness, harm, or annoyance to humans.
- `Interspecific`: Different species fighting.
- `Intraspecific`: Same species fighting.
- `Watershed`: An area or ridge of land that separates waters flowing to different rivers, basins, or seas.
- `Intertidal Zone`: The intertidal zone, also known as the foreshore and seashore, is the area that is above water at low tide and underwater at high tide.
- `Limiting Factor`: Any factor that restricts the size of a population.
- `Tolerance Range`: The abiotic conditions within which a species can survive.

## The Spheres of Earth

### Atmosphere
- The layer of `gases` above Earth's surface, extending upward for hundreds of kilometers.
    - `78% nitrogen gas`.
    - `21% oxygen gas`.
    - `< 1% argon, water vapour, carbon dioxide & other gases`.
- Critical to (almost all) life on Earth.
- Acts like a **blanket & moderates surface temperature**.
- Insulation prevents excessive **heating** during the day & **excessive cooling** during the night.
- Average surface temperature droup from **15C to -18C**.
- Blocks some **solar radiation (most ultraviolet light)**.

    
### Biosphere
- The regions of Earth where `living organisms` exist.
- Describes **the locations in which life can exist within the lithosphere, atmosphere and hydrosphere**.
- Biosphere is thin in comparison to diameter of the Earth.
- ALL conditions required for **life must be met and maintained within this thin layer of ground, water, and nutrients to survive**.

### Hydrosphere
- All the `water` found on Earth, above and below the Earth's surface.
- Includes
    - **Oceans**
    - **Lakes**
    - **Ice**
    - **Ground Water**
    - **Clouds**
- 97% of water on Earth **is in the oceans**.


### Lithosphere
- The `hard part` of Earth's surface.
- **Rocky outer shell of Earth**.
- Consists of:
    - **Rocks and minerals that make up mountains, ocean floors, and Earth's solid landscape**
-Thickness: **50 - 150km**.


## Energy Flow
- `Law of Conservation of Energy`: Energy **can not** be **created** or **destroyed**. It can only be transformed or transfeered.
- Note that Photosynthesis and Cellular respiration are nearly **THE EXACT OPPOSITE**.

### Types of Energy
- #### Radiant Energy
    - Energy that travels through EMPTY SPACE
- #### Thermal Energy
    - Form of energy TRANSFERED DURING HEATING/COOLING
    - Keeps the Earth's surface warm
    - CANNOT provide organisms with energy to grow & function
- #### Light Energy
    -  VISIBLE forms of radiant energy
    - Can be used by some organisms (CANNOT be stored)
- #### Chemical Energy
    - Used by living organisms to perform functions (growth, reproduction, etc.)
    - MUST be replaced as it is used


### Photosynthesis
- Plants use the sun to make energy in the form of glucose or sugar.
- Animals cannot make their own food (glucose, energy).
    - Must get our food from plants.
- Plants are the first step in the food chain.
- Oxygen released during photosynthesis is necessary for all living things.
- 6CO<sub>2</sub> + 6H<sub>2</sub>O = C<sub>6</sub>H<sub>12</sub>O<sub>6</sub> + 6O<sub>2</sub>
- Carbon Dioxide + Water & Light energy =  Sugar + Oxygen. 
- The energy are stored in the plants stems, leaves, roots and seeds.

### Cellular Respiration
- Process of converting sugar into carbon dioxide, water and energy
- Makes stored energy available for use, Takes place in the mitochondria
- C<sub>6</sub>H<sub>12</sub>O<sub>6</sub> + 6O<sub>2</sub> = 6CO<sub>2</sub> + 6H<sub>2</sub>O + energy.
1. Original energy stored in the sugar is released
2. Occurs continuously
3. Does NOT require light energy
- **BOTH** producers **AND** consumers perform cellular respiration
- ALL humans are consumers (unless you’re the hulk)

#### Steps in Cellular Respiration
- Mitochondria takes in nutrients
    - Glucose and Oxygen
- Breaks both nutrients down
    - Creates energy for the cell
- #### REVERSE of Photosynthesis
    - Sugar breaks down into **CARBON DIOXIDE** and **WATER**
        - Release of energy when this happens
    

## Feeding Relationships
- Energy flow through an ecosystem in one direction, from the sun or inorganic compounds to autotrophs (producers) and then to various hetrotrophs (consumers).
- Food are a series of steps in which organisms transfers energy by eating or eaten (pg. 43).
- Food webs show the complex interactions within an ecosystem (pg. 44).
- Each step in a food chain or web is called a `trophic` level. Producers make up the first step, consumers make up the higher levels. E.g. first trophic level are producers, second trophic level are primary consumers, etc.
- Detrivores + scavengers are off to side (with all arrows pointing on it).
- **First Trophic Level**: `Plants`.
- `10% rule`, Only 10% of energy is stored in each organism, 90% of energy is lost (heat consumption).

## Ecological Pyramids
- Food chains and food webs do not give any information about the numbers of organisms involved.
- This information can be shown through ecological pyramids.
- An ecological pyramid is a diagram that shows the amount of energy or matter contained within each trophic level in a food web or food chain.

<img src="https://www.tutorialspoint.com/environmental_studies/images/upright_pyramid.jpg" width="300"> 

|Pyramid|Description|Picture|
|:------|:----------|:------|
|Pyramid of Biomass|Show the **total** amout of `living tissue` available at each `trophic` level. This shows the amount of tissue available for the next `trophic` level. <br> <br> Biomass is preferred to the use of numbers of organisms because individual organisms can vary in size. It is the `total mass` **(not the size)** that is important. Sometimes it’s inverted. <br> <br> Pyramid of biomass records the total dry organic matter of organisms at each trophic level in a given area of an ecosystem.|<img src="http://earth.rice.edu/mtpe/bio/biosphere/topics/energy/biomass_pyramid.gif" width="800">
|Numbers Pyramids|Shows the number of organisms at each trophic level per unit area of an ecosystem. <br> <br> Because each trophic level harvests only about `one tenth` of the energy from the level below, it can support only about one `10th` the amount of living tissue. <br> <br> **`Can be inverted`**: 1 large tree supports thousands of organisms living on it <br> <br> Pyramid of numbers displays the number of individuals annualy.|<img src="https://d321jvp1es5c6w.cloudfront.net/sites/default/files/imce-user-gen/pyramidnumbers2.png" width="400">|
|Energy Pyramid|Shows the amount of energy input to each trophic level in a given area of an ecosystem over an extended period.<br> <br> **CANNOT** be inverted, due to energy transfers<br> <br> **Only 10% of the energy available within one trophic level is transferred to organisms at the next trophic level.**|<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/3a/Ecological_Pyramid.svg/1200px-Ecological_Pyramid.svg.png" width="500">|

**NOTE FOR ENERGY PYRAMIDS**: In nature, ecological
efficiency varies from `5%` to `20%` energy available between successive trophic levels (`95%` to `80%` loss). About 10% efficiency is a general rule. `Rule of 10’s` at each level.


## Biogeochemical Cycles

|Cycle|Picture|
|:----|:------|
|Water Cycle|<img src="https://www.noaa.gov/sites/default/files/styles/scale_crop_1120x534/public/thumbnails/image/watercycle_rc.png?itok=CcUyhuxd" width="800">|
|Carbon Cycle|<img src="http://climatechangenationalforum.org/wp-content/uploads/2014/09/carbon_cycle_1.jpg" width="700">|
|Nitrogen Cycle|<img src="https://scienceprogress.org/wp-content/uploads/2008/06/nitrogen_cycle_591.jpg" width="700">|

## Water Cycle
- Continuous movement of water on, above and below the surface of the Earth.
- Water moves from one reservoir to another (ocean to atmosphere, river to lake)
    - Forms: Solid (ice), Liquid (water), Gas (vapour)

### Steps/Process:
- Exchange of energy leads to:
    - Temperature Change, Climate.
    - Condenses 🡪 occurs during cooler temp, water cooles and collects in clouds.
    - Precipitation 🡪 After condensation, water falls down from the clouds in the form of prefcipitation, can be commonly described as rain.
    - Transpiration 🡪 Water leaves from plants, plants losing water to air.
    - Infiltration/percolation 🡪 Water seeps into the ground, in between small cracks in the rocks and the Earth.
    - Evaporation 🡪 happens during warmer temp.
    - Surface Run Off 🡪 Water flows above the ground.

### Importance
- **Evaporation**:
    - Purifies the water.
    - New fresh water for the land
- **Flow of liquid water and ice**
    - Transports minerals across the globe.
- **Reshaping the geological features of Earth**
    - Erosion and sedimentation.

### Human Inpacts
- Humans building dams (flooding is a problem!).
- Deforestation contributes to global warming, hence melting glaciers and causing flooding in cities.
- (Also less transpiration from clear cutting) – pg. 48.
- Factories and cars pollute the air, leading to acid precipitation.
- Oil spills destroy aquatic ecosystems.

## Carbon Cycle
- Fourth most abundant element in universe
- Building block of all living things
- Main Pathway – in and out of living matter
- All living organisms contain carbon.

### Steps/Process

- CO<sub>2</sub> is a waste product of cellular respiration
- Plants use carbon dioxide and water to form simple sugars (photosynthesis)
- Light Energy 🡪 Chemical Energy

- Photosynthesis 🡪 converts carbon into sugr to form simple sugars.
- Biodegration 🡪 Animal remains decompose and turn into fossil fuels.
- Respiration 🡪 Animals respirate and give off carbon into the atmophere.
- Burning of fossil fuels 🡪 Release the carbon contained in the fossil fuels.

### Importance
- **Phtosynthesis**
    - CO<sub>2</sub> is converted to glucose using water and sunlight
- **Cellular Respiration**
    - Breaks down glucose to release energy, expel CO<sub>2</sub>
- **Oceans are a HUGE carbon sink**.

<img src="https://www.news-medical.net/image.axd?picture=2017%2F6%2Fshutterstock_581980219.jpg" width="500">

### Human Impacts
- **Mining & burning fossil fuels**: Speed up release of CO<sup>2</sub> to the atmosphere.
- **Deforestation & clearing vegetation**:  ↑ CO<sub>2</sub> in atmosphere.
- **Acid rain**: release CO<sub>2</sub> from limestone.
- CO<sub>2</sub> in the atmosphere is now higher than it has been in at least **800 000 years**.

## Nitrogen Cycle
- The most abudant gas in the atmopshere (~78%)
- `Nitrogen Fixation`: The process that causes the strong two-atom nitrogen molecules found in the atmopshere to break apart so they can combine with other atoms.
- `Nitrogen gets fixed`: When it is combined with oxygen or hydrogen.
- An essential component of DNA, RNA, and protenis - the building blocks of life.
- Atmopspheric nitrogen = N<sub>2</sub>
    - Most living organisms are `unable` to use this form of nitrogen
    - Therefore, must be **converted** to a usable form!

### Steps/Process
- Nitrogen gets fixed, but still needs to be broken in to **usable form**. It travels through the ground. `Ammonium` gets produced from the fixed nitrogen, then `nitrifying` bacteria turns that into **`nitrites`**, which is **NOT** usable yet. Nitrifying bacteria then convert **nirites** into **usable nitorgen**, **`nitrates`**.
- Plants then use **`nitrates`** for energy, where then **decomposers** turn into ammonium.
- Denitrification 🡪 `Denitrifying` bacteria break down  **`nitrates`**, **usable nitrogen** back into atmospheric nitrogen.

### Nitrogen Fixation
- Most of the nitrogen used by living things is taken from the atmosphere by certain bacteria in a process called `nitrogen fixation`. 
- These microorganisms convert nitrogen gas into a variety of nitrogen containing compounds such as nitrates, nitrites, and ammonia
- Lightning and UV radiation also fix small amounts of it
- Humans add nitrogen to soil through fertilizer
- 3 ways nitrogen to get fixed
    1. Atmopheric Fixation
        - Lightning Storms
            - stroms and fuel burning in car engines produce nitrates, which are washed by rain into soil water. 
    2. Industrial Fixation
    3. Biological Fixation
        - 2 types
            1. Free living Bacteria
                - Highly specialized bacteria live in the soil and have the ability to combine atmospheric nitrogen with hydrogen to make ammonium(NH<sub>4</sub><sup>+</sup>).
                - Nitrogen changes into ammonium.
            2. Symbiotic Relationship Bacteria
                - Bacteria live in the roots of legume family plants and provide the plants with ammonium(NH<sub>4</sub><sup>+</sup>) in exchange for the plant's carbon and a protected biome.
- `Nitrites` are absorbed by plant roots and converted to plant protein.
- `Nitrates` **can be absorbed by other plants** to continue the cycle.
- `Denitrifying bacteria` convert soil nitrates into N<sub>2</sub> gas
    - This is a `loss` of N<sub>2</sub> from the cycle 

### Human Impacts
- Nitrates also `enters` the cycle **through the addition of nitrogen rich fertilizers to the soil** – made industrially from nitrogen gas (Eutrophication – pg. 60) 
- Factories release NO compounds (acid rain)

## Changes In Population
- The carry capcacity of an ecosystem depends on numerous biotic and abiotic factors.
- These can be classified into two categories.
    1. `Density dependent factors`
    2. `Density independent factors`

### Density Independent Factors
- DIF’s can affect a population no matter what its density is. The effect of the factor (such as weather) on the size of the population **does not** depend on the **original size** of the population.
- Examples:
    - unusual weather
    - natural disasters
    - seasonal cycles
    - certain human activities—such as damming rivers and clear-cutting forests

### Density Dependent Factors
- DDF’s affect a population **ONLY** when it reaches a certain size. The effect of the factor (such as disease) on the size of the population depends on the **original size** of the population
- Examples:
    - Competition
    - Predation
    - Parasitism
    - Disease

## Relationships
1. **Symbiosis**
    - Two different organisms associate with each other in a close way.
    - Is the interaction between members of `two different species` that live together in a close association.
    - **Mutualism (+/+)**
        - Both species benefit from the relationship.
        - (eg. human intestine and good bacteria, bees and flowers, clownfish and sea anemone, cattle egret and cow).
    - **Commensalism (+/0)**
        - one species benefits, the other is **unaffected**. 
        - (eg. beaver cutting down trees, whales and barancles).
    - **Parasitism (-/+)**
        - one species is harmed, the other **benefits**. 
        - (eg. lice and humans, mosquito and humans).
    - **Competition (-/-)**
        - neither species benefits. Can be harmed. (-/-).
    - **Neutralism (0/0)**
        - both species are unaffected (unlikely).
        - True neutralism is extremely unlikely or even impossible to prove. One cannot assert positively that there is absolutely no competition between or benefit to either species.
        - Example: fish and dandelion
     
     <table class="table" style="max-width:80%">
     <tr>
     <th><b>+</b></th>
     <td><b>Parasitism and Predation</b></td>
     <td><b>Commensalism</b></td>
     <td><b>Mutalism</b></td>
     </tr>
     <tr>
     <th><b>0</b></th>
     <td></td>
     <td><b>Neutralism</b></td>
     <td><b>Commensalism</b></td>
     </tr>
     <tr>
     <th><b>-</b></th>
     <td><b>Competition</b></td>
     <td></td>
     <td><b>Parasitism and Predation</b></td>
     </tr>
     <tr>
     <th></th>
     <th><b>-</b></th>
     <th><b>0</b></th>
     <th><b>+</b></th>
     </tr>
     </table> 

2. **Competition**
    - Individuals compete for limited resources
        - Types
            - **Intraspecific Competition**
                - Is the competition between individuals of the **same** species.
                - (eg. male deer uses antlers to fight each other for mates, little herons compete for food).
            - **Interspecific Competition**
                - Is the competition between individuals of **different** species.
                - (eg. cardinals and blue jays at a bird feeder, lions and hyenas competing for food).

3. **Predation**
    - One animal eats (kills) another.
    - Like a eagle eating a mouse or something.

### Reasons To Compete
- Food and water.
- Space (habitat).
- Mates.

## Candian Biomes
<img src="https://slideplayer.com/slide/12708159/76/images/41/Canada%E2%80%99s+Biomes+Mountain+Forest+Tundra+Boreal+Forest+Grassland.jpg" width="800">

### Tundra
* Most **NORTHERN** biome of Canada.
* Low temperatures + lots of **PERMAFROST**
* Low decomposition rate.
* Plants grow slower due to cold
* `Species`: Polar bears, Caribou, Arctic foxes.

### Boreal Forest
* **Largest** biome in Canada.
* Warmer weather+plenty rainfall.
* Acidic Soil -  Limits variety of plants + slows down decomposition.
* `Species` Grey wolves, conifers, moss, black bears.

### Grassland
* Moderate rainfall (supports grass not trees).
* Dry $`\rightarrow`$ Fire $`\rightarrow`$ Fire prevents larges trees from growing.
* Very **Fertile** black soil (high decomposition rate)
* Large portions of this biome are replaced by farms in Canada.
* `Species`: Bison, Snakes, fescue grasses, voles.

### Temperate Deciduous Forest
* Layers of canopy trees, understorey trees, shrubs, ground vegetation.
* Variety oof plants + species.
* Fast decomposition rate (warm temperatures).
* Large portions of this biome used by humans for cities.
* `Species`: Shrews, decidious trees, deer, black bears.

### Mountain Forest
* Temperatures vary with elevation
* Windy + cool summers
* Heavy precipitation on leeward side of mountains
* `Species` Elk, cougar, large coniferous trees, ferns.

## Biodiversity
- There are alot of threats to biodiversity, pollution, invasive species etc.

### Pollution / Habitat Loss
- Pollution is caused everywhere and disrupts ecosystems. Like plastic pollution, which has killed many marine life such as turtle, as they believe plastic bags are jellfish and eats it and dies.
- We can easily manage this, by just reducing the amount of pllution done.
- Habitat loss is a result due to our greedy mind for resources such as trees. We deforest lots of trees, using unsustainable methods, such as **clear-cuttig**.
- We can easily manage this, with things such as **stewardship**, and using sustainble methods such as **selective cutting**.

### Invasive Species
- Introduction usually fails because few species can tolerate an entirely new environment.
- Can adapt to abiotic environment, may have difficulty finding food/cant deal with competition.

#### Impacts
- **Ecological**
    - Competition, food, alter nutrient cycles.
- **Economic**
    - Damage forests/crops = financial loss, diseases/pests destroy crops, trees and livestock.
- **Tourism**
    - Species loss and reduced water quality = poor wildlife viewing, fishing and water based activities.
    - Waterways choked with invasive aquatic plants = no boats.
- **Health**
    - Cause disease (west nile), pesticides used for control cause pollution and are health risks.

#### Controlling Measures
1. **Chemical Control** 
    - Most widely used = pesticides.
    - Used on forest/agricultural pests.
    - Pesticides dramatically reduce crop damage.
    - Environmental Risks.
        - May kill non-target native species/pollute air, water, soil.
2. **Mechanical Control**
    - Physical barriers or removal (cut down, burned, hunted).
    - Ex. Hamilton Harbour barrier to prevent Carp invasion. 
3. **Biological Control**
    - Challenging but effective.
    - Uses intentionally introduced organisms to control the invasive species.
    - Ex. 3 insect species released in Ontario to control purple loosestrife (invasive plant that grows in wetlands).
        - Tests indicated the insects are unlikely to feed on native plants.
    - Rarely eradicates an invasive species … may reduce population sizes to tolerable levels.


## Introducing Ecosystems
- Most ecosystems are **SUSTAINABLE**.
- Usually, a bigger food web/food chains are more sustainable, as its more resilent to change.
<table class="table">
<tr>
<th>Ecosystem</th><th>Key abiotic factors</th><th>Human action and result</th>
</tr>
<tr>
<th>Terrestrial Ecosystems</th>
<td>Light availability <br><br>Water availability<br><br>Nutrient availability<br><br>Temperature</td>
<td>Clear-cutting and fire remove shade and expose the remaining organisms to much more light<br><br>Damming rivers and draining swamps and marshes change water availability. Irrigation increases water availability<br><br>Farming practices may increase or decrease nutrient levels in the soil.<br><br>Global warming is decreasing suitable habitat for many cool-adapted species.</td>
</tr>
<tr>
<th>Aquatic Ecosystems</th>
<td>Light availability<br><br><br>Nutrient availability<br><br><br>Acidity<br><br><br>Temperature<br><br>Salinity<br><br></td>
<td>Activities that increase erosion or stir up the bottom cloud the water and reduce light penetration.<br><br>Nutrient runoff from agriculture and urban enviornments increases the nutrient content of <br> surface water and groundwater, causing algal blooms<br><br>Acidic air pollution results in acid precipitation. Carbon dioxide emissions <br>produced by the burning of fossil fuels are increasing the acidity of the oceans.<br><br>Industries and power plants release heated waste water into lakes and rivers, killing fish and other organisms.<br><br>Salting highways and long-term irrigation practices can cause salt to accumlate.<br></td>
</tr>
</table>

- `Limiting factors` can be something such as access to water, availability of food. Can be **both** `abiotic` and `biotic`. Human influences often act as `limiting factors`. 
- The carry capacity of an ecoystem is a limit where the organisms in that ecosystem can sustain themselves, human impacts often lower the `carry capacity`, as things such as pollution, destruction of habitats drastically lowers the `sustainability` of a ecosystem. In general, `limiting factors` link directly to `carrying capacity` as the the factors tell us the size that that the ecosystem can sustain itself. Which is essentially the `carrying capacity`.

### Successions
- **Primary Succession**: occurs on soil or bare rock, where no life previously existed, such as following a volcanic eruption
- **Secondary Succession**: follows a disturbance that disrupts but does not destroy the community. The regrowth of an area following a forest fire is an example of secondary succession.
- Succession provides a mechanism by which ecosystems maintain their long-term sustainability. It also allows ecosystems to recover from natural or human-caused disturbances.

### Argoecosystem v.s Natural
- The natural ecosystem is more sustainable, as there is a abudance of biodiversity, unlike argoecosystem which are usually monocultured. Which is unsustainble.
- We need both of these ecosystems, as we are in need of very fast supply of certain foods due to the vast demand of it.
- Leaching happens in agroecosystems, where nitrogen rich water from fertilizers seep into the soil. This causes nutrients to dissolve in water and seep out of the soil. Increase growth of algae, (think eurotrophication).

### Ecosystem Services/Products
- **Cultural Services**
    - Benefits relating to our enjoyment of the environment.
    - Ex. Recreational, aesthetic and spiritual experiences when we interact with natural surroundings.
    - Ecotourism: tourists engage in environmentally responsible travel to relatively undisturbed natural areas.
    - Ex. Canada’s Wilderness.
- **Ecosystem Products**
    - Humans use products produced by the ecosystem.
    - Hunt animals and harvest plants, lakes/oceans supply us with seafood.
    - **Terrestrial:** ecosystems: medicines, fibres, rubber and dyes.
    - **Forestry**: largest industries and employers.
- **Regulate** and **maintain** important **abiotic** and **biotic** features of environment.
    - Cycle water, oxygen, and nutrients.
- Help protect us from physical threats.
    - Plant communities protect the soil from wind and water erosion.
- Ecosystems act as sponges.
    - Absorb water and slowly release it into the groundwater and surface water (reduces erosion and protects against flooding, filters the water).
- Protect land from storms along coasts where wave damage erodes the shoreline.
    - Mangroves.
- **Monetary Values**
    - Very difficult to put a dollar value on it
    - Dollar value of cleaning the air/water, moderating climate and providing paper fibre, medicinesand other products is HIGH
    - Ranges into trillions dollars/year (maybe 60 trillion?)
    - Provides valuable services that are free and renewable.

### Pesticides
 - Pesticides can easily flow through food chains as there are not easily excreted, like DDT. There are fat-solutable.
 - DDT can be passed through organisms, from generation to generation, as the food we eat contains small amounts of DDT, and thus, the cycles continues. It doesn't matter how many generations ahead you are, everyone in the world contains some amount of DDT in their bodies.
 - Pesticide resistance?

## Article Questions
1. Explain the **three** components of biological diversity?
    - Genetic diversity
    - Ecological diversity
    - Species diversity
2. Why is this considered to be the plants most valuable resource? List 5 examples
    - Provide us with wood, food, medicines, raw materials + fibers 
3. Why has the extinction rate increasedi in the last 10,000 years? List 3 reasons
    - Beause we ruin organisms habitats, eg. destruction + deforestation of wetands + coroal reefs.
4. Explain why the reduction in diversity cannot be fixed through genetic engineering
    - Since genetic engineering depends on natural biodiversity for its raw material, you cannot rereate a certain species without the species exisiting itself.

1. Achim Steiner, Explain the tragedy is he referring to?
    - The tragedy of climate change being related to biodiversity
2. What are human health systems throughout history all based on?
    - Animals -frogs, amphibians, bears, cone snails, non-human primates, horseshoe crabs, etc.
3. Describe **three** organisms and their potential contribution to pharmaceutial science
    - Pananina poison frog, makes pumilations that can help heart disease
    - Ecuadorian poson frog, source 4 painkillers
    - Cone snails; compound in clinical trails 4 advanced cancer + AID painkillers
4. What are the basic needs that must be perserved "at all cost"? List 5.
    - Clean air, clean water, clean soil, clean energy from sun, and biodiversity.

1. Why was palm oil deemed an attractive biofuel? List 4 reasons.
    - Cheap, renewable ,alternative to petroleum, fights global warming, carbo-neutral , abudant, requires few-no modifications. 
2. How was the production of palm oil proved it to be a failure as a biofuel? Describe 2 reasons.
    - Plantations on drained peat bags, where CO<sup>2</sup> seeps into the air (600 million /tonnes/ year)
    - Smoke from fires to clear rainforests for plantations (1.4 billion tonnes /year)
3. Has the knowledge of the damaging effects of palm oil production made an impact on overall production? Explain your reasoning.
    - Yes and no; more companis are backing out due to lack of sustainability but other /more companies are joining in because of the great way to make profit.