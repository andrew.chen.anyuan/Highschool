# Unit 4: Measurement and Geometry
## Angle Theorems

1. ```Transversal Parallel Line Theorems``` (TPT)     
    a. Alternate Angles are Equal ```(Z-Pattern)```   
    b. Corresponding Angles Equal ```(F-Pattern)```    
    c. Interior Angles add up to 180 ```(C-Pattern)```      

  - <img src="https://dj1hlxw0wr920.cloudfront.net/userfiles/wyzfiles/58a52a99-05da-4595-88b8-2cbca91e8bbf.gif" width="300">   

2. ```Supplementary Angle Triangle``` (SAT)     
  - When two angles add up to 180 degrees     

  - <img src="https://embedwistia-a.akamaihd.net/deliveries/cdd1e2ebe803fc21144cfd933984eafe2c0fb935.jpg?image_crop_resized=960x600" width="500">   

3. ```Opposite Angle Theorem (OAT)``` (OAT)    
  - Two lines intersect, two angles form opposite. They have equal measures    

  - <img src="https://images.slideplayer.com/18/6174952/slides/slide_2.jpg" width="400">   

4. ```Complementary Angle Theorem``` (CAT)    
  - The sum of two angles that add up to 90 degrees     

  - <img src="http://images.tutorvista.com/cms/images/67/complementary-angle.png" width="300">   

5. ```Angle Sum of a Triangle Theorem``` (ASTT)   
  - The sum of the three interior angles of any triangle is 180 degrees    

  - <img src="https://dj1hlxw0wr920.cloudfront.net/userfiles/wyzfiles/f0516fa1-669b-441d-9f11-a33907a2a0b0.gif" width="300">   

6. ```Exterior Angle Theorem``` (EAT)
  - The measure of an exterior angle is equal to the sum of the measures of the opposite interior angles      

  -<img src="https://www.katesmathlessons.com/uploads/1/6/1/0/1610286/exterior-angle-theorem-diagram-picture_orig.png" width="300">

7. ``` Isosceles Triangle Theorem``` (ITT)   
  - The base angles in any isosceles triangle are equal   

  - <img src="http://www.assignmentpoint.com/wp-content/uploads/2016/06/isosceles-triangle-theorem.jpg" width="400">

8. ```Sum of The Interior Angle of a Polygon```
  - The sum of the interioir angles of any polygon is ```180(n-2)``` or ```180n - 360```, where ```n``` is the number of sides of the polygon

  - <img src="https://i.ytimg.com/vi/tmRpwCM1K1o/maxresdefault.jpg" width="500">


9. ```Exterior Angles of a Convex Polygon```
  - The sum of the exterior angle of any convex polygon is always ```360 degrees```   

  - <img src="https://image.slidesharecdn.com/findanglemeasuresinapolygon-110307143453-phpapp02/95/find-angle-measures-in-a-polygon-11-728.jpg?cb=1299508555" width="400">


## Properties of Quadrilaterals
- Determine the shape using the properties of it   

 |Figure|Properties|   
 |:-----|:---------|   
 |Scalene Triangle|no sides equal|Length of line segment|   
 |Isosceles Triangle| two sides equal|Length of line segment|   
 |Equilateral Triangle|All sides equal|Length of line segment|   
 |Right Angle Triangle|Two sides are perpendicular to each other|    
 |Parallelogram|Opposite sides are parallel and have equal length. Additionally, the diagonals bisect each other|    
 |Rectangle|Adjacent sides are perpendicular to each other. Furthermore, the diagonals bisect each other and are equal in length|
 |Square|All sides are equal in length. The adjacent sides and diagonals are perpendicular. The adjacent sides are equal in length, so as the diagonals|
 |Rhombus|Opposite sides are parallel and all sides are equal to each other, the diagonals are perpendicular|
 |Trapezoid|There is one pair of opposite sides and they are parallel and unequal in length|
 |Kite|The diagonals are perpendicular|

## 2D Geometry Equations
 |Shape|Formula|Picture|
 |:----|:------|:------|
 |Rectangle|```Area```: $`lw`$ <br> ```Perimeter```: $`2(l+w)`$|<img src="https://lh5.googleusercontent.com/Ib1Evz5PUwd4PzRmFkHj9IY2Is-UthHoUyyiEHAzkJP-296jZvMmHJM1Kws4PmuTeYHV2ZBIJenc4W1pKtsSHvU82lyjOed2XKBb1PWnoaeJ3sSPuaJgSTg8JWbxrvplabCanvTD" width="200">|   
 |Triangle|```Area```: $`\frac{bh}{2}`$ <br> ```Perimeter```: $`a+b+c`$|<img src="https://lh6.googleusercontent.com/covvHwXxQhrK2Hr0YZoivPkHodgstVUpAQcjpg8sIKU25iquSHrRd2EJT64iWLsg_75WnBw4T9P0OTBiZDkpqEkXxflZQrL16sNhcFfet_z4Mw5EPFgdx_4HzsagV0Sm5jN6EKr_" width="200">|
 |Circle|```Area```: $`πr^2`$ <br> ```Circumference```: $`2πr`$ or $`πd`$|<img src="https://lh5.googleusercontent.com/RydffLVrOKuXPDXO0WGPpb93R8Ucm27qaQXuxNy_fdEcLmuGZH4eYc1ILNmLEx8_EYrRuOuxFavtL9DF1lTWYOx9WaYauVlu0o_UR6eZLeGewGjFNUQSK8ie4eTm1BMHfRoQWHob" width="200">|
 |Trapezoid|```Area```: $` \frac{(a+b)h}{2}`$ <br> ```Perimeter```: $`a+b+c+d`$|<img src="https://lh6.googleusercontent.com/_nceVtFlScBbup6-sPMulUTV3MMKu1nonei0D1WY-KRkpHSbPCIWgDO8UGDQBGKh8i0dkAqOhFUHl7YHCFOt6AMRSJiXALlBBY0mBo1MMZxHRVcg8DknSlv4ng7_QswcZtaRwrJb" width="200">|

## 3D Geometry Equations
|3D Object|Formula|Picture|
 |:----|:------|:------|
 |Rectangular Prism|```Volume```: $`lwh`$ <br> ```SA```: $`2(lw+lh+wh)`$|<img src="https://lh6.googleusercontent.com/-mqEJ4AMk3xDPfqH5kdVukhtCGl3fgTy2ojyAArla54c7UoAnqKW_bsYSaFySXLplE59pqLIg5ANZAL1f6UEejsrKJwQCfyO7gwUQmSDoJJtQG_WkfHcOFDjidXV4Y4jfU2iA5b-" width="200">|
 |Square Based Pyramid|```Volume```: $`\frac{1}{3} b^2 h`$ <br> ```SA```: $`2bs+b^2`$|<img src="https://lh5.googleusercontent.com/iqaaJtx-Kx4vFT3Yp6YLOmpDFL7_qk2uh0Z21pgPJMDRgchiUBcHeTWkMrR9mFjxCj8w7za1xwN9bo4UFACPZRMSl-V67uPv9FvDyNJVjedmeehx5K-iUK9sBhObhNsLJpNItkg0" width="200">|
 |Sphere|```Volume```: $`\frac{4}{3}  πr^3`$ <br> ```SA```: $`4πr^2`$|<img src="https://lh6.googleusercontent.com/DL6ViJLbap2dcSAlZnYKR7c33033g8WuJVvqz0KpzCyIJ0wXyrh5ejoLhrTxlX9uASQlxPmihm8doU1sNbaQxqBcTaPnP-lC6LUrPqzPNi11AHiHQAu3ag7uIGcwzkdC9e5uo1en" width="200">|
 |Cone|```Volume```: $` \frac{1}{3} πr^2  h`$ <br> ```SA```: $`πrs+πr^2`$|<img src="https://lh5.googleusercontent.com/V3iZzX8ARcipdJiPPFYso_il3v_tcrYHZlFnq3qkekRSVBVcj8OzWxMuBqN45aHbv6y-fDH4uY11Gus3KMrvf_Z_TvsfJCwZZ19Ezf7Yj6DzVirp-Gx3V0Qy793ooUwTDmdKW_xq" width="200">|
 |Cylinder|```Volume```: $`πr^2h`$ <br> ```SA```: $`2πr^2+2πh`$|<img src="https://lh5.googleusercontent.com/4uWukD3oNUYBG-fLX2-g58X8at0h74al7BJI5l78LZ0Bu9nXuZnt9dp9xiETeLTqykP-WWFdO_H5by4RkgDVxSENZgootSrAsOUoY2RWubflNOAau1bVFgm9YIe59SmiFlyxwgDV" width="200">|
 |Triangular Prism|```Volume```: $`ah+bh+ch+bl`$ <br> ```SA```: $` \frac{1}{2} blh`$|<img src="https://lh3.googleusercontent.com/_oRUVgfdksfUXGKQk3AtrtY70E8jEq-RRK-lB9yKc_Rtio2f2utGAY-rI4UqjWEeTzUoN_r7EiqdZZeeE12EY-fiV55QQKdvnv4y4VaxQ9xt9Izugp6Ox_LqIUpQzPKVldptgKWm" width="200">|


## Optimization (For Maximimizing Area/Volume, or Minimizing Perimeter/Surface Area)

### 2D Objects   

 |Shape|Maximum Area|Minimum Perimeter|
 |:----|:-----------|:----------------|
 |4-sided rectangle|A rectangle must be a square to maximaze the area for a given perimeter. The length is equal to the width<br>$`A = lw`$<br>$`A_{max} = (w)(w)`$<br>$`A_{max} = w^2`$|A rectangle must be a square to minimaze the perimeter for a given area. The length is equal to the width.<br>$`P = 2(l+w)`$<br>$`P_{min} = 2(w + w)`$<br>$`P_{min} = 2(2w)`$<br>$`P_{min} = 4w`$|
 |3-sided rectangle|$`l = 2w`$<br>$`A = lw`$<br>$`A_{max} = 2w(w)`$<br>$`A_{max} = 2w^2`$|$`l = 2w`$<br>$`P = l+2w`$<br>$`P_{min} = 2w+2w`$<br>$`P_{min} = 4w`$|


### 3D Objects

 |3D Object|Maximum Volumne|Minimum Surface Area|
 |:--------|:--------------|:-------------------|
 |Cylinder(closed-top)|The cylinder must be similar to a cube where $`h = 2r`$<br>$`V = πr^2h`$<br>$`V_{max} = πr^2(2r)`$<br>$`V_{max} = 2πr^3`$|The cylinder must be similar to a cube where $`h = 2r`$<br>$`SA = 2πr^2+2πrh`$<br>$`SA_{min} = 2πr^2+2πr(2r)`$<br>$`SA_{min} = 2πr^2+4πr^2`$<br>$`SA_{min} = 6πr^2`$|
 |Rectangular Prism(closed-top)|The prism must be a cube, <br> where $`l = w = h`$<br>$`V = lwh`$<br>$`V_{max} = (w)(w)(w)`$<br>$`V_{max} = w^3`$|The prism must be a cube, <br>where $`l = w = h`$<br>$`SA = 2lh+2lw+2wh`$<br>$`SA_{min} = 2w^2+2w^2+2w^2`$<br>$`SA_{min} = 6w^2`$|
 |Cylinder(open-top)|$`h = r`$<br>$`V = πr^2h`$<br>$`V_{max} = πr^2(r)`$<br>$`V_{max} = πr^3`$|$`h = r`$<br>$`SA = πr^2+2πrh`$<br>$`SA_{min} = πr^2+2πr(r)`$<br>$`SA_{min} = πr^2+2πr^2`$<br>$`SA_{min} = 3πr^2`$|
 |Square-Based Rectangular Prism(open-top)|$`h = \frac{w}{2}`$<br>$`V = lwh`$<br>$`V_{max} = (w)(w)(\frac{w}{2})`$<br>$`V_{max} = \frac{w^3}{2}`$|$`h = \frac{w}{2}`$<br>$`SA = w^2+4wh`$<br>$`SA_{min} = w^2+4w(\frac{w}{2})`$<br>$`SA_{min} = w^2+2w^2`$<br>$`SA_{min} = 3w^2`$|

## Labelling
- Given any polygons, labelling the vertices must always:   
   1. use ```CAPITAL LETTERS```
   2. they have to be labeled in ```clockwise``` or ```counter-clockwise``` directions    
- For a triangle, the side lengths are labeled in ```LOWERCASE LETTERS``` associated to the opposite side of the vertex   

- <img src="http://www.technologyuk.net/mathematics/trigonometry/images/trigonometry_0073.gif" width="400">

## Median
- Each median divides the triangle into 2 smaller triangles of equal area   
- The centroid is exactly $`\dfrac{2}{3}`$the way of each median from the vertex, or $`\dfrac{1}{3}`$ the way from the midpoint of the opposite side, or ```2:1``` ratio   
- The three medians divide the triangle into ```6``` smaller triangles of equal area and ```3 pairs``` of congruent triangles   

- <img src="https://blog.udemy.com/wp-content/uploads/2014/05/d-median.png" width="500">

## Terms:
- ```Altitude``` The height of a triangle, a line segment through a vertex and perpendicular to the opposite side    
- ```Orthocenter```: where all 3 altitudes of the triangle intersect      
   - <img src="https://mathbitsnotebook.com/Geometry/Constructions/orthocenter1a.jpg" width="300">
- ```Midpoint```: A point on a line where the length of either side of the point are equal   
- ```Median```: A line segment joining the vertex to the midpoint of the opposite side     
- ```Midsegment```: A line joining 2 midpoints of the 2 sides of a triangle   
- ```Centroid```: The intersection of the 3 medians of a triangle   
   - <img src="http://www.mathwords.com/c/c_assets/centroid.jpg" width="300">

## Proportionality theorem:
- The midsegment of a triangle is ```half``` the length of the opposite side and ```parallel``` to the opposite side   
- Three midsegment of a triangle divide ```4 congruent``` triangles with the same area   
- The Ratio of the outer triangle to the triangle created by the 3 midsegments is ```4 to 1```   
- <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSPYlT2JwH4oMYHmpq0DLhBTi1goY0JaRBpNdmZBWgWKSaXAJTM" width="300">

## Tips
- Make sure to know your optimization formualas   
- Read the word problems carefully, determine which formual to use   
- Never **ASSUME**, be sure to **CALCULATE** as most of the time the drawings are **NOT ACCURATE**   
- To find ```missing area```, take what you have, subtract what you don't want      
- Don't be afraid to draw lines to help you solve the problem   
