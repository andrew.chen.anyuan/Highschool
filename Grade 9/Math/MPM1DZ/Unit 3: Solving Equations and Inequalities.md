# Unit 3: Solving Equations and Inequailties

## Equations
- a ```mathematical statement``` in which the value on the ```left side``` equals the value on the ```right side``` of the equal sign     
- To ```solve``` and equation is to find the variable that makes the statement true
### Methods to solve an equation
 1. Expand and simplify both sides
 2. Isolate using reverse order of operations
 3. Check the solution by plugging the variable back into the equation and check if the ```left side``` equals the ```right side```
   
## Absolute Values 
- There are 2 cases. For this sort of equation, you must split the equation into 2 separate equations. One of the
- equations will have the absolute bracket be positive while the other negative.   
- Absolute values are written in the form $`| x |`$
- where    
$`
| x | =
\begin{cases}
x, & \text{if  } x > 0\\
0, & \text{if  } x = 0\\
-x, & \text{if  } x < 0
\end{cases}
`$

## Quadractic Equations
- ```Quadratic Function```: A parabolic graph where the axis of symmetry is parallel to the y-axis    
- ```Quadratic Equation```: This function is set equal to ```0```. The solution to the equation are called ```roots```     
- Solve quadratic equation by:    

  1. Isolation    
     - $`a(x+b)^2 + k = 0`$
  2. Factor using zero-product property    
     - ```The Zero Factor Property``` refers to when $`a \times b=0`$, then either $`a=0`$ or $`b=0`$.     
     - $`(x-a)(x-b)=0`$    
     - $`x = a, b`$      

- <img src="http://www.assignmentpoint.com/wp-content/uploads/2017/12/Quadratic-Expression-1.jpg" width="400">

**Note:**   
- $`\sqrt{x^2} = \plusmn x`$ (There are 2 possible solutions)
- ```Distrubutive Property``` - This is opening the bracket. $`a(x+y) = ax+ay`$   
    
## Tips
- ```Absolute Values``` can have 2 solutions     
- ```Quadratics``` can also have 2 solutions    
- Make sure to do the reverse when moving things to the other side, meaning a positive on the ```left side``` becomes a negative on the ```right side```    
