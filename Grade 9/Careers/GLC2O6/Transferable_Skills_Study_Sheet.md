# Study Sheet

# Employment Trends

## Globalization
- What does globalization look like?
- Example (clothing manufacturer)
    1. has a dress designed in Australia
    2. buys fabric from china
    3. has the dress sewn in Mexico
    4. warehouses the dress in canada
    5. sells it in North American and Europe

- What happens in some far-off place may not seem important, but it could have major effect on your work opportunities

- As more Canadian companies do buisness in the fast-moving international marketplace, they must:
    - compete with companies around the world
    - become more productive by finding ways to do things more cheaply
    - be able to change direction quickly to respond to the market place
    - meet the needs of a wide range of customers
    - be able to hire quickly and layoff quickly as needed

## Computers Everywhere!
- the computer has found its way into every part of our lives. They are on our disks. they run the robots on production lines. they are part of items such as our cars, our wristwatches, our microwaves. Computer know-how is essential for your work life
- Computerization is changing the way organizations operate
    - Traditional work is disappearing. You'll find computerized machines operating dispatching, driver license renewals
    - New jobs are being created to develop and run computer hardware, software, and computer networks
    - Computers are also creating new technologies which are, in turn, creating new types of work that never existed before

## Growth Of The Net
- Growth of the Internet use is affecting the way companies do business. Organizations can now
    - Know more about their competition in Canada and around the world 
    - Advertise to global audineces through Web sites
    - Sell their products/services to global customers. This is called e-commerce
    - Use e-mail as a way to communicate with employees, suppliers and buyers

## Smalll Business Grows Big
- It is used to be that only very large business had the financial means to compete in the marketplace. But computerizations and use of the Internet has given small organizatiosn the tools to be part of the global marketplace. In Canada, more jobs are being created by small businesses than large corporations
- Small businesses are growing faster than their bigger counterparts. Why?
    - The internet allows small companies to advertise equally with big companies. They can profit in specialized markets - markets too small for big business that has to sell to millions of people to be profitable
    - Small organizations can change quickly if necessary. It's hard for a big business to "turn on a dime"
    - Small organizations can be more innovative, particularly with respect to tehcnology
    - Because a small company can change quickly, it can take advantage of new ideas faster than a big one

## Baby Boomers
- Canada's largest group of citizens are getting older, retiring, and leaving their occupations. This means:
    - Growth of upper-level positions at companies
    - increased need for health care
    - increase in social/specialized spending

- As they grow older, baby boomers will require more frequent, sepcialized medical care. They will also want to maintain connections with careers and family for as long as possible. Therefore, expect to see continued growth in fields that cater to their wishes. The assisted lviving industry will continue to outpace other areas of healthcare, while growing teams of social workers will help care for less fortunate baby boomers

##  Growth Of Green Industries
- Businesses and governments are increasingly aware of Green Trends in employment, business, and other industries. Since the emergence of the green economy has produced a widespread adaptations and relocation of existing jobs
    - Increased need for training in green industries
    - Growth in green technologies
    - Changing requirements for business to maintain green standards
    - Increase in public awareness of enviornmental issues
    - Demand for workers in skilled fields (ex. trades)
- Globally, a new economy is emerging, and it has a strong shade of green. By some estimates, the green economy is already woth more than US $4 trillion and is growing fast. The importance of harm caused by climate change is known to everyone. Climate change is a crisis of enormous magnitude and it isn't just an issue in the future
- We are already feeling the effects, for example with receding glaciers, extreme weather events, etc. Some of theses jobs will be in specialized areas, such as installing solar panels and reseraching new building material technologies but the vast majority of jobs are in the same areas of employment that people already work in today.

# Transferable Skills
- A skill that can be used in `multiple` different occupations
- A transferable skill is one that you can take with you - `portable skill`
- You learn transferable skill through your `interactions & expereices` with others, school voluteering, home, jobs
- Transferable skills are organized into `3` sub-sets
    - Working with `people`
    - Working with `things`
    - `information & data`
- A transferable skill can give you an edge above others because employers will consider you a better candidate
- Makes you a more `efficient` worker

## Leadership
- A good leader:
- `listens` to ideas of others
- `encourages` others to participate
- take `charge`, not take `control`
- Lift people up they don't let them down
- Take responsibilities for mistakes and not blame others
- Lead through `respect` not through `fear`
- Solve conflicts and direct their group
- Give `feedback` that is clear and `specific`

## Communication Skills
- Skills that are needed for `writing`, `speaking` to and `interaction` with others effectively
- Communication can be `verbal` (ex. speaking singing) and `non-verbal` (ex. written, body language, facial expression)
- One way communication is `one main speaker, people listen`
- Two way communication is `back and forth interaction, building on to the points, sustsaining the conversation`

### Communication Process
|Process|Description|
|:------|:----------|
|Sender|Person delivering information to the reciever or receivers|
|Receiver|The receiver is the person that decodes and interprets the message|
|Feedback|Response given to reciever|
|Interference|Anything that impedes the communication|

### How To Receive A Message
- Listen $`\rightarrow`$ Analyze $`\rightarrow`$ Check
- Barrier to communication: `something that prevents the receiver to understand the message`
- Possible Barriers
    - Physical, hearing impaired
    - Language
    - Noise
    - Distance, proximity
    - Medium
    - Intereptions

- **Active Listening:** Active engagement, shows that you are listening, gives alot of feedback

## Stress Management
- **Stress**: The `imbalance` between demands and `resources`. When `pressure` exceeds one's perceived ability to `cope`.
- Stress Management Skills: help to balance yourself so that the level of stress is at a manageable level
- Possible Stressors
    - School
    - Relationships
    - Job/responsibilities
    - Everything, environment

- **Peer pressure:** `expectations` of yourself and others that you don't feel that you can keep up with them
    - Do well in school, drugs etc
- **Eustress**: `positive, productive` stresss to help you get things done. `Motivates` you to do better
    - Wedding day, excitied, ready to go to colleges, anxiety, scary book/movie
- Indentify your stressors!
    - Avoid the stressors
    - Alter the stressors
    - Accept the stressors
    - Adapt the stressors

## Organization
- State of being `structured` and `arranged` in an orderly way
- Advantages of Organization
    - reduces stress/workload
    - increases efficiency
    - maintains order and convenience
- Effective organizational skills are
    - learned, practiced, and updated constantly (ex: learning to prioritize urgency vs importance in order to better manage time)

## Negotiation
- Negotiation is a `discussion` that is aimed at reaching a `argreement`
- Sometimes negotiation requires a third party called a `mediator`
- **Mediator**: a `neutral` third party
- The mediator will try to help the two parties settle an agreement. The mediataor must `educate` themselves about both parties and what they are trying to achieve and what they will settle for in order to help them come to a `resolution`
- Possible outcomes
    - One team wins, both team wins, one team loses 

## Teamwork
- Ability to work, `cooperate`, and get `along` with other people in order to attain a common `goal`

# Workplace Hazards
- **Hazard**: Something that does harm to you or makes you ill

|Type of Hazard|Examples|
|:-------------|:-------|
|Physical|- electricity<br>- constant loud noise<br>- liquid/spills<br>-unguarded moving machinary|
|Biological|- blood or bodily fluids<br>- fungi<br>- bacteria & virius<br>- plants|
|Chemical|- Liquids eg. paints, acids<br>- Vapours + fumes eg. welding fumes<br>- gases eg. acetlyene, propane, carbon monoxide<br>- flammable materials eg. gasoline|
|Ergonomic|- poor lighting<br>- poor workstation or chairs<br>- poor posture<br>- constant lifting|

## Occupational Health and Safety Act Rights
- Right To Know
    - employer must tell you about all hazards at your job
- Right To Participate
    - take part in keeping the workplace safe
- Right To Refuse
    - you may refuse work that is unsafe/endangering

## Responsiblities For A Safe Enviornment
- Ensure that required training is given to you
- you are provided with equipment & training on how to use it properly
- make you & supervisors aware of all possible hazards

## 3 Types Of General Safety Traing
- General workplace safety training/information
- Specific safety training
- WHMIS

## Reasons To Report Injury Right Away
- correct medical treatment can be received immediately
- correct amount of compensation recieved
- cause of the injury can be investigated & prevented from happening again
- **PPE**: - personal protection equipment

# ESA
- Purpose of The Employment Standarads Act 2000 (ESA)
    - Sets out the **rights & responsiblities** of both employers & employers in ontario workplaces
- Employment Standards Act 2000
    - An act that provides the minimum standards for most employers working in ontario
- A 30 min break must be offered per 5 hours of work
- When young workers are called in to work, and they work more than 3hrs/day normally, they will get paid the 3hrs, even if they didn't do the work for 3hrs because they are called in
- Young workers are eligible for overtime if they work over 44hrs a week, which they get 1.5 their normal pay
- Young workers are entitled to be paid for public holidays
- When an employee works on a public holiday, they either get 1.5x their pay or alternative day off
- Young workers who are part-time employess **are** covered by the ESA
- The ESA covers young workers, the have the same rights as other employees in ontario workplaces under the ESA (certain jobs can be exempted)
- An employee cannot deduct the cost of a uniform or other items from an employees pay, unless agreed in written form
- An employee cannot withhold, make dudction from or require an employee to turn over their tips and other gratuties unless following a court order or there is a tip pool arrangement
- To see if getting paid correctly, keep a record of hours worked
- Young workers are only entitled to vacation pay after 12 months of employment
- Young workers working in retail are allowed to refuse to work on public holidays or Sundays, unless agreed upon at time of hire, basically having the option to work 
    - Depends on certain services (eg. restaurants), will be informed upon hiring
- Employers have to tell young workers in advance if they are going to end their employement
- Young workers can file through ministry of labour to get money owed by the employer
- Call the ESA for more info
- Workers that are not covered by the *Employment Standards Act*: `bank tellers`
- General hourly **minimum wage**: `$14`
    - 3 exceptions
        1. Firefighters
        2. Actors
        3. Student ($13.5)
- 3 Basic Rights
    1. Right to refuse unsafe work.
    2. Right to participate in the workplace health and safety activities through the Health and Safety Committee (HSC) or as a worker health and safety representative.
    3. Right to know, or the right to be informed about, actual and potential dangers in the workplace.

## Minimum Age
|Job|Minimum Age|
|:--|:----------|
|Factory Worker|15|
|Window Cleaner|18|
|Underground Miner|18|
|Construction Worker|16|
|Logger|16|

## Jobs That Work During Holidays
1. Hotel motels, tourist resorts
2. restaurants, and tavers
3. hosipital and nursing homes
4. continuous operations
- They should be paid a `public holiday pay` + `premium pay` for each hours worked

